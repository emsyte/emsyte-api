jest.mock("../integrations/actions");
import {
  IntegrationProvider,
  IntegrationType,
} from "@emsyte/common/integration";
import { validateProviderSettings } from "../integrations/actions";
import { getAPICaller } from "../utils/testing/api";
import { handler } from "./integration";

const caller = getAPICaller(handler);
const INTEGRATIONS = [
  {
    provider: IntegrationProvider.SEND_GRID,
    type: IntegrationType.EMAIL,
    title: "Sendgrid",
    description: "World's Largest Cloud-Based Email Delivery Platform.",
    active: false,
    settings: null,
  },
  {
    provider: IntegrationProvider.TWILIO,
    type: IntegrationType.SMS,
    title: "Twilio",
    description: "Communication APIs for SMS, Voice, Video and Authentication",
    active: true,
    settings: {},
  },
  {
    provider: IntegrationProvider.ACTIVE_CAMPAIGN,
    type: IntegrationType.CRM,
    title: "ActiveCampaign",
    description: "Marketing Automation - Small Business CRM",
    active: false,
    settings: null,
  },

  {
    provider: IntegrationProvider.HUB_SPOT,
    type: IntegrationType.CRM,
    title: "HubSpot",
    description: "Inbound Marketing, Sales, and Service Software",
    active: false,
    settings: null,
  },

  {
    provider: IntegrationProvider.CONSTANT_CONTACT,
    type: IntegrationType.CRM,
    title: "Constant Contact",
    description: "Powerful email. No restrictions. Real results.",
    active: false,
    settings: null,
  },
];

describe("Integrations", () => {
  test("Get integration settings", async () => {
    const response = await caller.get("/get", {
      auth: 3,
      team: "team-premium",
    });
    expect(response.data).toEqual({ data: INTEGRATIONS });
  });

  test("Update integration settings", async () => {
    (validateProviderSettings as any).mockReturnValue({
      accountSid: "changed",
      authToken: "changed",
    });

    const response = await caller.post(
      "/set",
      {
        provider: IntegrationProvider.TWILIO,
        settings: {
          accountSid: "test",
          authToken: "test",
        },
      },
      {
        auth: 2,
        team: "team-basic",
      }
    );

    const expected = {
      provider: IntegrationProvider.TWILIO,
      type: IntegrationType.SMS,
      title: "Twilio",
      description:
        "Communication APIs for SMS, Voice, Video and Authentication",
      active: true,
      settings: {
        accountSid: "changed",
        authToken: "changed",
      },
    };

    expect(response.data).toEqual({ data: expected });

    // Test that it updated the integrations
    const response2 = await caller.get("/get", {
      auth: 2,
      team: "team-basic",
    });
    expect(response2.data).toEqual({
      data: INTEGRATIONS.map((integration) =>
        integration.provider === IntegrationProvider.TWILIO
          ? expected
          : integration
      ),
    });
  });
});
