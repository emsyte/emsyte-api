import {
  // doGetActiveIntegration,
  doGetIntegrationSettings,
  doSetIntegrationSetting,
} from "../controllers/integrations";
import { TeamRouter } from "../utils/TeamRouter";
import { SetIntegrationValidator } from "../validators/integrations";

const router = new TeamRouter();

// TODO: Create interfaces in @emsyte/common for request/response

router.post(
  "/set",
  {
    bodyValidator: SetIntegrationValidator,
    bodyValidationMessage: "Integration validation failed.",
    permissions: ["integrations:edit"],
  },
  async ({ data, team }) => {
    return { data: await doSetIntegrationSetting(team, data) };
  }
);

router.get(
  "/get",
  {
    permissions: ["integrations:read"],
  },
  async ({ team }) => {
    return { data: await doGetIntegrationSettings(team) };
  }
);

export const handler = router.getHandler();
