import { IsString } from "class-validator";
import { doGetPlans, doValidatePromotionCode } from "../controllers/stripe";
import ServerlessRouter from "../utils/ServerlessRouter";

const router = new ServerlessRouter();

// Plan Method
router.get(
  "/plans",
  {
    allowUnauthenticated: true,
  },
  async () => {
    return await doGetPlans();
  }
);

class CouponQuery {
  @IsString()
  coupon: string;
}

router.get(
  "/coupon",
  {
    queryValidator: CouponQuery,
    allowUnauthenticated: true,
  },
  async ({ query }) => {
    return await doValidatePromotionCode(query.coupon);
  }
);

export const handler = router.getHandler();
