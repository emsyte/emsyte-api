import {
  APIAuthLoginData,
  APIAuthRefreshTokenData,
  APIAuthRegisterData,
  APIAuthResponse,
  APIUserForgotPasswordConfirmData,
  APIUserForgotPasswordData,
  APIUserResponse,
} from "@emsyte/common/user";
import {
  doForgotPassword,
  doForgotPasswordConfirm,
  // getUser,
  login,
  refresh,
  register,
} from "../controllers/users";
import ServerlessRouter from "../utils/ServerlessRouter";
import {
  RefreshTokenValidator,
  UserForgotPasswordConfirmValidator,
  UserForgotPasswordValidator,
  UserLoginValidator,
  UserRegisterValidator,
} from "../validators/users";

const router = new ServerlessRouter();

router.post<APIAuthRegisterData, APIAuthResponse>(
  "/register",
  {
    bodyValidator: UserRegisterValidator,
    allowUnauthenticated: true,
  },
  ({ data }) => {
    return register(data);
  }
);

router.post<APIAuthLoginData, APIAuthResponse>(
  "/login",
  {
    bodyValidator: UserLoginValidator,
    allowUnauthenticated: true,
  },
  async ({ data }) => {
    const { email, password } = data;
    return await login(email, password);
  }
);

router.post<APIAuthRefreshTokenData, APIUserResponse>(
  "/token/refresh",
  {
    bodyValidator: RefreshTokenValidator,
    allowUnauthenticated: true,
  },
  async ({ data }) => {
    const { token, refreshToken } = data;
    return await refresh(token, refreshToken);
  }
);

router.post<APIUserForgotPasswordData, { message: string }>(
  "/forgot-password",
  {
    bodyValidator: UserForgotPasswordValidator,
    allowUnauthenticated: true,
  },
  async ({ data }) => {
    const response = await doForgotPassword(data);
    return response;
  }
);

router.post<APIUserForgotPasswordConfirmData, { message: string }>(
  "/forgot-password-confirm",
  {
    bodyValidator: UserForgotPasswordConfirmValidator,
    allowUnauthenticated: true,
  },
  async ({ data }) => {
    const response = await doForgotPasswordConfirm(data);
    return response;
  }
);

export const handler = router.getHandler();
