import { APIResponse } from "@emsyte/common/api";
import { ViewerEventObject, WebinarOfferObject } from "@emsyte/common/webinar";
import { Type } from "class-transformer";
import { IsInt } from "class-validator";
import { serialize } from "cookie";
import { doGetUploadUrl } from "../controllers/reactions";
import { getPostUrl } from "../controllers/webinars/attendees";
import { createTrackingCookie } from "../controllers/webinars/tracking";
import { Attendee } from "../db/entities/Attendee";
import { Offer } from "../db/entities/Offer";
import { OfferEvent } from "../db/entities/OfferEvent";
import { ViewerEvent } from "../db/entities/ViewerEvent";
import { serializeViewerEvent } from "../lib/serializers/eventSerializer";
import { serializeOffer } from "../lib/serializers/offerSerializer";
import { getResponse } from "../utils/api";
import { AttendeeRouter } from "../utils/AttendeeRouter";
import { Response } from "../utils/Response";
import { getIntFromPath } from "../validators/utils";

// APIs related to registering for a webinar
const router = new AttendeeRouter();

/**
 * Get Event
 */
router.get<APIResponse<ViewerEventObject>>(
  "/event",
  {},
  async ({ event, attendeeId }) => {
    const result = serializeViewerEvent(event);

    if (attendeeId) {
      const attendee = await Attendee.findOne(attendeeId);
      attendee.event = event;

      result.webinar.settings.postViewUrl = getPostUrl(event.webinar, attendee);
    }

    return getResponse(result);
  }
);

/**
 * Get Offers
 */
router.get<APIResponse<WebinarOfferObject[]>>(
  "/offers",
  {},
  async ({ event }) => {
    const offers = await Offer.find({ webinar: event.webinar });
    return getResponse(offers.map(serializeOffer));
  }
);

/**
 * Track Offer
 */
router.post(
  "/offers/:offerId/track",
  {},
  async ({ attendeeId, pathParameters, headers }) => {
    const offerId = getIntFromPath(pathParameters, "offerId");

    // Don't track the actions of users
    if (!attendeeId) {
      return {};
    }

    const attendee = await Attendee.findOne(attendeeId);
    const offer = await Offer.findOne(offerId);

    // TODO: Validate that the offer is on the webinar attached to the attendee event
    await OfferEvent.create({
      attendee: { id: attendeeId },
      offer: { id: offerId },
    }).save();

    return new Response(
      {},
      {
        headers: {
          // TODO: Specify allowed origins
          "Access-Control-Allow-Origin": headers.origin,
          "set-cookie": serialize(
            `em-offer-${offer.id}`,
            createTrackingCookie(attendee, offer),
            {
              path: "/",
            }
          ),
        },
      }
    );
  }
);

class UploadUrlValidator {
  @Type(() => Number)
  @IsInt()
  start: number;
}

router.get(
  "/get-upload-url",
  {
    queryValidator: UploadUrlValidator,
  },
  async ({ attendeeId, query }) => {
    return await doGetUploadUrl(attendeeId, query.start);
  }
);

class HeartbeatValidator {
  @IsInt()
  time_offset: number;
}

router.post(
  "/heartbeat",
  {
    bodyValidator: HeartbeatValidator,
  },
  async ({ attendeeId, data }) => {
    console.log(attendeeId);

    await ViewerEvent.create({
      attendee: { id: attendeeId },
      time_offset: data.time_offset,
    }).save();
    return {};
  }
);

export const handler = router.getHandler();
