import { APIResponse } from "@emsyte/common/api";
import {
  APITemplateCreateData,
  APITemplateInstanceCreateData,
  APITemplateInstanceUpdateData,
  APITemplateUpdateData,
  TemplateInstanceObject,
  TemplateInstanceObjectFull,
  TemplateObject,
} from "@emsyte/common/template";
import {
  doCreateTemplateInstance,
  doDeleteTemplateInstance,
  doGetAllTemplates,
  doGetTemplateInstance,
  doGetTemplateInstances,
  doResetTemplateInstance,
  doCreateTemplateFromInstance,
  doUpdateTemplateInstance,
  doUpdateTemplateFromInstance,
  doDeleteTemplate,
  doGetTemplate,
} from "../controllers/template";
import {
  serializeTemplate,
  serializeTemplateFull,
  serializeTemplateInstance,
  serializeTemplateInstanceFull,
} from "../lib/serializers/templateSerializer";
import { TeamRouter } from "../utils/TeamRouter";
import {
  CreateTemplateInstanceValidator,
  CreateTemplateValidator,
  UpdateTemplateInstanceValidator,
  UpdateTemplateValidator,
} from "../validators/template";
import { getIntFromPath } from "../validators/utils";

const router = new TeamRouter();

/**
 * Get global templates
 */
router.get<APIResponse<TemplateObject[]>>("/", {}, async ({ team }) => {
  const templates = await doGetAllTemplates(team);

  return { data: templates.map(serializeTemplate) };
});

/**
 * Get template instances tied to a webinar
 */
router.get<APIResponse<TemplateInstanceObject[]>>(
  "/webinar/:webinarId",
  {},
  async ({ team, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const instances = await doGetTemplateInstances(team, webinarId);
    return { data: instances.map(serializeTemplateInstance) };
  }
);

/**
 * Create template instance tied to a webinar
 */
router.post<APITemplateInstanceCreateData, APIResponse<TemplateInstanceObject>>(
  "/webinar/:webinarId",
  {
    bodyValidator: CreateTemplateInstanceValidator,
  },
  async ({ pathParameters, data, team }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const instance = await doCreateTemplateInstance(team, webinarId, data);

    return {
      data: serializeTemplateInstance(instance),
    };
  }
);

/**
 * Update a template instance
 */
router.put<APITemplateInstanceUpdateData, APIResponse<TemplateInstanceObject>>(
  "/instance/:instanceId",
  {
    bodyValidator: UpdateTemplateInstanceValidator,
  },
  async ({ pathParameters, data, team }) => {
    const instanceId = getIntFromPath(pathParameters, "instanceId");
    const templateInstance = await doUpdateTemplateInstance(
      team,
      instanceId,
      data
    );

    return {
      data: serializeTemplateInstance(templateInstance),
    };
  }
);

/**
 * Get template instance content
 */
router.get<APIResponse<TemplateInstanceObjectFull>>(
  "/instance/:instanceId",
  {},
  async ({ pathParameters, team }) => {
    const instanceId = getIntFromPath(pathParameters, "instanceId");
    const templateInstance = await doGetTemplateInstance(team, instanceId, {
      relations: ["parent"],
    });

    return {
      data: serializeTemplateInstanceFull(templateInstance),
    };
  }
);

/**
 * Delete template instance content
 */
router.delete<{}, APIResponse<TemplateInstanceObject>>(
  "/instance/:instanceId",
  {},
  async ({ pathParameters, team }) => {
    const instanceId = getIntFromPath(pathParameters, "instanceId");
    const templateInstance = await doDeleteTemplateInstance(team, instanceId);

    return {
      data: serializeTemplateInstance(templateInstance),
    };
  }
);

/**
 * Reset the content of the template instance to it's parent
 */
router.post<{}, APIResponse<TemplateInstanceObject>>(
  "/instance/:instanceId/reset",
  {},
  async ({ pathParameters, team }) => {
    const instanceId = getIntFromPath(pathParameters, "instanceId");
    const templateInstance = await doResetTemplateInstance(team, instanceId);

    return {
      data: serializeTemplateInstance(templateInstance),
    };
  }
);

/**
 * Create new global template
 */
router.post<APITemplateCreateData, APIResponse<TemplateObject>>(
  "/",
  {
    bodyValidator: CreateTemplateValidator,
  },
  async ({ team, data }) => {
    const template = await doCreateTemplateFromInstance(team, data);
    return {
      data: serializeTemplate(template),
    };
  }
);

/**
 * Update global template
 */
router.put<APITemplateUpdateData, APIResponse<TemplateObject>>(
  "/:templateId",
  {
    bodyValidator: UpdateTemplateValidator,
  },
  async ({ pathParameters, team, data }) => {
    const templateId = getIntFromPath(pathParameters, "templateId");

    const template = await doUpdateTemplateFromInstance(team, templateId, data);

    return {
      data: serializeTemplate(template),
    };
  }
);

/**
 * Get global template content (for preview)
 */
router.get<APIResponse<TemplateObject>>(
  "/:templateId",
  {},
  async ({ pathParameters, team }) => {
    const templateId = getIntFromPath(pathParameters, "templateId");

    const template = await doGetTemplate(team, templateId);

    return {
      data: serializeTemplateFull(template),
    };
  }
);

/**
 * Delete global template
 */
router.delete<{}, APIResponse<TemplateObject>>(
  "/:templateId",
  {},
  async ({ pathParameters, team }) => {
    const templateId = getIntFromPath(pathParameters, "templateId");

    const template = await doDeleteTemplate(team, templateId);

    return {
      data: serializeTemplate(template),
    };
  }
);

export const handler = router.getHandler();
