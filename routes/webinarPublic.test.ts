import { APIResponse } from "@emsyte/common/api";
import {
  APIWebinarAttendeeCreateData,
  EventObject,
  PublicWebinarObject,
  WebinarType,
} from "@emsyte/common/webinar";
import { Attendee } from "../db/entities/Attendee";
import { getAPICaller } from "../utils/testing/api";
import { handler } from "./webinarPublic";
import MockDate from "mockdate";

describe("Webinar Public Routes", () => {
  const caller = getAPICaller(handler);
  afterEach(() => {
    MockDate.reset();
  });

  test("Get Webinar", async () => {
    const expected: APIResponse<PublicWebinarObject> = {
      data: {
        id: 1,
        name: "Test Webinar",
        type: WebinarType.RECORDED,
        description: "test",
        thumbnail:
          "https://s3.amazonaws.com/io.app.justwebinar/webinars/images/2020/September/16/ea4cce842d738589/Test%20Webinar.jpg",
        registrationFee: 0,
        isPaid: false,
        checkoutLink: "",
        redirectLink: "",
        settings: {
          registrationFormScript: "",
        },
      },
    };

    const response = await caller.get("/1");
    expect(response.data).toEqual(expected);
  });

  test("Get events", async () => {
    MockDate.set("2020-09-09T10:30:00.000Z");

    const expected: APIResponse<EventObject[]> = {
      data: [
        {
          id: 1,
          canceled: false,
          start: "2020-09-10T17:30:00.000Z",
          end: "2020-09-10T18:00:00.000Z",
          videoUrl: "https://www.youtube.com/watch?v=xv2gfxX-xDo",
          recurrenceId: null,
        },
      ],
    };

    const response = await caller.get("/1/events");
    expect(response.data).toEqual(expected);
  });

  test("Get event", async () => {
    MockDate.set("2020-09-09T10:30:00.000Z");

    const expected: APIResponse<EventObject> = {
      data: {
        id: 1,
        canceled: false,
        start: "2020-09-10T17:30:00.000Z",
        end: "2020-09-10T18:00:00.000Z",
        videoUrl: "https://www.youtube.com/watch?v=xv2gfxX-xDo",
        recurrenceId: null,
      },
    };

    const response = await caller.get("/1/events/1");
    expect(response.data).toEqual(expected);
  });

  test("Register for event", async () => {
    const request: APIWebinarAttendeeCreateData = {
      email: "attendee@example.com",
      timezone: "America/Chicago",
      firstName: "Joe",
      lastName: "Smith",
    };

    const expected = {
      message: expect.any(String),
      redirectTo: expect.any(String),
    };

    const response = await caller.post("/1/events/1/register", request);

    expect(response.statusCode).toEqual(200);
    expect(response.data).toEqual(expected);

    const attendee = await Attendee.findOne({
      ...request,
      event: { id: 1 },
    });

    expect(attendee).toBeDefined();
  });

  test("Require valid timezone", async () => {
    const request: APIWebinarAttendeeCreateData = {
      email: "bad-timezone@example.com",
      timezone: "Bar/Foo",
      firstName: "Joe",
      lastName: "Smith",
    };

    const response = await caller.post("/1/events/1/register", request);

    expect(response.statusCode).toEqual(422);
    expect(response.data).toEqual({
      message: "Validation failed",
      errors: { timezone: ["timezone must be a valid timezone"] },
    });
  });
});
