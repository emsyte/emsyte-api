import { APIResponse } from "@emsyte/common/api";
import { AuthType } from "@emsyte/common/auth";
import {
  ViewerEventObject,
  WebinarStatus,
  WebinarType,
} from "@emsyte/common/webinar";
import * as jwt from "jsonwebtoken";
import * as moment from "moment";
import { URL } from "url";
import { getAPICaller } from "../utils/testing/api";
import { handler } from "./viewer";

describe("Viewer API", () => {
  const api = getAPICaller(handler);

  test("should return viewer event", async () => {
    const response = await api.get("event", {
      claims: {
        id: 1,
        type: AuthType.ATTENDEE,
        active: true,
      },
      pathParameters: { eventId: "1" },
    });

    const expected: APIResponse<ViewerEventObject> = {
      data: {
        canceled: false,
        id: 1,
        start: "2020-09-10T17:30:00.000Z",
        end: "2020-09-10T18:00:00.000Z",
        recurrenceId: null,
        videoUrl: "https://www.youtube.com/watch?v=xv2gfxX-xDo",

        webinar: {
          id: 1,
          name: "Test Webinar",
          type: WebinarType.RECORDED,
          description: "test",
          duration: 732,
          status: WebinarStatus.ACTIVE,
          replayExpiration: 1,
          thumbnail:
            "https://s3.amazonaws.com/io.app.justwebinar/webinars/images/2020/September/16/ea4cce842d738589/Test%20Webinar.jpg",
          registrationFee: 0,
          isPaid: false,
          checkoutLink: "",
          redirectLink: "",
          settings: {
            postRegistrationUrl:
              "http://localhost:3000/webinar/confirmation/eyJpZCI6MX0=",
            postViewUrl: expect.stringMatching(
              /^http:\/\/localhost:3000\/webinar\/thankyou\/eyJpZCI6MX0=.+/
            ),

            liveRoomScript: "",
            registrationFormScript: "",
            registrationPageScript: "",
            replayRoomScript: "",
            thankyouPageScript: "",
          },
        },
      },
    };
    expect(response.data).toEqual(expected);
  });

  test("should add token to post view url", async () => {
    const response = await api.get<APIResponse<ViewerEventObject>>("event", {
      claims: {
        id: 1,
        type: AuthType.ATTENDEE,
        active: true,
      },
      pathParameters: { eventId: "1" },
    });

    const postViewUrl = new URL(
      response.data.data.webinar.settings.postViewUrl
    );

    expect(postViewUrl.searchParams.get("id")).toEqual(
      Buffer.from(JSON.stringify({ id: 1, eventId: 1 })).toString("base64")
    );

    const claims = jwt.decode(postViewUrl.searchParams.get("atoken")) as any;

    // The token expiration should be after the replay expiration is over
    expect(
      moment(claims.exp * 1000).diff(moment("2020-09-12T05:00:00.000Z"))
    ).toEqual(0);
  });
});
