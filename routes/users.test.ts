import { getAPICaller } from "../utils/testing/api";
import { handler } from "./users";
// import { Team } from "../db/entities/Team";

describe("Users", () => {
  const api = getAPICaller(handler);

  // test("Register", async () => {
  //   const response = await api.post<APIAuthResponse>("/register", {
  //     email: "register-test@example.com",
  //     firstName: "John",
  //     lastName: "Doe",
  //     teamName: "Register Test Team",
  //     password: "access",
  //   });

  //   expect(response.data).toEqual({
  //     refreshToken: expect.any(String),
  //     token: expect.any(String),
  //     defaultTeam: expect.any(String),
  //   });

  //   const team = await Team.findOne({
  //     where: { slug: response.data.defaultTeam },
  //     relations: ["teamToUsers", "teamToUsers.user"],
  //   });
  //   expect(team.name).toEqual("Register Test Team");
  //   expect(team.teamToUsers[0].user.email).toEqual("register-test@example.com");
  // });

  test("Login", async () => {
    const response = await api.post("/login", {
      email: "johndoe1@example.com",
      password: "access",
    });

    expect(response.data).toEqual({
      refreshToken: expect.any(String),
      token: expect.any(String),
      defaultTeam: "team-free",
    });
  });
});
