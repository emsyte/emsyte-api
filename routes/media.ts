import { IsIn, IsMimeType, IsString } from "class-validator";
import * as FileType from "file-type";
import { BinaryResponse, Response } from "../utils/Response";
import ServerlessRouter from "../utils/ServerlessRouter";
import { getUploadUrl } from "../utils/storage";
import { FSStorageDriver } from "../utils/storage/fs";
import { USE_S3 } from "../utils/storage/manager";

const router = new ServerlessRouter();

class UploadUrlQueryValidator {
  @IsIn(["webinar", "template-thumbnail", "template", "offer", "reaction"])
  group: string;

  @IsString()
  filename: string;

  @IsMimeType()
  contentType: string;
}

router.get(
  "upload-url",
  {
    queryValidator: UploadUrlQueryValidator,
  },
  async ({ query, userId }) => {
    console.log(userId);
    const { url, uploadTo } = await getUploadUrl(
      query.group,
      query.filename,
      query.contentType
    );
    return { url, uploadTo };
  }
);

if (!USE_S3) {
  router.get(
    "*key",
    { allowUnauthenticated: true },
    async ({ pathParameters }) => {
      const key = pathParameters.key;
      console.log(key);

      const driver = new FSStorageDriver();
      const content = await driver.getFile(key);

      const contentType = await FileType.fromBuffer(content);

      return new BinaryResponse(content, contentType.mime);
    }
  );

  router.put<any, {}>(
    "*key",
    {
      allowUnauthenticated: true,
    },
    async ({ pathParameters, data }) => {
      const key = pathParameters.key;

      const driver = new FSStorageDriver();
      await driver.uploadFile(key, "", data.files[0].content);

      return new Response({});
    }
  );
}

export const handler = router.getHandler();
