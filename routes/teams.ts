import { APIResponse } from "@emsyte/common/api";
import {
  APICheckTeamResponse,
  APICheckTeamSlugQuery,
  APIRegisterTeamData,
  TeamObject,
} from "@emsyte/common/team";
import { doCheckTeamName, doRegisterTeam } from "../controllers/teams";
import { serializeTeam } from "../lib/serializers/teamSerializer";
import { getResponse } from "../utils/api";
import ServerlessRouter from "../utils/ServerlessRouter";
import { CheckNameValidator, RegisterTeamValidator } from "../validators/teams";

const router = new ServerlessRouter();

router.get<APICheckTeamResponse, APICheckTeamSlugQuery>(
  "check-name",
  {
    queryValidator: CheckNameValidator,
    queryValidationMessage: "Invalid team name",
    allowUnauthenticated: true,
  },
  async ({ query: { slug } }) => {
    return await doCheckTeamName(slug);
  }
);

router.post<APIRegisterTeamData, APIResponse<TeamObject>>(
  "register",
  {
    bodyValidator: RegisterTeamValidator,
    bodyValidationMessage: "Invalid team data",
  },
  async ({ data, userId }) => {
    const team = await doRegisterTeam(userId, data);
    return getResponse(serializeTeam(team));
  }
);

export const handler = router.getHandler();
