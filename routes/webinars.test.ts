import { APIResponse } from "@emsyte/common/api";
import {
  APIWebinarAddRecurrenceData,
  APIWebinarCreateData,
  EventObject,
  Frequency,
  RecurrenceObject,
  ViewerEventObject,
  WebinarObject,
  WebinarOfferObject,
  WebinarSettingsObject,
  WebinarStatus,
  WebinarType,
} from "@emsyte/common/webinar";
import MockDate from "mockdate";
import { Event } from "../db/entities/Event";
import { Webinar } from "../db/entities/Webinar";
import { serializeEvent } from "../lib/serializers/eventSerializer";
import { getAPICaller } from "../utils/testing/api";
import { handler } from "./webinars";

const webinarSettings: WebinarSettingsObject = {
  postRegistrationUrl:
    "http://localhost:3000/webinar/confirmation/eyJpZCI6MX0=",
  postViewUrl: "http://localhost:3000/webinar/thankyou/eyJpZCI6MX0=",

  liveRoomScript: "",
  registrationFormScript: "",
  registrationPageScript: "",
  replayRoomScript: "",
  thankyouPageScript: "",
};

describe("Webinar Routes", () => {
  afterEach(() => MockDate.reset());

  // webinar1 from fixture
  const webinar = {
    id: 1,
    name: "Test Webinar",
    type: WebinarType.RECORDED,
    description: "test",
    videoUrl: "https://www.youtube.com/watch?v=xv2gfxX-xDo",
    duration: 732,
    status: WebinarStatus.ACTIVE,
    replayExpiration: 1,
    thumbnail:
      "https://s3.amazonaws.com/io.app.justwebinar/webinars/images/2020/September/16/ea4cce842d738589/Test%20Webinar.jpg",
    registrationFee: 0,
    isPaid: false,
    checkoutLink: "",
    redirectLink: "",
    settings: webinarSettings,
  };
  const { id, ...webinarWithoutId } = webinar;

  const api = getAPICaller(handler);

  test("Can retrieve webinars", async () => {
    const expected: APIResponse<WebinarObject[]> = {
      data: [
        {
          ...webinar,
          id: 2,
          name: "Test Draft Webinar",
          status: WebinarStatus.DRAFT,
        },
        {
          ...webinar,
        },
      ],
    };

    const response = await api.get("/", { auth: 1, team: "team-free" });
    expect(response.data).toEqual(expected);
  });

  test("Can retrieve individual webinar", async () => {
    const expected: APIResponse<WebinarObject> = {
      data: {
        ...webinar,
        emailGateway: null,
        smsGateway: null,
      },
    };

    const response = await api.get("/1", { auth: 1, team: "team-free" });

    expect(response.data).toEqual(expected);
  });

  test("Can create new webinar", async () => {
    const request: APIWebinarCreateData = {
      name: "Test Webinar",
      description: "test",
      duration: 300,
      replayExpiration: 2,
      videoUrl: "<video url>",
      type: WebinarType.RECORDED,
      thumbnail: "",
    };

    const expected: APIResponse<WebinarObject> = {
      data: {
        id: expect.any(Number),
        ...request,
        status: WebinarStatus.DRAFT,
        settings: {
          postRegistrationUrl: expect.stringMatching(
            /http:\/\/localhost:3000\/webinar\/confirmation\/([a-zA-Z=]+)/
          ),
          postViewUrl: expect.stringMatching(
            /http:\/\/localhost:3000\/webinar\/thankyou\/([a-zA-Z=]+)/
          ),

          liveRoomScript: "",
          registrationFormScript: "",
          registrationPageScript: "",
          replayRoomScript: "",
          thankyouPageScript: "",
        },
        description: "test",
        thumbnail: "",
        registrationFee: 0,
        isPaid: false,
        checkoutLink: "",
        redirectLink: "",
      },
    };

    const response = await api.post<APIResponse<WebinarObject>>("/", request, {
      auth: 2,
      team: "team-basic",
    });
    expect(response.data).toEqual(expected);

    const response2 = await api.post(
      `/${response.data.data.id}/activate`,
      {},
      {
        auth: 2,
        team: "team-basic",
      }
    );
    expect(response2.data).toEqual({
      data: {
        ...expected.data,
        status: WebinarStatus.ACTIVE,
      },
    });
  });

  test("Can schedule events", async () => {
    MockDate.set("2020-09-01");

    const request: APIWebinarAddRecurrenceData = {
      timezone: "America/New_York",
      days: [4],
      frequency: Frequency.WEEKLY,
      start: "2020-09-03 13:30",
      end: "2020-09-20",
    };

    const response: APIResponse<RecurrenceObject> = await api.post(
      "/9/recurrences",
      request,
      {
        auth: 3,
        team: "team-premium",
      }
    );

    const expected: APIResponse<RecurrenceObject> = {
      data: {
        webinarId: 9,
        id: 1,
        timezone: "America/New_York",
        days: [4],
        frequency: Frequency.WEEKLY,
        start: "2020-09-03 13:30",
        end: "2020-09-20",
      },
    };

    const expectedEvents: EventObject[] = [
      {
        canceled: false,
        id: expect.any(Number),
        videoUrl: webinar.videoUrl,
        recurrenceId: 1,
        start: "2020-09-03T17:30:00.000Z",
        end: "2020-09-03T18:00:00.000Z",
      },
      {
        canceled: false,
        id: expect.any(Number),
        videoUrl: webinar.videoUrl,
        recurrenceId: 1,
        start: "2020-09-10T17:30:00.000Z",
        end: "2020-09-10T18:00:00.000Z",
      },
      {
        canceled: false,
        id: expect.any(Number),
        videoUrl: webinar.videoUrl,
        recurrenceId: 1,
        start: "2020-09-17T17:30:00.000Z",
        end: "2020-09-17T18:00:00.000Z",
      },
    ];
    expect(response.data).toEqual(expected);
    const events = await Event.find({
      webinarId: 9,
    });
    expect(events.map(serializeEvent)).toEqual(expectedEvents);
  });

  test("Can schedule single event", async () => {
    MockDate.set("2021-08-31 19:30");

    const request: APIWebinarAddRecurrenceData = {
      days: [],
      end: "2021-08-31",
      frequency: Frequency.DAILY,
      start: "2021-08-31 15:00",
      timezone: "America/Chicago",
    };

    const response = await api.post<APIResponse<RecurrenceObject>>(
      "/9/recurrences",
      request,
      {
        auth: 3,
        team: "team-premium",
      }
    );

    const expected: APIResponse<RecurrenceObject> = {
      data: {
        webinarId: 9,
        id: expect.any(Number),
        timezone: "America/Chicago",
        days: [],
        frequency: Frequency.DAILY,
        start: "2021-08-31 15:00",
        end: "2021-08-31",
      },
    };
    expect(response.data).toEqual(expected);

    const expectedEvents: EventObject[] = [
      {
        canceled: false,
        id: expect.any(Number),
        videoUrl: webinar.videoUrl,
        recurrenceId: expect.any(Number),
        start: "2021-08-31T20:00:00.000Z",
        end: "2021-08-31T20:30:00.000Z",
      },
    ];

    const events = await Event.find({
      webinarId: 9,
      recurrenceId: response.data.data.id,
    });

    expect(events.map(serializeEvent)).toEqual(expectedEvents);
  });

  test("Archive webinar", async () => {
    MockDate.set("2021-06-23T09:30:00.000Z");
    const newWebinar = await Webinar.create({
      ...webinarWithoutId,
      teamId: 3,
    }).save();

    await Event.createQueryBuilder()
      .insert()
      .values([
        {
          webinarId: newWebinar.id,
          start: "2021-06-22T10:00:00.000Z",
          end: "2021-06-22T11:00:00.000Z",
        },
        {
          webinarId: newWebinar.id,
          start: "2021-06-23T10:00:00.000Z",
          end: "2021-06-23T11:00:00.000Z",
        },
        {
          webinarId: newWebinar.id,
          start: "2021-06-24T10:00:00.000Z",
          end: "2021-06-24T11:00:00.000Z",
        },
      ])
      .execute();

    const response = await api.post<APIResponse<WebinarObject>>(
      `/${newWebinar.id}/archive`,
      {},
      {
        auth: 3,
        team: "team-premium",
      }
    );
    const events = await Event.find({ webinarId: newWebinar.id });

    expect(events.map(serializeEvent)).toEqual([
      {
        canceled: false,
        id: expect.any(Number),
        recurrenceId: null,
        start: "2021-06-22T10:00:00.000Z",
        end: "2021-06-22T11:00:00.000Z",
        videoUrl: "",
      },
      {
        canceled: true,
        id: expect.any(Number),
        recurrenceId: null,
        start: "2021-06-23T10:00:00.000Z",
        end: "2021-06-23T11:00:00.000Z",
        videoUrl: "",
      },
      {
        canceled: true,
        id: expect.any(Number),
        recurrenceId: null,
        start: "2021-06-24T10:00:00.000Z",
        end: "2021-06-24T11:00:00.000Z",
        videoUrl: "",
      },
    ]);

    expect(response.statusCode).toEqual(200);
    expect(response.data.data.status).toEqual(WebinarStatus.ARCHIVE);
  });

  test("Add videoUrl to live events", async () => {
    const newWebinar = await Webinar.create({
      ...webinarWithoutId,
      type: WebinarType.LIVE,
      videoUrl: "",
      teamId: 3,
    }).save();

    const event = await Event.create({
      webinarId: newWebinar.id,
      start: "2020-09-10T17:30:00.000Z",
      end: "2020-09-10T18:00:00.000Z",
      videoUrl: "",
    }).save();

    const response = await api.put<APIResponse<ViewerEventObject>>(
      `/${newWebinar.id}/events/${event.id}`,
      {
        videoUrl: "https://example.com/video.mp4",
      },
      {
        auth: 3,
        team: "team-premium",
      }
    );

    expect(response.statusCode).toEqual(200);
    expect(response.data.data.videoUrl).toEqual(
      "https://example.com/video.mp4"
    );
  });

  test("Attempting to add a videoUrl to a recorded webinar fails", async () => {
    const newWebinar = await Webinar.create({
      ...webinarWithoutId,
      type: WebinarType.RECORDED,
      videoUrl: "",
      teamId: 3,
    }).save();

    const event = await Event.create({
      webinarId: newWebinar.id,
      start: "2020-09-10T17:30:00.000Z",
      end: "2020-09-10T18:00:00.000Z",
      videoUrl: "",
    }).save();

    const response = await api.put<APIResponse<ViewerEventObject>>(
      `/${newWebinar.id}/events/${event.id}`,
      {
        videoUrl: "https://example.com/video.mp4",
      },
      {
        auth: 3,
        team: "team-premium",
      }
    );

    expect(response.statusCode).toEqual(409);
  });

  describe("Product Offers", () => {
    test("Can list product offers", async () => {
      const expected: APIResponse<WebinarOfferObject[]> = {
        data: [
          {
            id: 1,
            name: "Cool Thing",
            start: 900,
            end: 1800,
            price: 2000,
            description: "This cool thing is really cool",
            buttonText: "Click me",
            checkout: "https://emsyte.io",
            thumbnail: "",
          },
        ],
      };
      const response = await api.get("/1/offers", {
        auth: 1,
        team: "team-free",
      });
      expect(response.data).toEqual(expected);
    });
  });
});
