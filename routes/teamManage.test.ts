jest.mock("../controllers/email");
import { APIResponse } from "@emsyte/common/api";
import {
  TeamObject,
  TeamPlan,
  TeamStatus,
  TeamUserObject,
  TeamUserRole,
  TeamUserStatus,
  APIInviteTeamMemberData,
  APIEditTeamMemberData,
} from "@emsyte/common/team";
import { UserStatus } from "@emsyte/common/user";
import { Team } from "../db/entities/Team";
import { serializeTeam } from "../lib/serializers/teamSerializer";
import { getAPICaller } from "../utils/testing/api";
import { handler } from "./teamManage";
import { doSendTeamInvite } from "../controllers/email";
// import { doSendTeamInvite } from "../controllers/email";

describe("Team Manage Routes", () => {
  const caller = getAPICaller(handler);

  test("Can delete team", async () => {
    const response = await caller.delete<APIResponse<TeamObject>>(
      "/",
      {},
      { auth: 7, team: "team4" }
    );

    expect(response.statusCode).toEqual(200);

    expect(await Team.findOne(4)).toBeUndefined();
  });

  test("Can update team", async () => {
    const response = await caller.put<APIResponse<TeamObject>>(
      "/",
      {
        name: "Test Team",
      },
      { auth: 8, team: "team5" }
    );

    expect(response.statusCode).toEqual(200);

    const expected: TeamObject = {
      name: "Test Team",
      plan: TeamPlan.PREMIUM,
      slug: "team5",
      status: TeamStatus.ENABLED,
      billingEmail: "johndoe@example.com",
    };

    expect(serializeTeam(await Team.findOne(5))).toEqual(expected);
  });

  test("Can list team members", async () => {
    const response = await caller.get<APIResponse<TeamUserObject[]>>(
      "/members",
      { auth: 1, team: "team-free" }
    );
    const expected: APIResponse<TeamUserObject[]> = {
      data: [
        {
          status: TeamUserStatus.ACTIVATED,
          role: TeamUserRole.OWNER,
          user: {
            id: 1,
            email: "johndoe1@example.com",
            firstName: "John",
            lastName: "Doe",
            status: UserStatus.ACTIVE,
          },
        },
        {
          status: TeamUserStatus.INVITED,
          role: TeamUserRole.MEMBER,
          user: {
            id: 4,
            email: "johndoe4@example.com",
            firstName: "John",
            lastName: "Doe",
            status: UserStatus.ACTIVE,
          },
        },
        {
          status: TeamUserStatus.ACTIVATED,
          role: TeamUserRole.MEMBER,
          user: {
            id: 5,
            email: "johndoe5@example.com",
            firstName: "John",
            lastName: "Doe",
            status: UserStatus.ACTIVE,
          },
        },
      ],
    };
    expect(response.data).toEqual(expected);
  });

  test("Can remove team member", async () => {
    await caller.delete(
      "/members/3",
      {},
      { auth: 2, team: "team-remove-member" }
    );
    const response = await caller.get<APIResponse<TeamUserObject[]>>(
      "/members",
      { auth: 2, team: "team-remove-member" }
    );

    expect(response.data.data).toEqual([
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.OWNER,
        user: {
          email: "johndoe2@example.com",
          id: 2,
          firstName: "John",
          lastName: "Doe",
          status: UserStatus.ACTIVE,
        },
      },
    ]);
  });

  test("Invite new team member", async () => {
    const update: APIInviteTeamMemberData = {
      email: "johndoe2@example.com",
      role: TeamUserRole.MEMBER,
    };
    await caller.post("/members", update, {
      auth: 1,
      team: "team-invite-member",
    });

    const response = await caller.get<APIResponse<TeamUserObject[]>>(
      "/members",
      { auth: 1, team: "team-invite-member" }
    );

    expect(response.data.data).toEqual([
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.OWNER,
        user: {
          email: "johndoe1@example.com",
          firstName: "John",
          lastName: "Doe",
          id: 1,
          status: UserStatus.ACTIVE,
        },
      },
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.MEMBER,
        user: {
          email: "johndoe2@example.com",
          firstName: "John",
          lastName: "Doe",
          id: 2,
          status: UserStatus.ACTIVE,
        },
      },
    ]);

    expect(doSendTeamInvite).toBeCalled();
  });

  test("Invite new team member (who's not a user)", async () => {
    const update: APIInviteTeamMemberData = {
      email: "not-a-user@example.com",
      role: TeamUserRole.MEMBER,
    };
    await caller.post("/members", update, {
      auth: 1,
      team: "team-invite-member2",
    });

    const response = await caller.get<APIResponse<TeamUserObject[]>>(
      "/members",
      { auth: 1, team: "team-invite-member2" }
    );

    expect(response.data.data).toEqual([
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.OWNER,
        user: {
          email: "johndoe1@example.com",
          firstName: "John",
          lastName: "Doe",
          id: 1,
          status: UserStatus.ACTIVE,
        },
      },
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.MEMBER,
        user: {
          email: "not-a-user@example.com",
          id: expect.any(Number),
          firstName: "",
          lastName: "",
          status: UserStatus.ACTIVE,
        },
      },
    ]);

    expect(doSendTeamInvite).toBeCalled();
  });

  test("Update team member", async () => {
    const update: APIEditTeamMemberData = {
      role: TeamUserRole.ADMIN,
    };
    await caller.put("/members/3", update, {
      auth: 2,
      team: "team-update-member",
    });
    const response = await caller.get<APIResponse<TeamUserObject[]>>(
      "/members",
      { auth: 2, team: "team-update-member" }
    );

    expect(response.data.data).toEqual([
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.OWNER,
        user: {
          email: "johndoe2@example.com",
          id: 2,
          firstName: "John",
          lastName: "Doe",
          status: UserStatus.ACTIVE,
        },
      },
      {
        status: TeamUserStatus.ACTIVATED,
        role: TeamUserRole.ADMIN,
        user: {
          email: "johndoe3@example.com",
          id: 3,
          firstName: "John",
          lastName: "Doe",
          status: UserStatus.ACTIVE,
        },
      },
    ]);
  });
});
