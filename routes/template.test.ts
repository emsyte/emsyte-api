jest.mock("../utils/storage/manager");
import { APIResponse } from "@emsyte/common/api";
import {
  APITemplateInstanceUpdateData,
  TemplateInstanceObject,
  TemplateInstanceObjectFull,
  TemplateObject,
  TemplateType,
} from "@emsyte/common/template";
import { Template, TemplateInstance } from "../db/entities/Template";
import { getAPICaller } from "../utils/testing/api";
import { handler } from "./template";

import { getDriver as _getDriver } from "../utils/storage/manager";
import { StorageDriver } from "../utils/storage/driver";

const getDriver = (_getDriver as unknown) as jest.MockedFunction<
  typeof _getDriver
>;

const BASE_TEMPLATES = [
  {
    id: 1,
    base: true,
    name: "Fancy Registration",
    type: TemplateType.REGISTRATION,
    thumbnail:
      "https://s3.amazonaws.com/io.app.justwebinar/templates/images/2020/October/26/90d89dbdf999eb91/Fancy%20Registration.png",
  },
  {
    id: 2,
    base: true,
    name: "Simple Confirmation",
    type: TemplateType.CONFIRMATION,
    thumbnail:
      "https://s3.amazonaws.com/io.app.justwebinar/template-thumbnail/2020/November/5/0dfb24177bd41003/Simple%20Thank%20you.png",
  },
  {
    id: 3,
    base: true,
    name: "Simple Thank You",
    type: TemplateType.THANK_YOU,
    thumbnail:
      "https://s3.amazonaws.com/io.app.justwebinar/template-thumbnail/2020/November/5/0dfb24177bd41003/Simple%20Thank%20you.png",
  },
];

describe("Templates", () => {
  const api = getAPICaller(handler);

  class MockDriver extends StorageDriver {
    getBaseUrl(): string {
      return "https://s3.amazonaws.com/io.app.justwebinar/";
    }
    async getUploadUrl(_key: string, _contentType: string) {
      return "";
    }
    async delete(_key: string) {}
    async duplicate(_fromKey: string, _toKey: string) {}

    async uploadFile(
      _key: string,
      _contentType: string,
      _content: string | Buffer
    ) {}
    async getFile(_key: string): Promise<Buffer> {
      return Buffer.from("", "ascii");
    }
  }

  const driver = new MockDriver();

  getDriver.mockReturnValue(driver);

  test("List global templates", async () => {
    const expected: APIResponse<TemplateObject[]> = {
      data: BASE_TEMPLATES,
    };
    const response = await api.get("/", { auth: 1, team: "team-free" });
    expect(response.data).toEqual(expected);
  });

  test("Include custom global templates", async () => {
    const expected: APIResponse<TemplateObject[]> = {
      data: [
        ...BASE_TEMPLATES,
        {
          id: 4,
          base: false,
          name: "Custom Registration",
          type: TemplateType.REGISTRATION,
          thumbnail:
            "https://s3.amazonaws.com/io.app.justwebinar/template-thumbnail/2020/November/5/f78d0b28c6ebdf39/Neat%20Registration.png",
        },
      ],
    };
    const response = await api.get("/", { auth: 3, team: "team-premium" });
    expect(response.data).toEqual(expected);
  });

  test("Get webinar template instances", async () => {
    const expected: APIResponse<TemplateInstanceObject[]> = {
      data: [
        {
          id: 1,
          parentId: 1,
          type: TemplateType.REGISTRATION,
          webinarId: 1,
          thumbnail:
            "https://s3.amazonaws.com/io.app.justwebinar/templates/images/2020/October/26/90d89dbdf999eb91/Fancy%20Registration.png",
        },
        {
          id: 4,
          parentId: 2,
          type: TemplateType.CONFIRMATION,
          webinarId: 1,
          thumbnail:
            "https://s3.amazonaws.com/io.app.justwebinar/templates/images/2020/October/26/90d89dbdf999eb91/Fancy%20Registration.png",
        },
      ],
    };
    const response = await api.get("/webinar/1", {
      auth: 1,
      team: "team-free",
    });
    expect(response.data).toEqual(expected);
  });

  test("Create template instance", async () => {
    const expected: APIResponse<TemplateInstanceObject> = {
      data: {
        id: expect.any(Number),
        type: TemplateType.REGISTRATION,
        thumbnail: expect.any(String),
        parentId: 1,
        webinarId: 2,
      },
    };
    const response = await api.post(
      "/webinar/2",
      {
        parentId: 1,
      },
      { auth: 1, team: "team-free" }
    );

    expect(response.data).toEqual(expected);
  });

  test("Update template instance", async () => {
    const data: APITemplateInstanceUpdateData = {
      assets: "[]",
      components: "[]",
      css: "[]",
      html: "[]",
      styles: "[]",
      thumbnail: "thumbnail",
    };

    const expected: APIResponse<TemplateInstanceObject> = {
      data: {
        id: expect.any(Number),
        type: TemplateType.REGISTRATION,
        thumbnail: "thumbnail",
        parentId: 1,
        webinarId: 3,
      },
    };
    const response = await api.put("/instance/2", data, {
      auth: 3,
      team: "team-premium",
    });

    expect(response.data).toEqual(expected);

    const instance = await TemplateInstance.findOne(2);
    expect(instance.content).toEqual(
      JSON.stringify({
        assets: "[]",
        components: "[]",
        css: "[]",
        html: "[]",
        styles: "[]",
      })
    );
  });
  test("Get template instance content", async () => {
    const expected: APIResponse<TemplateInstanceObjectFull> = {
      data: {
        id: 1,
        assets: expect.any(String),
        components: expect.any(String),
        css: expect.any(String),
        html: expect.any(String),
        styles: expect.any(String),
        thumbnail:
          "https://s3.amazonaws.com/io.app.justwebinar/templates/images/2020/October/26/90d89dbdf999eb91/Fancy%20Registration.png",
        type: TemplateType.REGISTRATION,
        parent: BASE_TEMPLATES[0],
        webinarId: 1,
      },
    };

    const response = await api.get("/instance/1", {
      auth: 1,
      team: "team-free",
    });
    expect(response.data).toEqual(expected);
  });

  test("Delete template instance", async () => {
    const response = await api.delete(
      "/instance/3",
      {},
      {
        auth: 1,
        team: "team-free",
      }
    );
    expect(response.statusCode).toEqual(200);
    const instance = await TemplateInstance.findOne(3);
    expect(instance).toBeUndefined();
  });

  test("Reset template instance content", async () => {
    const instance = await TemplateInstance.findOne(4, {
      relations: ["parent"],
    });
    await instance.update({ content: "", thumbnail: "" });

    const response = await api.post(
      "/instance/4/reset",
      {},
      {
        auth: 1,
        team: "team-free",
      }
    );
    expect(response.statusCode).toEqual(200);
    await instance.reload();
    expect(instance.content).toEqual(instance.parent.content);

    // Should reset the template thumbnail
    expect(instance.thumbnail).not.toEqual("");
  });

  test("Save instance as template", async () => {
    const expected: APIResponse<TemplateObject> = {
      data: {
        base: false,
        id: 5,
        name: "Test Template",
        thumbnail: expect.any(String),
        type: TemplateType.REGISTRATION,
      },
    };

    const spy = jest.spyOn(driver, "duplicate");

    const response = await api.post<APIResponse<TemplateObject>>(
      "/",
      {
        instanceId: 1,
        name: "Test Template",
      },
      { auth: 1, team: "team-free" }
    );

    expect(response.data).toEqual(expected);
    expect(spy).toHaveBeenCalledWith(
      "templates/images/2020/October/26/90d89dbdf999eb91/Fancy Registration.png",
      expect.any(String)
    );

    // const instance = await TemplateInstance.findOne(1);
    // const template = await Template.findOne(4);
    // expect(template.content).toEqual(instance.content);
  });

  test("Update global template from instance", async () => {
    const expected: APIResponse<TemplateObject> = {
      data: {
        base: false,
        id: 5,
        name: "Test Template Updated",
        thumbnail: expect.any(String),
        type: TemplateType.REGISTRATION,
      },
    };

    const response = await api.put<APIResponse<TemplateObject>>(
      "/5",
      {
        instanceId: 4,
        name: "Test Template Updated",
      },
      { auth: 1, team: "team-free" }
    );

    expect(response.data).toEqual(expected);
    const instance = await TemplateInstance.findOne(4);
    const template = await Template.findOne(5);
    expect(template.content).toEqual(instance.content);
  });

  test("Update global template name (without changing instance)", async () => {
    const expected: APIResponse<TemplateObject> = {
      data: {
        base: false,
        id: 5,
        name: "Test Template Updated - Again",
        thumbnail: expect.any(String),
        type: TemplateType.REGISTRATION,
      },
    };

    const response = await api.put<APIResponse<TemplateObject>>(
      "/5",
      {
        name: "Test Template Updated - Again",
      },
      { auth: 1, team: "team-free" }
    );

    expect(response.data).toEqual(expected);
    const instance = await TemplateInstance.findOne(4);
    const template = await Template.findOne(5);
    expect(template.content).toEqual(instance.content);
  });

  test("Delete global template", async () => {
    const expected: APIResponse<TemplateObject> = {
      data: {
        base: false,
        name: "Test Template Updated - Again",
        thumbnail: expect.any(String),
        type: TemplateType.REGISTRATION,
      },
    } as any;

    const response = await api.delete<APIResponse<TemplateObject>>(
      "/5",
      {},
      { auth: 1, team: "team-free" }
    );

    expect(response.data).toEqual(expected);
    const template = await Template.findOne(5);
    expect(template).toBeUndefined();
  });
});
