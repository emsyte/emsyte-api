import { APIResponse } from "@emsyte/common/api";
import { UserTeamObject } from "@emsyte/common/team";
import {
  APIChangePasswordData,
  APIUserResponse,
  APIUserUpdateData,
  UserObject,
} from "@emsyte/common/user";
import {
  doChangePassword,
  doDeactivateAccount,
  doGetUser,
  doUpdateUser,
} from "../controllers/users";
import { TeamToUser } from "../db/entities/Team";
import { serializeUserTeam } from "../lib/serializers/teamSerializer";
import { serializeUser } from "../lib/serializers/userSerializer";
import { getResponse } from "../utils/api";
import ServerlessRouter from "../utils/ServerlessRouter";
import {
  ChangePasswordValidator,
  UserUpdateValidator,
} from "../validators/users";

const router = new ServerlessRouter();

router.get<APIResponse<UserTeamObject[]>>("/teams", {}, async ({ userId }) => {
  const teamToUsers = await TeamToUser.find({
    where: { user: { id: userId } },
    relations: ["team"],
  });
  return getResponse(teamToUsers.map(serializeUserTeam));
});

router.put<APIUserUpdateData, APIUserResponse>(
  "/user",
  {
    bodyValidator: UserUpdateValidator,
  },
  async ({ userId, data }) => {
    return await doUpdateUser(userId, data);
  }
);

router.get<APIResponse<UserObject>>("/user", {}, async ({ userId }) => {
  const user = await doGetUser(userId);
  return getResponse(serializeUser(user));
});

router.post<APIChangePasswordData, { message: string }>(
  "/change-password",
  {
    bodyValidator: ChangePasswordValidator,
  },
  async ({ userId, data }) => {
    const response = await doChangePassword(userId, data);
    return response;
  }
);

router.delete<void, { message: string }>("/", {}, async ({ userId }) => {
  const response = await doDeactivateAccount(userId);
  return response;
});

export const handler = router.getHandler();
