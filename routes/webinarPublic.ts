import { APIResponse } from "@emsyte/common/api";
import {
  AuthAttendeeObject,
  AuthType,
  AuthUserObject,
} from "@emsyte/common/auth";
import { TemplateType } from "@emsyte/common/template";
import { EventObject, PublicWebinarObject } from "@emsyte/common/webinar";
import { Type } from "class-transformer";
import { IsNumber, IsOptional, IsString, isUUID } from "class-validator";
import { parse } from "cookie";
import { doRenderPageTemplate } from "../controllers/template";
import {
  doActivateAttendee,
  doCreateAttendeeForEvent,
  getReplayLinkFromToken,
  getViewLinkFromToken,
} from "../controllers/webinars/attendees";
import {
  doGetAllPublicWebinarEvents,
  doGetPublicWebinar,
  doGetPublicWebinarEvent,
} from "../controllers/webinars/public";
import { trackOfferPurchase } from "../controllers/webinars/tracking";
import { Attendee } from "../db/entities/Attendee";
import { Event } from "../db/entities/Event";
import { PageView } from "../db/entities/PageView";
import { Template } from "../db/entities/Template";
import { verifyToken } from "../lib/auth";
import { serializeEvent } from "../lib/serializers/eventSerializer";
import { serializePublicWebinar } from "../lib/serializers/webinarSerializer";
import { getResponse } from "../utils/api";
import { BinaryResponse, RedirectResponse } from "../utils/Response";
import ServerlessRouter from "../utils/ServerlessRouter";
import ValidatorError from "../utils/ValidatorError";
import { getIntFromPath } from "../validators/utils";
import * as jwt from "jsonwebtoken";
import {
  WebinarAttendeeCreateData,
  WebinarAttendeeRegisterData,
} from "../validators/webinars";
// APIs related to registering for a webinar
const router = new ServerlessRouter();

async function getAttendee(token: string) {
  // TODO: Is this a security vulnerability?
  const attendee = jwt.decode(token) as AuthUserObject | AuthAttendeeObject;
  if (attendee.type !== AuthType.ATTENDEE) {
    return;
  }
  return await Attendee.findOne(attendee.id, {
    relations: ["event"],
  });
}

class ActivateQueryValidator {
  @IsOptional()
  @IsString()
  atoken: string;

  @IsOptional()
  @IsString()
  id: string;

  @IsOptional()
  @IsString()
  pageViewKey: string;
}

router.get<
  APIResponse<{
    content: string;
  }>,
  { atoken: string; id: string }
>(
  "/:id/render/:type",
  {
    allowUnauthenticated: true,
    queryValidator: ActivateQueryValidator,
  },
  async ({ pathParameters, query, isFeatureEnabled }) => {
    const webinarId = getIntFromPath(pathParameters, "id");
    const type = pathParameters.type as TemplateType;

    const content = await doRenderPageTemplate(
      webinarId,
      type,
      query.id &&
        query.atoken &&
        (await getTemplateContext(
          type,
          query.id,
          query.atoken,
          isFeatureEnabled("replay")
        ))
    );

    return getResponse({ content });
  }
);

async function getTemplateContext(
  type: TemplateType,
  id: string,
  atoken: string,
  replayEnabled: boolean
) {
  const attendee = await getAttendee(atoken);

  if (type === TemplateType.THANK_YOU) {
    if (!replayEnabled) {
      return {
        event: attendee.event,
        attendee,
      };
    }

    return {
      replayLink: getReplayLinkFromToken(id, atoken),
      event: attendee.event,
      attendee,
    };
  }

  return {
    viewLink: getViewLinkFromToken(id, atoken),
    event: attendee.event,
    attendee,
  };
}

function getTemplate(id: number | undefined) {
  if (!id) {
    return null;
  }

  const template = Template.findOne(id);

  return template ?? null;
}

class PageViewBody {
  @IsOptional()
  @IsString()
  pageViewKey: string;
}

router.post(
  "/:encId/page-view",
  {
    bodyValidator: PageViewBody,
    allowUnauthenticated: true,
  },
  async ({ data, pathParameters }) => {
    const encId = pathParameters.encId;
    const { id, templateId } = JSON.parse(
      Buffer.from(encId, "base64").toString("ascii")
    );

    const template = await getTemplate(templateId);

    const pageView = await PageView.create({
      template,
      webinar: { id },
      key: isUUID(data.pageViewKey) ? data.pageViewKey : undefined,
    }).save();

    return getResponse({ pageViewKey: pageView.key });
  }
);

router.get<APIResponse<PublicWebinarObject>>(
  "/:id",
  {
    allowUnauthenticated: true,
  },
  async ({ pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const webinar = await doGetPublicWebinar(id);

    return getResponse(serializePublicWebinar(webinar));
  }
);

router.get<APIResponse<EventObject[]>>(
  "/:id/events",
  {
    allowUnauthenticated: true,
  },
  async ({ pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");

    const events = await doGetAllPublicWebinarEvents(id);
    return getResponse(events.map(serializeEvent));
  }
);

router.get<APIResponse<EventObject>>(
  "/:id/events/:eventId",
  {
    allowUnauthenticated: true,
  },
  async ({ pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const eventId = getIntFromPath(pathParameters, "eventId");

    const event = await doGetPublicWebinarEvent(id, eventId);
    return getResponse(serializeEvent(event));
  }
);

router.post(
  "/:id/events/:eventId/register",
  {
    bodyValidator: WebinarAttendeeCreateData,
    allowUnauthenticated: true,
  },
  async ({ data, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const eventId = getIntFromPath(pathParameters, "eventId");
    return await doCreateAttendeeForEvent(id, eventId, data);
  }
);

router.post(
  "/register/:enc",
  {
    allowUnauthenticated: true,
    bodyValidator: WebinarAttendeeCreateData,
  },
  async ({ pathParameters, data }) => {
    const enc = pathParameters["enc"];
    const content = JSON.parse(Buffer.from(enc, "base64").toString("ascii"));
    const { eventId } = content;

    const event = await Event.findOne(eventId, {
      relations: ["webinar"],
    });

    await doCreateAttendeeForEvent(event.webinar.id, event.id, data);

    return new RedirectResponse(
      event.webinar.settings.postRegistrationUrl,
      303
    );
  }
);

router.post(
  "/activate",
  {
    bodyValidator: WebinarAttendeeRegisterData,
    allowUnauthenticated: true,
  },
  async ({ data }) => {
    const { type, id } = verifyToken(data.token);
    if (type !== AuthType.ATTENDEE) {
      throw new ValidatorError("Invalid token");
    }

    return await doActivateAttendee(id);
  }
);

class PurchaseValidator {
  @Type(() => Number)
  @IsNumber()
  offerId: number;
}

router.get(
  "/offers/purchase",
  {
    allowUnauthenticated: true,
    queryValidator: PurchaseValidator,
  },
  async ({ headers, query }) => {
    // See: https://github.com/jaredpalmer/TIL/blob/master/Node.js/easy-tracking-pixel.md
    const trackImg = Buffer.from(
      "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
      "base64"
    );

    const cookies = parse(headers.cookie);
    const cookie = cookies[`em-offer-${query.offerId}`];

    // Track event
    const data = await trackOfferPurchase(cookie);
    console.log("offer purchase event", data);

    return new BinaryResponse(trackImg, "image/gif");
  }
);

export const handler = router.getHandler();
