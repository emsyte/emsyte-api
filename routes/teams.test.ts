import { getAPICaller } from "../utils/testing/api";
import { handler } from "./teams";
// import {
//   APIRegisterTeamData,
//   TeamPlan,
//   TeamObject,
//   TeamStatus,
// } from "@emsyte/common/team";
// import { APIResponse } from "@emsyte/common/api";

describe("Team routes", () => {
  const caller = getAPICaller(handler);

  test("Doesn't allow invalid names", async () => {
    const response = await caller.get("check-name?slug=invalid$name");

    expect(response.statusCode).toEqual(422);
    expect(response.data).toEqual({
      errors: expect.anything(),
      message: "Invalid team name",
    });
  });

  test("Rejects taken team names", async () => {
    const response = await caller.get("check-name?slug=team-basic");

    expect(response.statusCode).toEqual(409);
    expect(response.data).toEqual({
      message: "Team name already taken",
    });
  });

  test("Accepts valid team name", async () => {
    const response = await caller.get("check-name?slug=valid-team");

    expect(response.statusCode).toEqual(200);
    expect(response.data).toEqual({
      message: "Team name available",
    });
  });

  // TODO: We can't test this now because it requires a stripe source token
  // test("Can register new team", async () => {
  //   const request: APIRegisterTeamData = {
  //     name: "Test",
  //   };

  //   const expected: APIResponse<TeamObject> = {
  //     data: {
  //       name: "Test",
  //       plan: TeamPlan.FREE,
  //       slug: expect.any(String),
  //       status: TeamStatus.DISABLED,
  //       billingEmail: "johndoe6@example.com",
  //     },
  //   };

  //   const response = await caller.post("register", request, { auth: 6 });
  //   expect(response.data).toEqual(expected);
  // });
});
