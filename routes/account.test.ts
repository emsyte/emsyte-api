import { getAPICaller } from "../utils/testing/api";
import { handler } from "./account";
import { APIResponse } from "@emsyte/common/api";
import {
  UserTeamObject,
  TeamUserRole,
  TeamUserStatus,
  TeamObject,
  TeamPlan,
  TeamStatus,
} from "@emsyte/common/team";

describe("Account", () => {
  const api = getAPICaller(handler);

  test("Get teams", async () => {
    const baseTeam: Omit<TeamObject, "slug"> = {
      billingEmail: "johndoe@example.com",
      name: "Premium Team",
      plan: TeamPlan.PREMIUM,
      status: TeamStatus.ENABLED,
    };
    const expected: APIResponse<UserTeamObject[]> = {
      data: [
        {
          team: {
            ...baseTeam,
            slug: "team7",
          },
          role: TeamUserRole.MEMBER,
          status: TeamUserStatus.ACTIVATED,
        },
        {
          team: {
            ...baseTeam,
            slug: "team8",
          },
          role: TeamUserRole.ADMIN,
          status: TeamUserStatus.ACTIVATED,
        },
        {
          team: {
            ...baseTeam,
            slug: "team9",
          },
          role: TeamUserRole.OWNER,
          status: TeamUserStatus.ACTIVATED,
        },
      ],
    };
    const response = await api.get("/teams", { auth: 11 });

    expect(response.statusCode).toEqual(200);
    expect(response.data).toEqual(expected);
  });
});
