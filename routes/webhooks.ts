import ServerlessRouter from "../utils/ServerlessRouter";
import Stripe from "stripe";
import RouterError from "../utils/RouterError";
import { doHandleReactionReport, Meta } from "../controllers/reactions";
import { EmsyteReport } from "../lib/analyticsService";
const router = new ServerlessRouter();

const { STRIPE_SECRET_KEY, STRIPE_WEBHOOK_SIGNATURE } = process.env;

const stripe = new Stripe(STRIPE_SECRET_KEY, {
  apiVersion: "2020-08-27",
  typescript: true,
});

function getEvent(data: string, signature: string) {
  try {
    return stripe.webhooks.constructEvent(
      data,
      signature,
      STRIPE_WEBHOOK_SIGNATURE
    );
  } catch {
    throw new RouterError(
      "Webhook signature verification failed",
      400,
      "Webhook signature verification failed"
    );
  }
}

// TODO: Setup webhooks
async function handleStripeEvent(event: Stripe.Event) {
  // Handle the event
  // Review important events for Billing webhooks
  // https://stripe.com/docs/billing/webhooks
  // Remove comment to see the various objects sent for this sample
  switch (event.type) {
    case "invoice.paid":
      // Used to provision services after the trial has ended.
      // The status of the invoice will show up as paid. Store the status in your
      // database to reference when a user accesses your service to avoid hitting rate limits.
      break;
    case "invoice.payment_failed":
      // If the payment fails or the customer does not have a valid payment method,
      //  an invoice.payment_failed event is sent, the subscription becomes past_due.
      // Use this webhook to notify your user that their payment has
      // failed and to retrieve new card details.
      break;
    case "invoice.finalized":
      // If you want to manually send out invoices to your customers
      // or store them locally to reference to avoid hitting Stripe rate limits.
      break;
    case "customer.subscription.deleted":
      if (event.request != null) {
        // handle a subscription cancelled by your request
        // from above.
      } else {
        // handle subscription cancelled automatically based
        // upon your subscription settings.
      }
      break;
    case "customer.subscription.trial_will_end":
      if (event.request != null) {
        // handle a subscription cancelled by your request
        // from above.
      } else {
        // handle subscription cancelled automatically based
        // upon your subscription settings.
      }
      break;
    default:
    // Unexpected event type
  }
}

router.post<string, {}>(
  "stripe",
  {
    allowUnauthenticated: true,
    parser: (raw) => raw,
  },
  async ({ data, headers }) => {
    const event = getEvent(data, headers["stripe-signature"]);

    await handleStripeEvent(event);

    return {};
  }
);

router.post<EmsyteReport<Meta>, void>(
  "emsyte",
  { allowUnauthenticated: true },
  async ({ data }) => {
    await doHandleReactionReport(data);
  }
);

export const handler = router.getHandler();
