import { APIResponse } from "@emsyte/common/api";
import {
  APIEditTeamData,
  APIEditTeamMemberData,
  APIInviteTeamMemberData,
  TeamObject,
  TeamUserObject,
} from "@emsyte/common/team";
import { isNumberString, IsString } from "class-validator";
import {
  doCreateCustomer,
  doCreateSubscription,
  doDeactivateSubscription,
  doGetPaymentMethods,
  doGetSubscription,
  doGetTransactionHistory,
  doReplacePaymentMethods,
} from "../controllers/stripe";
import {
  doEditTeamMember,
  doGetTeamMembers,
  doInviteTeamMember,
  doRemoveTeamMember,
} from "../controllers/teamMembers";
import { doDeleteTeam, doEditTeam } from "../controllers/teams";
import {
  serializeTeam,
  serializeTeamUser,
} from "../lib/serializers/teamSerializer";
import { getResponse } from "../utils/api";
import { TeamRouter } from "../utils/TeamRouter";
import ValidatorError from "../utils/ValidatorError";
import {
  CreateCustomerValidator,
  CreateSubscriptionValidator,
} from "../validators/stripe";
import {
  EditTeamMemberValidator,
  EditTeamValidator,
  InviteTeamMemberValidator,
} from "../validators/teams";

const router = new TeamRouter();

router.delete<void, APIResponse<TeamObject>>(
  "/",
  {
    permissions: ["team:delete"],
  },
  async ({ team }) => {
    const _team = await doDeleteTeam(team);
    return getResponse(serializeTeam(_team));
  }
);

router.put<APIEditTeamData, APIResponse<TeamObject>>(
  "/",
  {
    permissions: ["team:edit"],
    bodyValidator: EditTeamValidator,
  },
  async ({ data, team }) => {
    const _team = await doEditTeam(team, data);
    return getResponse(serializeTeam(_team));
  }
);

router.get<APIResponse<TeamUserObject[]>>(
  "/members",
  {
    permissions: ["members:list"],
  },
  async ({ team }) => {
    const teamToUsers = await doGetTeamMembers(team);
    return getResponse(teamToUsers.map(serializeTeamUser));
  }
);

router.post<APIInviteTeamMemberData, APIResponse<TeamUserObject>>(
  "/members",
  {
    bodyValidator: InviteTeamMemberValidator,
    permissions: ["members:invite"],
  },
  async ({ team, userId, data }) => {
    const teamMember = await doInviteTeamMember(team, userId, data);
    return getResponse(serializeTeamUser(teamMember));
  }
);

router.delete<void, {}>(
  "/members/:id",
  {
    permissions: ["members:remove"],
  },
  async ({ team, pathParameters: { id } }) => {
    if (!isNumberString(id, { no_symbols: true })) {
      throw new ValidatorError("Missing fields.", 400);
    }
    await doRemoveTeamMember(team, parseInt(id));
    return {};
  }
);

router.put<APIEditTeamMemberData, APIResponse<TeamUserObject>>(
  "/members/:id",
  {
    bodyValidator: EditTeamMemberValidator,
    permissions: ["members:remove"],
  },
  async ({ team, data, pathParameters: { id } }) => {
    if (!isNumberString(id, { no_symbols: true })) {
      throw new ValidatorError("Missing fields.", 400);
    }
    const teamMember = await doEditTeamMember(team, parseInt(id), data);
    return getResponse(serializeTeamUser(teamMember));
  }
);

// *** Billing Routes ***

// Customers Methods
router.post(
  "/billing/customer",
  {
    bodyValidator: CreateCustomerValidator,
    allowDisabled: true,
    permissions: ["billing:edit"],
  },
  async ({ team, data: { token } }) => {
    return doCreateCustomer(team, token);
  }
);

// Subscription Methods

router.get(
  "/billing/subscription",
  {
    permissions: ["billing:read"],
    allowDisabled: true,
  },
  async ({ team }) => {
    return doGetSubscription(team);
  }
);

router.post(
  "/billing/subscription",
  {
    bodyValidator: CreateSubscriptionValidator,
    permissions: ["billing:edit"],
    allowDisabled: true,
  },
  async ({ team, data }) => {
    const { price, coupon } = data;
    return doCreateSubscription(team, price, coupon);
  }
);

router.delete(
  "/billing/subscription",
  {
    permissions: ["billing:edit"],
    allowDisabled: true,
  },
  async ({ team }) => {
    return doDeactivateSubscription(team);
  }
);

// Transaction History
router.get(
  "/billing/transactions",
  {
    permissions: ["billing:read"],
    allowDisabled: true,
  },
  async ({ team }) => {
    return { charges: await doGetTransactionHistory(team) };
  }
);

// Payments Methods
router.get(
  "/billing/payment-methods",
  {
    permissions: ["billing:read"],
    allowDisabled: true,
  },
  async ({ team }) => {
    return { methods: await doGetPaymentMethods(team) };
  }
);

class UpdatePaymentMethodValidator {
  @IsString()
  source: string;
}

router.put(
  "/billing/payment-methods",
  {
    permissions: ["billing:edit"],
    bodyValidator: UpdatePaymentMethodValidator,
    allowDisabled: true,
  },
  async ({ team, data: { source } }) => {
    return { methods: await doReplacePaymentMethods(team, source) };
  }
);

// TODO: Add fallowing endpoints:
// 1. Endpoint to change billing email

export const handler = router.getHandler();
