import { IsISO8601 } from "class-validator";
import * as moment from "moment";
import { In } from "typeorm";
import {
  doGetDashboardAnalytics,
  doGetEventAnalytics,
  doGetWebinarAnalytics,
} from "../controllers/webinars/analytics";
import { Attendee } from "../db/entities/Attendee";
import { OfferEvent, OfferEventType } from "../db/entities/OfferEvent";
import { PaidRegistration } from "../db/entities/PaidRegistration";
import { ViewerEvent } from "../db/entities/ViewerEvent";
import { serializeAttendee } from "../lib/serializers/attendeeSerializer";
import { getResponse } from "../utils/api";
import { TeamRouter } from "../utils/TeamRouter";
import { getIntFromPath } from "../validators/utils";

const router = new TeamRouter();

// TODO: Analytics reports
router.get("/audience", {}, async ({ team }) => {
  const attendees = await Attendee.createQueryBuilder("attendee")
    .distinctOn(["email"])
    .innerJoin("attendee.event", "event")
    .innerJoin("event.webinar", "webinar")
    .where('webinar."teamId" = :teamId', { teamId: team.id })
    .getMany();

  return getResponse(attendees.map(serializeAttendee));
});

router.get(
  "/audience/:emailId",
  {},
  async ({ team, pathParameters: { emailId } }) => {
    const buff = Buffer.from(emailId, "base64");
    const email = buff.toString("ascii");

    const attendees = await Attendee.createQueryBuilder("attendee")
      .innerJoinAndSelect("attendee.event", "event")
      .innerJoinAndSelect("event.webinar", "webinar")
      .where('webinar."teamId" = :teamId', { teamId: team.id })
      .andWhere("attendee.email = :email", { email })
      .getMany();

    const attendeeIds = attendees.map((a) => a.id);

    const offerEvents = await OfferEvent.createQueryBuilder("offer_event")
      .innerJoinAndSelect("offer_event.offer", "offer")
      .where({
        attendee: In(attendeeIds),
      })
      .getMany();

    const watchTime = await ViewerEvent.createQueryBuilder("viewer_event")
      .select('"attendeeId", MAX(time_offset) as duration')
      .where({
        attendee: In(attendeeIds),
      })
      .groupBy('viewer_event."attendeeId"')
      .getRawMany<{ duration: number; attendeeId: number }>();

    const paidRegistrations = await PaidRegistration.find({
      attendeeId: In(attendeeIds),
    });

    const events = [
      ...offerEvents.map((event) =>
        event.type === OfferEventType.PURCHASE
          ? {
              when: event.createdDate.toISOString(),
              type: "offer_" + event.type,
              name: event.offer.name,
              price: event.price,
            }
          : {
              when: event.createdDate.toISOString(),
              type: "offer_" + event.type,
              name: event.offer.name,
            }
      ),

      ...attendees.map((attendee) => {
        const paidRegistration = paidRegistrations.find(
          (pr) => pr.attendeeId === attendee.id
        );

        return {
          when: attendee.createdDate.toISOString(),
          type: "registration",
          name: attendee.event.webinar.name,
          price: paidRegistration?.registrationFee ?? 0,
        };
      }),

      ...watchTime.map((event) => {
        const attendee = attendees.find((a) => a.id === event.attendeeId);

        return {
          when: attendee.event.start,
          type: "watch_webinar",
          name: attendee.event.webinar.name,
          duration: event.duration,
        };
      }),
    ];

    return getResponse(events.sort((a, b) => compare(a.when, b.when)));
  }
);

function compare<T>(a: T, b: T) {
  if (a < b) {
    return -1;
  } else if (a > b) {
    return 1;
  } else {
    return 0;
  }
}

class WebinarQueryValidator {
  @IsISO8601()
  from: string;

  @IsISO8601()
  to: string;
}

router.get(
  "webinar/:webinarId",
  {
    queryValidator: WebinarQueryValidator,
  },
  async ({ query, pathParameters, team }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const from = moment(query.from);
    const to = moment(query.to);

    const analytics = await doGetWebinarAnalytics(team, webinarId, {
      from,
      to,
    });
    return getResponse(analytics);
  }
);

router.get("event/:eventId", {}, async ({ pathParameters }) => {
  const eventId = getIntFromPath(pathParameters, "eventId");

  const analytics = await doGetEventAnalytics(eventId);
  return getResponse(analytics);
});

class DashboardQueryValidator {
  @IsISO8601()
  from: string;

  @IsISO8601()
  to: string;
}

router.get(
  "dashboard",
  {
    queryValidator: DashboardQueryValidator,
  },
  async ({ team, query }) => {
    const from = moment(query.from);
    const to = moment(query.to);

    const analytics = await doGetDashboardAnalytics(team, { from, to });
    return getResponse(analytics);
  }
);

export const handler = router.getHandler();
