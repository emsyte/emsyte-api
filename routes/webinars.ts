import { APIResponse } from "@emsyte/common/api";
import {
  APINotificationCreateData,
  APINotificationUpdateData,
  NotificationObject,
} from "@emsyte/common/notification";
import {
  APINotificationGatewaySetData,
  APIWebinarAddRecurrenceData,
  APIWebinarChatCreateData,
  APIWebinarChatUpdateData,
  APIWebinarCreateData,
  APIWebinarOfferCreateData,
  APIWebinarUpdateData,
  CalendarEventObject,
  EventObject,
  ViewerEventObject,
  WebinarChatObject,
  WebinarObject,
  WebinarOfferObject,
  RecurrenceObject,
  APIWebinarAddEventVideoUrl,
} from "@emsyte/common/webinar";
import { isNumberString } from "class-validator";
import * as parse from "csv-parse";
import moment = require("moment");
import {
  doCreateWebinar,
  doCreateWebinarOffer,
  doDeleteWebinar,
  doGetAllEvents,
  doGetAllWebinarEvents,
  doGetAllWebinarOffers,
  doGetAllWebinars,
  doGetWebinar,
  doGetWebinarOffer,
  doUpdateWebinar,
} from "../controllers/webinars";
import {
  doActivateWebinar,
  doArchiveWebinar,
} from "../controllers/webinars/base";
import {
  doCreateWebinarChatMessage,
  doDeleteChatMessage,
  doGetAllWebinarChatMessages,
  doGetChatMessage,
  doUpdateWebinarChatMessage,
  doUploadWebinarChatMessage,
} from "../controllers/webinars/chat";
import {
  doCreateRecurrence,
  doDeleteRecurrence,
  doGetRecurrences,
  doGetSchedule,
  doGetWebinarEvent,
  doSetEventUrl,
} from "../controllers/webinars/events";
import {
  doGetNotificationGatewayFields,
  doRemoveNotificationGateway,
  doSetNotificationGateway,
} from "../controllers/webinars/gateway";
import {
  doCreateNotification,
  doDeleteNotification,
  doGetNotifications,
  doUpdateNotification,
} from "../controllers/webinars/notifications";
import { doUpdateWebinarOffer } from "../controllers/webinars/offers";
import { serializeChat } from "../lib/serializers/chatSerializer";
import {
  serializeCalendarEvent,
  serializeEvent,
  serializeViewerEvent,
} from "../lib/serializers/eventSerializer";
import { serializeNotification } from "../lib/serializers/notificationSerializer";
import { serializeOffer } from "../lib/serializers/offerSerializer";
import {
  serializeRecurrence,
  serializeWebinar,
} from "../lib/serializers/webinarSerializer";
import { getResponse } from "../utils/api";
import { TeamRouter } from "../utils/TeamRouter";
import ValidatorError from "../utils/ValidatorError";
import { getIntFromPath } from "../validators/utils";
import {
  CalendarEventsValidator,
  NotificationCreateValidator,
  NotificationGatewayFieldsQueryValidator,
  NotificationGatewayQueryValidator,
  NotificationGatewaySetValidator,
  NotificationUpdateValidator,
  WebinarAddEventVideoUrlData,
  WebinarAddRecurrenceData,
  WebinarChatCreateValidator,
  WebinarChatUpdateValidator,
  WebinarChatUploadValidator,
  WebinarCreateData,
  WebinarOfferCreateData,
  WebinarUpdateData,
} from "../validators/webinars";

const router = new TeamRouter();

router.post<APIWebinarCreateData, APIResponse<WebinarObject>>(
  "/",
  {
    bodyValidator: WebinarCreateData,
    bodyValidationMessage: "Webinar validation failed",
    permissions: ["webinar:create"],
  },
  async ({ data, team }) => {
    const webinar = await doCreateWebinar(team, data);
    return getResponse(serializeWebinar(webinar));
  }
);

router.put<APIWebinarUpdateData, APIResponse<WebinarObject>>(
  "/:id",
  {
    bodyValidator: WebinarUpdateData,
    bodyValidationMessage: "Webinar validation failed",
    permissions: ["webinar:edit"],
  },
  async ({ data, team, pathParameters: { id } }) => {
    if (!isNumberString(id, { no_symbols: true })) {
      throw new ValidatorError("Missing fields.", 400);
    }

    const webinar = await doUpdateWebinar(team, parseInt(id), data);
    return getResponse(serializeWebinar(webinar));
  }
);

router.post<{}, APIResponse<WebinarObject>>(
  "/:id/activate",
  {},
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const webinar = await doActivateWebinar(team, id);

    return getResponse(serializeWebinar(webinar));
  }
);

router.post<{}, APIResponse<WebinarObject>>(
  "/:id/archive",
  {},
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const webinar = await doArchiveWebinar(team, id);

    return getResponse(serializeWebinar(webinar));
  }
);

router.delete<void, APIResponse<WebinarObject>>(
  "/:id",
  {
    permissions: ["webinar:delete"],
  },
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const webinar = await doDeleteWebinar(team, id);
    return getResponse(serializeWebinar(webinar));
  }
);

router.get<APIResponse<WebinarObject>>(
  "/:id",
  {
    permissions: ["webinar:read"],
  },
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const webinar = await doGetWebinar(team, id);
    return getResponse(serializeWebinar(webinar));
  }
);

router.get<APIResponse<WebinarObject[]>>(
  "/",
  {
    permissions: ["webinar:list"],
  },
  async ({ team }) => {
    const webinars = await doGetAllWebinars(team);
    return getResponse(webinars.map(serializeWebinar));
  }
);

router.post<APIWebinarAddRecurrenceData, APIResponse<RecurrenceObject>>(
  ":id/recurrences",
  {
    bodyValidator: WebinarAddRecurrenceData,
    permissions: ["webinar:schedule"],
  },
  async ({ team, data, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const recurrence = await doCreateRecurrence(team, id, data);
    return getResponse(serializeRecurrence(recurrence));
  }
);

router.delete<{}, APIResponse<RecurrenceObject>>(
  ":id/recurrences/:recurrenceId",
  {
    permissions: ["webinar:schedule"],
  },
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const recurrenceId = getIntFromPath(pathParameters, "recurrenceId");
    const recurrence = await doDeleteRecurrence(team, id, recurrenceId);

    return getResponse(serializeRecurrence(recurrence));
  }
);

router.get<APIResponse<RecurrenceObject[]>>(
  ":id/recurrences",
  {},
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const recurrences = await doGetRecurrences(team, id);
    return getResponse(recurrences.map(serializeRecurrence));
  }
);

// // Events
// router.post<APIWebinarScheduleData, APIResponse<EventObject[]>>(
//   "/:id/events",
//   {
//     bodyValidator: WebinarScheduleData,
//     permissions: ["webinar:schedule"],
//   },
//   async ({ data, pathParameters }) => {
//     const id = getIntFromPath(pathParameters, "id");
//     const events = await doScheduleWebinarEvents(id, data);
//     return getResponse(events.map(serializeEvent));
//   }
// );

router.get<APIResponse<EventObject[]>>(
  "/:id/events",
  {
    permissions: ["webinar:read"],
  },
  async ({ pathParameters: { id } }) => {
    if (!isNumberString(id, { no_symbols: true })) {
      throw new ValidatorError("Missing fields.", 400);
    }
    const events = await doGetAllWebinarEvents(parseInt(id));
    return getResponse(events.map(serializeEvent));
  }
);

router.get<APIResponse<ViewerEventObject>>(
  "/:id/events/:eventId",
  {
    permissions: ["webinar:read"],
  },
  async ({ team, pathParameters }) => {
    const id = getIntFromPath(pathParameters, "id");
    const eventId = getIntFromPath(pathParameters, "eventId");
    const event = await doGetWebinarEvent(team, id, eventId);

    return getResponse(serializeViewerEvent(event));
  }
);

router.put<APIWebinarAddEventVideoUrl, APIResponse<ViewerEventObject>>(
  "/:id/events/:eventId",
  {
    bodyValidator: WebinarAddEventVideoUrlData,
  },
  async ({ team, pathParameters, data }) => {
    const id = getIntFromPath(pathParameters, "id");
    const eventId = getIntFromPath(pathParameters, "eventId");
    const event = await doSetEventUrl(team, id, eventId, data);

    return getResponse(serializeViewerEvent(event));
  }
);

router.get<
  APIResponse<{
    events: CalendarEventObject[];
    recurrences: RecurrenceObject[];
  }>,
  { from?: string; to?: string; limit?: number; order?: "ASC" | "DESC" }
>(
  "/schedule",
  {
    permissions: ["webinar:list"],
    queryValidator: CalendarEventsValidator,
  },
  async ({ team, query }) => {
    const pagination =
      query.from && query.to
        ? { from: moment(query.from), to: moment(query.to) }
        : query.limit;

    const { events, recurrences } = await doGetSchedule(
      team,
      pagination,
      query.order
    );
    return getResponse({
      events: events.map(serializeCalendarEvent),
      recurrences: recurrences.map(serializeRecurrence),
    });
  }
);

router.get<
  APIResponse<CalendarEventObject[]>,
  { from?: string; to?: string; limit?: number; order?: "ASC" | "DESC" }
>(
  "/events",
  {
    permissions: ["webinar:list"],
    queryValidator: CalendarEventsValidator,
  },
  async ({ team, query }) => {
    const pagination =
      query.from && query.to
        ? { from: moment(query.from), to: moment(query.to) }
        : query.limit;

    const events = await doGetAllEvents(team, pagination, query.order);
    return getResponse(events.map(serializeCalendarEvent));
  }
);

// Offers
router.post<APIWebinarOfferCreateData, APIResponse<WebinarOfferObject>>(
  "/:id/offers",
  {
    bodyValidator: WebinarOfferCreateData,
    permissions: ["product-offer:create"],
  },
  async ({ data, pathParameters: { id } }) => {
    if (!isNumberString(id, { no_symbols: true })) {
      throw new ValidatorError("Missing fields.", 400);
    }

    // TODO: Create a put endpoint to update offers
    const offer = await ("id" in data
      ? doUpdateWebinarOffer((data as any).id, data)
      : doCreateWebinarOffer(parseInt(id), data));

    return getResponse(serializeOffer(offer));
  }
);

router.get<APIResponse<WebinarOfferObject[]>>(
  "/:id/offers",
  {
    permissions: ["product-offer:list"],
  },
  async ({ pathParameters: { id } }) => {
    if (!isNumberString(id, { no_symbols: true })) {
      throw new ValidatorError("Missing fields.", 400);
    }
    const offers = doGetAllWebinarOffers(parseInt(id));
    return getResponse((await offers).map(serializeOffer));
  }
);

router.get<APIResponse<WebinarOfferObject>>(
  "/:id/offers/:offerId",
  {
    permissions: ["product-offer:read"],
  },
  async ({ pathParameters: { id, offerId } }) => {
    if (
      !isNumberString(id, { no_symbols: true }) ||
      !isNumberString(offerId, { no_symbols: true })
    ) {
      throw new ValidatorError("Missing fields.", 400);
    }
    const offer = await doGetWebinarOffer(parseInt(id), parseInt(offerId));
    return getResponse(serializeOffer(offer));
  }
);

// Notifications
router.post<APINotificationCreateData, APIResponse<NotificationObject>>(
  "/:webinarId/notifications",
  {
    bodyValidator: NotificationCreateValidator,
    // TODO:
    permissions: [],
  },
  async ({ team, data, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const notification = await doCreateNotification(team, webinarId, data);
    return getResponse(serializeNotification(notification));
  }
);

router.get<APIResponse<NotificationObject[]>>(
  "/:webinarId/notifications",
  {
    // TODO:
    permissions: [],
  },
  async ({ team, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const notifications = await doGetNotifications(team, webinarId);
    return getResponse(notifications.map(serializeNotification));
  }
);

function getNotificationGatewayType(type: "email" | "sms") {
  const mapping = {
    sms: "smsGateway",
    email: "emailGateway",
  } as const;

  return mapping[type];
}

router.post<
  APINotificationGatewaySetData,
  APIResponse<WebinarObject>,
  { type: "email" | "sms" }
>(
  "/:webinarId/gateway",
  {
    // TODO:
    permissions: [],
    bodyValidator: NotificationGatewaySetValidator,
    queryValidator: NotificationGatewayQueryValidator,
  },
  async ({ team, data, query, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const webinar = await doSetNotificationGateway(
      team,
      webinarId,
      getNotificationGatewayType(query.type),
      data
    );
    return getResponse(serializeWebinar(webinar));
  }
);

router.delete(
  "/:webinarId/gateway",
  {
    // TODO:
    permissions: [],
    queryValidator: NotificationGatewayQueryValidator,
  },
  async ({ team, query, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const webinar = await doRemoveNotificationGateway(
      team,
      webinarId,
      getNotificationGatewayType(query.type)
    );
    return getResponse(serializeWebinar(webinar));
  }
);

router.get(
  "/gateway/fields",
  {
    // TODO:
    permissions: [],
    queryValidator: NotificationGatewayFieldsQueryValidator,
  },
  async ({ team, query }) => {
    const fields = await doGetNotificationGatewayFields(team, query.provider);
    return getResponse(fields);
  }
);

router.put<APINotificationUpdateData, APIResponse<NotificationObject>>(
  "/:webinarId/notifications/:notificationId",
  {
    bodyValidator: NotificationUpdateValidator,
    // TODO:
    permissions: [],
  },
  async ({ team, data, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const notificationId = getIntFromPath(pathParameters, "notificationId");
    const notification = await doUpdateNotification(
      team,
      webinarId,
      notificationId,
      data
    );
    return getResponse(serializeNotification(notification));
  }
);

router.delete<void, {}>(
  "/:webinarId/notifications/:notificationId",
  {
    // TODO:
    permissions: [],
  },
  async ({ team, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const notificationId = getIntFromPath(pathParameters, "notificationId");
    await doDeleteNotification(team, webinarId, notificationId);

    return {};
  }
);

// router.put("/:id/offers/:offerId", async (event) => {
//   const { id, offerId } = webinarsValidator.getWebinarOffer(event);
//   return doUpdateWebinarOffer(id, offerId);
// });

// Saved chat messages
router.get<APIResponse<WebinarChatObject[]>>(
  "/:webinarId/chats",
  {},
  async ({ team, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const chatMessages = await doGetAllWebinarChatMessages(team, webinarId);
    return getResponse(chatMessages.map(serializeChat));
  }
);

router.get<APIResponse<WebinarChatObject>>(
  "/:webinarId/chats/:chatId",
  {},
  async ({ team, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const chatId = getIntFromPath(pathParameters, "chatId");
    const chatMessage = await doGetChatMessage(team, webinarId, chatId);

    return getResponse(serializeChat(chatMessage));
  }
);

router.post<APIWebinarChatCreateData, APIResponse<WebinarChatObject>>(
  "/:webinarId/chats",
  {
    bodyValidator: WebinarChatCreateValidator,
  },
  async ({ team, data, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const chatMessages = await doCreateWebinarChatMessage(
      team,
      webinarId,
      data
    );

    return getResponse(serializeChat(chatMessages));
  }
);

router.put<APIWebinarChatUpdateData, APIResponse<WebinarChatObject>>(
  "/:webinarId/chats/:chatId",
  {
    bodyValidator: WebinarChatUpdateValidator,
  },
  async ({ team, data, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const chatId = getIntFromPath(pathParameters, "chatId");
    const chatMessages = await doUpdateWebinarChatMessage(
      team,
      webinarId,
      chatId,
      data
    );

    return getResponse(serializeChat(chatMessages));
  }
);

router.post<
  { records: APIWebinarChatCreateData[] },
  APIResponse<{ message: string }>
>(
  "/:webinarId/chats/upload",
  {
    parser: async (event) => {
      const parser = parse(event.body, {
        columns: true,
      });
      const records = [];

      for await (const record of parser) {
        records.push(record);
      }

      return { records };
    },
    bodyValidator: WebinarChatUploadValidator,
  },
  async ({ team, data, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    await doUploadWebinarChatMessage(team, webinarId, data.records);

    return getResponse({ message: "Successfully uploaded chat messages" });
  }
);

router.delete<void, {}>(
  "/:webinarId/chats/:chatId",
  {},
  async ({ team, pathParameters }) => {
    const webinarId = getIntFromPath(pathParameters, "webinarId");
    const chatId = getIntFromPath(pathParameters, "chatId");
    await doDeleteChatMessage(team, webinarId, chatId);

    return {};
  }
);

export const handler = router.getHandler();
