const path = require("path");
const slsw = require("serverless-webpack");
const fs = require("fs");
const TerserPlugin = require("terser-webpack-plugin");
const nodeExternals = require("webpack-node-externals");
const webpack = require("webpack");
const SentryWebpackPlugin = require("@sentry/webpack-plugin");

const nodeModules = {};
fs.readdirSync("node_modules")
  .filter((item) => [".bin"].indexOf(item) === -1) // exclude the .bin folder
  .forEach((mod) => {
    nodeModules[mod] = "commonjs " + mod;
  });

const commitHash = require("child_process")
  .execSync("git rev-parse HEAD")
  .toString()
  .trim();

module.exports = {
  mode: slsw.lib.webpack.isLocal ? "development" : "production",
  entry: slsw.lib.entries,
  devtool: "source-map",
  externals: [nodeModules, nodeExternals()],
  resolve: {
    extensions: [".js", ".jsx", ".json", ".ts", ".tsx"],
  },
  optimization: {
    minimize: false,
    // minimizer: [
    //   new TerserPlugin({
    //     sourceMap: true,
    //     terserOptions: {
    //       // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
    //       keep_classnames: true,
    //     },
    //   }),
    // ],
  },
  output: {
    libraryTarget: "commonjs",
    path: path.join(__dirname, ".webpack"),
    filename: "[name].js",
  },
  target: "node",
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: { transpileOnly: true },
      },
      { test: /\.handlebars$/, loader: "handlebars-loader" },
      {
        parser: {
          amd: false,
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.COMMIT_HASH": JSON.stringify(commitHash),
      "process.env.BUILD_DATE": JSON.stringify(new Date().toISOString()),
      "process.env.APP_ENV": JSON.stringify(slsw.lib.options.stage),
      "process.env.SENTRY_RELEASE": JSON.stringify(commitHash),
    }),
    ...(slsw.lib.webpack.isLocal
      ? []
      : [
          new SentryWebpackPlugin({
            // sentry-cli configuration
            // authToken: process.env.SENTRY_AUTH_TOKEN,

            // TODO: This is bad and you know it
            authToken:
              "aa1dc61c9ebd4e4497c77b15ba64b8723cbc65a3f341493f8a98dfb87d089055",

            org: "emsyte-llc",
            project: "emsyte-api",
            release: commitHash,

            // webpack specific configuration
            include: ".",
            ignore: ["node_modules", "webpack.config.js"],
          }),
        ]),
  ],
};
