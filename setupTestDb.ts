import { config } from "dotenv";
import { getConnection, Connection } from "typeorm";
import initializeDatabase from "./db/connection";

import * as path from "path";
import {
  Builder,
  fixturesIterator,
  Loader,
  Parser,
  Resolver,
} from "typeorm-fixtures-cli/dist";

config({
  path: ".env.test",
});

const FIXTURES_PATH = "./db/fixtures";

async function loadFixtures(connection: Connection) {
  const loader = new Loader();
  loader.load(path.resolve(FIXTURES_PATH));

  const resolver = new Resolver();
  const fixtures = resolver.resolve(loader.fixtureConfigs);
  const builder = new Builder(connection, new Parser());

  for (const fixture of fixturesIterator(fixtures)) {
    const entity = await builder.build(fixture);
    await connection.getRepository(entity.constructor.name).save(entity);
  }
}

export default async function setupDatabase() {
  // Set timezone to UTC
  process.env.TZ = "UTC";

  console.log("Setting up test database");
  const connection = await createConnection();
  await connection.synchronize(true);
  await loadFixtures(connection);
  // Why does this print an error to the console?
  // await connection.close();
  console.log("Database ready");
}

async function createConnection() {
  try {
    const existingConnection = getConnection();

    if (existingConnection.isConnected) {
      return existingConnection;
    } else {
      existingConnection.close();
    }
  } catch {}

  console.log("Creating new connection");
  try {
    return await initializeDatabase({
      database: "test",
      synchronize: false,
    });
  } catch (e) {
    console.log("Creating new database");
    const connection = await initializeDatabase({
      synchronize: false,
    });
    await connection.query("CREATE DATABASE test;");
    await connection.close();

    return await initializeDatabase({
      database: "test",
      synchronize: false,
    });
  }
}
