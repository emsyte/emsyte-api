import { config } from "dotenv";
import { Connection } from "typeorm";
import initializeDatabase, { getEnvConnectionOptions } from "./db/connection";

config({
  path: ".env.test",
});

let connection: Connection;

beforeAll(async () => {
  // Don't use production database for tests!
  const options = getEnvConnectionOptions();
  if (options.type === "aurora-data-api-pg") {
    throw Error("Attempting to use production or staging database for tests!");
  }

  connection = await initializeDatabase({
    database: "test",
  });
});

afterAll(async () => {
  await connection.close();
});
