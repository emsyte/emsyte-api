module.exports = {
  roots: ["<rootDir>"],
  globalSetup: "<rootDir>/setupTestDb.ts",
  setupFilesAfterEnv: ["<rootDir>/setupTests.ts"],
  collectCoverageFrom: ["**/*.{ts,tsx}", "!**/db/migration/**"],
  testEnvironment: "node",
  testMatch: [
    "**/__tests__/**/*.+(ts|tsx|js)",
    "**/?(*.)+(spec|test).+(ts|tsx|js)",
  ],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
    "^.+\\.(handlebars)$": "<rootDir>/config/jest/handlebarsTransform.js",
  },
};
