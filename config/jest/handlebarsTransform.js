module.exports = {
  process() {
    return "const hbs = require('handlebars'); module.exports = hbs.compile('');";
  },
  getCacheKey() {
    return "handlebarsTransform";
  },
};
