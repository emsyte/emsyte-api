// Emsyte API URL

const BASE_URL =
  "https://kioyre7wn5.execute-api.us-east-1.amazonaws.com/production";

// const BASE_URL = "http://localhost:8000";

// TODO: Pull from environment variables
const API_KEY =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYmY2MmE4NzYtZmY0Yy00ODlmLWFhMmQtNTI1ZGUxZWEyOGEwIn0.cMiRwH7RhommaVMb8pURX8M29UEYDUmz_l7N6vgtfk4";

export type EmsyteAlgorithm = "attention" | "emotion";

export interface EmsyteReport<M> {
  meta: M;
  algorithms: EmsyteAlgorithm[];
  measurements_per_second: number;
  report: Record<EmsyteAlgorithm, any>;
}

import Axios from "axios";

const client = Axios.create({
  baseURL: BASE_URL,
  headers: {
    Authorization: "Bearer " + API_KEY,
  },
});

interface CreateObservationResponse {
  key: string;
}

async function start(algorithms: EmsyteAlgorithm[]) {
  const response = await client.post<CreateObservationResponse>(
    "/observation",
    { algorithms }
  );
  return response.data;
}

interface GetUploadUrlResponse {
  upload_to: string;
}

async function getUploadUrl(observation: string, start: number) {
  const response = await client.get<GetUploadUrlResponse>(
    `/observation/${observation}/get-upload-url?start=${start}`
  );

  return response.data;
}

interface FinalizeObservationData {
  meta?: Record<string, string>;
  callback?: string;
}

interface FinalizeObservationResponse {}

async function finalize(observation: string, data: FinalizeObservationData) {
  const response = await client.post<FinalizeObservationResponse>(
    `/observation/${observation}/finalize`,
    data
  );

  return response.data;
}

const AnalyticsService = {
  start,
  finalize,
  getUploadUrl,
};

export default AnalyticsService;
