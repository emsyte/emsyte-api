import { Event } from "../../db/entities/Event";
import {
  CalendarEventObject,
  EventObject,
  ViewerEventObject,
} from "@emsyte/common/webinar";
import { serializeWebinar } from "./webinarSerializer";

export function serializeEvent(event: Event): EventObject {
  const { id, start, end, canceled, recurrenceId, videoUrl } = event;

  return {
    id,
    start: new Date(start).toISOString(),
    end: new Date(end).toISOString(),
    videoUrl,
    canceled,
    recurrenceId,
  };
}

export function serializeViewerEvent(event: Event): ViewerEventObject {
  const { videoUrl, ...webinar } = serializeWebinar(event.webinar);

  return { ...serializeEvent(event), webinar };
}

export function serializeCalendarEvent(
  event: Event & { registered: number; attending: number }
): CalendarEventObject {
  return {
    ...serializeEvent(event),
    webinar: serializeWebinar(event.webinar),
    registered: event.registered,
    attending: event.attending,
  };
}
