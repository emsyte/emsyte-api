import { WebinarChatObject } from "@emsyte/common/webinar";
import { ChatMessage } from "../../db/entities/ChatMessage";

export function serializeChat(chat: ChatMessage): WebinarChatObject {
  const { id, offset_timestamp, label, name, content } = chat;
  return { id, offset_timestamp, label, name, content };
}
