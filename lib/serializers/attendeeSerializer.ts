import { Attendee } from "../../db/entities/Attendee";
import { AttendeeObject, AttendeeObjectFull } from "@emsyte/common/attendee";
import { AuthType, AuthAttendeeObject } from "@emsyte/common/auth";
import { serializeViewerEvent } from "./eventSerializer";

export function serializeAttendee(attendee: Attendee): AttendeeObject {
  const {
    id,
    firstName,
    lastName,
    timezone,
    email,
    active,
    phone,
    createdDate,
    updatedDate,
  } = attendee;

  return {
    id,
    firstName,
    lastName,
    timezone,
    email,
    active,
    phone,
    createdDate: createdDate.toISOString(),
    updatedDate: updatedDate.toISOString(),
  };
}

export function serializeAttendeeFull(attendee: Attendee): AttendeeObjectFull {
  return {
    ...serializeAttendee(attendee),
    event: serializeViewerEvent(attendee.event),
  };
}

export function serializeAuthAttendee(attendee: Attendee): AuthAttendeeObject {
  return {
    ...serializeAttendee(attendee),
    type: AuthType.ATTENDEE,
  };
}
