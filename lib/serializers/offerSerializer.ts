import { WebinarOfferObject } from "@emsyte/common/webinar";
import { Offer } from "../../db/entities/Offer";

export function serializeOffer(offer: Offer): WebinarOfferObject {
  const {
    id,
    start,
    end,
    name,
    checkout,
    buttonText,
    description,
    price,
    thumbnail,
  } = offer;
  return {
    id,
    start,
    end,
    name,
    checkout,
    buttonText,
    description,
    price: +price,
    thumbnail,
  };
}
