import { Team, TeamToUser } from "../../db/entities/Team";
import {
  TeamObject,
  UserTeamObject,
  TeamUserObject,
} from "@emsyte/common/team";
import { serializeUser } from "./userSerializer";

export function serializeTeam(team: Team): TeamObject {
  const { name, slug, billingEmail, plan, status } = team;
  return { name, slug, billingEmail, plan, status };
}

export function serializeUserTeam(teamToUser: TeamToUser): UserTeamObject {
  const { team, role, status } = teamToUser;

  return {
    role,
    status,
    team: serializeTeam(team),
  };
}

export function serializeTeamUser(teamToUser: TeamToUser): TeamUserObject {
  const { user, role, status } = teamToUser;

  return {
    role,
    status,
    user: serializeUser(user),
  };
}
