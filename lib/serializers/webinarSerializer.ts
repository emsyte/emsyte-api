import { INTEGRATIONS } from "@emsyte/common/integration";
import {
  NotificationGatewayObject,
  PublicWebinarObject,
  PublicWebinarSettingsObject,
  WebinarObject,
  RecurrenceObject,
  WebinarSettingsObject,
} from "@emsyte/common/webinar";
import { Integration } from "../../db/entities/Integration";
import { NotificationGateway } from "../../db/entities/NotificationsGateway";
import { Recurrence, Webinar } from "../../db/entities/Webinar";
import { WebinarSettings } from "../../db/entities/WebinarSettings";
import * as moment from "moment-timezone";

export function serializeWebinar(webinar: Webinar): WebinarObject {
  const {
    id,
    name,
    type,
    description,
    videoUrl,
    duration,
    replayExpiration,
    thumbnail,
    isPaid,
    registrationFee,
    checkoutLink,
    redirectLink,
    status,
    settings,
  } = webinar;

  return {
    id,
    name,
    type,
    description,
    videoUrl,
    duration,
    replayExpiration,
    thumbnail,
    isPaid,
    registrationFee,
    checkoutLink,
    redirectLink,
    status,
    settings: serializeSettings(settings),

    emailGateway:
      webinar.emailGateway &&
      serializeNotificationGateway(webinar.emailGateway),
    smsGateway:
      webinar.smsGateway && serializeNotificationGateway(webinar.smsGateway),
  };
}

export function serializePublicWebinar(webinar: Webinar): PublicWebinarObject {
  const {
    id,
    name,
    type,
    description,
    thumbnail,
    isPaid,
    registrationFee,
    checkoutLink,
    redirectLink,
  } = webinar;

  return {
    id,
    name,
    type,
    description,
    thumbnail,
    isPaid,
    registrationFee,
    checkoutLink,
    redirectLink,

    settings: serializePublicSettings(webinar.settings),
  };
}

function serializeSettings(settings: WebinarSettings): WebinarSettingsObject {
  return settings;
}

function serializePublicSettings(
  settings: WebinarSettings
): PublicWebinarSettingsObject {
  const { registrationFormScript } = settings;
  return { registrationFormScript };
}

function serializeNotificationGateway(
  settings: NotificationGateway
): NotificationGatewayObject {
  return {
    integration: serializeIntegration(settings.integration),
    settings: settings.settings,
  };
}

function serializeIntegration(integration: Integration) {
  const def = INTEGRATIONS.find((i) => i.provider === integration.provider);

  return {
    ...def,
    settings: integration.settings,
    active: true,
  };
}

export function serializeRecurrence(recurrence: Recurrence): RecurrenceObject {
  const { days, start, end, id, frequency, timezone, webinarId } = recurrence;
  return {
    days,
    start: moment(start).tz(timezone, true).format("YYYY-MM-DD HH:mm"),
    end: moment(end).tz(timezone, true).format("YYYY-MM-DD"),
    id,
    frequency,
    timezone,
    webinarId,
  };
}
