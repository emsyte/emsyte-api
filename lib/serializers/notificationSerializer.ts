import { NotificationObject } from "@emsyte/common/notification";
import { Notification } from "../../db/entities/Notification";

export function serializeNotification(
  notification: Notification
): NotificationObject {
  const {
    id,
    type,
    deliveryMethod,
    subject,
    content,
    hoursOffset,
  } = notification;
  return { id, type, deliveryMethod, subject, content, hoursOffset };
}
