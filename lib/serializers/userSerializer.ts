import { User } from "../../db/entities/User";
import { UserObject } from "@emsyte/common/user";
import { AuthType, AuthUserObject } from "@emsyte/common/auth";

export function serializeUser(user: User): UserObject {
  const { id, email, status, firstName, lastName } = user;
  return { id, email, status, firstName, lastName };
}

export function serializeAuthUser(user: User): AuthUserObject {
  return {
    id: user.id,
    type: AuthType.USER,
    ...serializeUser(user),
  };
}
