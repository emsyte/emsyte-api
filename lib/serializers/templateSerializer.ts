import {
  TemplateInstanceObject,
  TemplateInstanceObjectFull,
  TemplateObject,
  TemplateObjectFull,
} from "@emsyte/common/template";
import { Template, TemplateInstance } from "../../db/entities/Template";

export function serializeTemplate(template: Template): TemplateObject {
  const { name, type, thumbnail, base, id } = template;
  return { name, type, thumbnail, base, id };
}

export function serializeTemplateFull(template: Template): TemplateObjectFull {
  const { content } = template;
  const parsed = JSON.parse(content);

  return {
    ...serializeTemplate(template),
    ...parsed,
  };
}

export function serializeTemplateInstance(
  template: TemplateInstance
): TemplateInstanceObject {
  const { type, thumbnail, parentId, webinarId, id } = template;
  return { type, thumbnail, id, parentId, webinarId };
}

export function serializeTemplateInstanceFull(
  template: TemplateInstance
): TemplateInstanceObjectFull {
  const { content } = template;
  const parsed = JSON.parse(content);
  const { parentId, ...rest } = serializeTemplateInstance(template);
  return {
    ...rest,
    ...parsed,
    parent: serializeTemplate(template.parent),
  };
}
