/**
 * Integrations with different CRMs
 */
import axios from "axios";
import { Attendee } from "../db/entities/Attendee";
import { NotificationGateway } from "../db/entities/NotificationsGateway";

export async function createActiveCampaignContact(
  attendee: Attendee,
  gateway: NotificationGateway,
  fieldValues: Record<string, string>
) {
  await axios.post(
    `${gateway.integration.settings.api_url}/api/3/contact/sync`,
    {
      contact: {
        email: attendee.email,
        firstName: attendee.firstName,
        lastName: attendee.lastName,
        phone: attendee.phone,
        fieldValues: Object.entries(fieldValues).map(([field, value]) => ({
          field: gateway.settings.fields[field],
          value,
        })),
      },
    },
    {
      headers: {
        "Api-Token": gateway.integration.settings.api_key,
      },
    }
  );
}
