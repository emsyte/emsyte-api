import * as jwt from "jsonwebtoken";
import { User } from "../db/entities/User";
import { serializeAuthUser } from "./serializers/userSerializer";
import { serializeAuthAttendee } from "./serializers/attendeeSerializer";
import { AuthUserObject, AuthAttendeeObject } from "@emsyte/common/auth";
import { Attendee } from "../db/entities/Attendee";
import * as moment from "moment";

const APP_SECRET = process.env.APP_SECRET || "not_a_secret";

interface RefreshTokenContent {
  id: number;
}

export function createUserToken(user: User) {
  return jwt.sign(serializeAuthUser(user), APP_SECRET, {
    expiresIn: "24h",
  });
}

export function createAttendeeToken(attendee: Attendee) {
  // Token is good until after the event is over
  const expiresIn =
    moment(attendee.event.end).diff(moment(), "seconds") + 20 * 60; // Add 20 minute grace period;

  return jwt.sign(serializeAuthAttendee(attendee), APP_SECRET, {
    expiresIn,
  });
}

export function createAttendeeReplayToken(attendee: Attendee) {
  // Token is good until after the event is over
  const expiresIn = moment(attendee.event.end)
    .tz(attendee.timezone)
    .endOf("day")
    .add(attendee.event.webinar.replayExpiration, "days")
    .diff(moment(), "seconds");

  return jwt.sign(serializeAuthAttendee(attendee), APP_SECRET, {
    expiresIn,
  });
}

export function verifyToken(token: string) {
  return jwt.verify(token, APP_SECRET) as AuthUserObject | AuthAttendeeObject;
}

export function createRefreshToken(user: User) {
  const content: RefreshTokenContent = {
    id: user.id,
  };
  return jwt.sign(content, APP_SECRET, { expiresIn: "7d" });
}

export function verifyRefreshToken(refreshToken: string) {
  return jwt.verify(refreshToken, APP_SECRET) as RefreshTokenContent;
}
