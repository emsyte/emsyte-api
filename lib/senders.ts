/**
 * Provide handlers for sending notifications using different notification backends
 */

import axios from "axios";

export interface EmailName {
  email: string;
  name: string;
}

export interface Email {
  from: EmailName;
  to: EmailName;
  subject: string;
  content: string;
}

export interface TemplateEmail {
  from: EmailName;
  to: EmailName;
  templateId: string;
  data: any;
}

function buildEmailName(emailName: EmailName) {
  // TODO: Sanitize `emailName.name` to remove invalid chars
  return `${emailName.name} <${emailName.email}>`;
}

export async function sendEmailSendGridTemplate(
  email: TemplateEmail,
  apiKey: string
) {
  if (process.env.NODE_ENV === "test") {
    return;
  }

  // Who needs an SDK package?
  try {
    const data = {
      from: {
        email: email.from.email,
        name: email.from.name,
      },
      personalizations: [
        {
          to: [
            {
              email: email.to.email,
              name: email.to.name,
            },
          ],
          dynamic_template_data: email.data,
        },
      ],
      template_id: email.templateId,
    };

    const response = await axios.post(
      "https://api.sendgrid.com/v3/mail/send",
      data,
      {
        headers: {
          Authorization: "Bearer " + apiKey,
        },
      }
    );
    // TODO: Should we do something with this?
    return response;
  } catch (error) {
    console.log("sendgrid error");
    console.log(JSON.stringify(error.response.data));
  }
}

export async function sendEmailLocal(email: TemplateEmail): Promise<void>;
export async function sendEmailLocal(email: Email): Promise<void>;
export async function sendEmailLocal(
  email: Email | TemplateEmail
): Promise<void> {
  console.log("Sending Email");
  console.log("--------------------------------");
  console.log("to: " + buildEmailName(email.to));
  console.log("from: " + buildEmailName(email.from));

  if ("subject" in email) {
    console.log("subject: " + email.subject);
  } else {
    console.log("templateId:", email.templateId);
    console.log(JSON.stringify(email.data, null, 2));
  }

  console.log("");
  // console.log(email.content);
}
