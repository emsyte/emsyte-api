import { AuthAttendeeObject, AuthUserObject } from "@emsyte/common/auth";
import { TeamUserStatus } from "@emsyte/common/team";
import { WebinarStatus } from "@emsyte/common/webinar";
import { APIGatewayProxyEventBase } from "aws-lambda";
import { Attendee } from "../db/entities/Attendee";
import { Event } from "../db/entities/Event";
import { TeamToUser } from "../db/entities/Team";
import { getAuthorizedAttendeeOrUserId } from "../validators/authorizer";
import { getIntFromPath } from "../validators/utils";
import RouterError from "./RouterError";
import { HandlerData, HandlerOptions, RESTRouter } from "./ServerlessRouter";

interface AttendeeHandlerOptions {}

interface AttendeeHandlerData {
  attendeeId?: number;
  userId?: number;

  event: Event;
}

// TODO: Should we use APIGatewayProxyWithLambdaAuthorizerEvent instead?
type APIGatewayEvent = APIGatewayProxyEventBase<
  AuthUserObject | AuthAttendeeObject
>;

/**
 * Every route requires authentication, and the authenticated user must be a member of the team.
 *
 * Additionally, there will be
 */
export class AttendeeRouter extends RESTRouter<
  AttendeeHandlerOptions,
  AttendeeHandlerData
> {
  protected async _getHandlerData<TRequest, TQuery = {}>(
    event: APIGatewayEvent,
    options: HandlerOptions<TRequest, TQuery, AttendeeHandlerOptions>
  ): Promise<HandlerData<TRequest, TQuery, AttendeeHandlerData>> {
    const data = await this._getBaseHandlerData(event, options);
    const webinarEvent = await this._getWebinarEvent(event);

    const authData = getAuthorizedAttendeeOrUserId(
      event,
      options.allowUnauthenticated ?? false
    );

    if (authData.attendeeId) {
      await this.validateAttendee(authData.attendeeId, webinarEvent);
    } else if (authData.userId) {
      await this.validateUser(authData.userId, webinarEvent);
    }

    return {
      ...data,
      ...authData,
      event: webinarEvent,
    };
  }
  async validateUser(userId: number, event: Event) {
    const teamToUser = await TeamToUser.findOne({
      user: { id: userId },
      team: { id: event.webinar.teamId },
    });

    if (!teamToUser || teamToUser.status !== TeamUserStatus.ACTIVATED) {
      throw new RouterError("Event not found", 404, "Event not found");
    }
  }
  async validateAttendee(attendeeId: number, event: Event) {
    const attendee = await Attendee.findOne(attendeeId, {
      relations: ["event"],
    });
    if (!attendee) {
      throw new RouterError("Event not found", 404, "Event not found");
    }
    if (attendee.event.id !== event.id) {
      throw new RouterError("Event not found", 404, "Event not found");
    }
  }

  protected getAuthData<TRequest, TQuery = {}>(
    _event: APIGatewayEvent,
    _options: HandlerOptions<TRequest, TQuery, AttendeeHandlerOptions>
  ) {
    return {} as any;
  }

  protected prepareEvent(event: APIGatewayEvent) {
    const eventId = event.pathParameters.eventId;
    const { event: _event, pathQuery } = super.prepareEvent(event);
    _event.pathParameters.eventId = eventId;
    return { event: _event, pathQuery };
  }

  protected async _getWebinarEvent(event: APIGatewayEvent) {
    const eventId = getIntFromPath(event.pathParameters, "eventId");
    const webinarEvent = await Event.findOne(eventId, {
      relations: ["webinar"],
    });

    if (!webinarEvent) {
      throw new RouterError("Event not found", 404, "Event not found");
    }

    if (webinarEvent.webinar.status !== WebinarStatus.ACTIVE) {
      throw new RouterError("Event not found", 404, "Event not found");
    }

    if (webinarEvent.canceled) {
      throw new RouterError(
        "Event has been canceled",
        404,
        "Event has been canceled"
      );
    }

    return webinarEvent;
  }
}
