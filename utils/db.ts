import * as AWS from "aws-sdk";

const params =
  process.env.NODE_ENV === "production"
    ? {}
    : {
        endpoint: "http://localhost:4002",
      };

const ddb = new AWS.DynamoDB.DocumentClient(params);

const db = {
  ConnectionsTable: process.env.APPLICATION_CONNECTIONS_TABLE,
  MessagesTable: process.env.APPLICATION_MESSAGES_TABLE,
  Primary: {
    Key: "pk",
    Range: "sk",
    TimeToLive: "ttl",
  },
  Connection: {
    Primary: {
      Key: "pk",
      Range: "sk",
    },
    Channels: {
      Index: "reverse",
      Key: "sk",
      Range: "pk",
    },
  },
  Channel: {
    Primary: {
      Key: "pk",
      Range: "sk",
    },
    Connections: {
      Key: "pk",
      Range: "sk",
    },
    Messages: {
      Key: "pk",
      Range: "sk",
    },
  },
  Message: {
    Primary: {
      Key: "pk",
      Range: "sk",
    },
  },
};

function parseEntityId(target) {
  if (typeof target === "object") {
    // use from raw event, only needed for connectionId at the moment
    target = target.requestContext.connectionId;
  }

  return target;
}

async function fetchConnectionSubscriptions(connection) {
  const connectionId = parseEntityId(connection);
  console.log(`My connectionId is ${connectionId}`);
  const results = await ddb
    .query({
      TableName: db.ConnectionsTable,
      IndexName: db.Connection.Channels.Index,
      KeyConditionExpression: `${db.Connection.Channels.Key} = :connectionId`,
      ExpressionAttributeValues: {
        ":connectionId": connectionId,
      },
    })
    .promise();
  console.log(results.Items);
  return results.Items;
}

async function fetchChannelSubscriptions(channelId: string) {
  const results = await ddb
    .query({
      TableName: db.ConnectionsTable,
      KeyConditionExpression: `${db.Channel.Connections.Key} = :channelId`,
      ExpressionAttributeValues: {
        ":channelId": channelId,
      },
    })
    .promise();

  console.log(results.Items);
  return results.Items;
}

export const client = {
  ...db,
  parseEntityId,
  fetchConnectionSubscriptions,
  fetchChannelSubscriptions,
  Client: ddb,
};
