import { StorageDriver } from "./driver";
import { FSStorageDriver } from "./fs";
import { S3StorageDriver } from "./s3";

export const USE_S3 = process.env.NODE_ENV === "production";

let DRIVER: StorageDriver;

function _getDriver() {
  if (USE_S3) {
    return new S3StorageDriver();
  }

  return new FSStorageDriver();
}

export function getDriver() {
  if (!DRIVER) {
    DRIVER = _getDriver();
  }

  return DRIVER;
}
