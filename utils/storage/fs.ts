import { promises as fs } from "fs";
import * as path from "path";
import { StorageDriver } from "./driver";

const API_BASE = process.env.API_BASE;
function getRootDir() {
  const index = __dirname.indexOf(".webpack");

  if (index === -1) {
    return __dirname;
  }

  return path.join(__dirname.slice(0, index), "media");
}

export const LOCAL_MEDIA_PATH = getRootDir();

export class FSStorageDriver extends StorageDriver {
  getBaseUrl(): string {
    return API_BASE + "/media/local/";
  }

  async getUploadUrl(key: string, _contentType: string) {
    return this.getUrlFromKey(key);
  }

  async delete(key: string) {
    const filepath = this._keyToPath(key);
    await fs.unlink(filepath);

    // Cleanup empty dirs
    let current = path.dirname(filepath);
    let items = await fs.readdir(current);
    while (items.length == 0) {
      await fs.rmdir(current);
      current = path.dirname(current);
      items = await fs.readdir(current);
    }
  }

  async duplicate(fromKey: string, toKey: string) {
    const fromPath = this._keyToPath(fromKey);
    const toPath = this._keyToPath(toKey);

    await fs.mkdir(path.dirname(toPath), {
      recursive: true,
    });

    await fs.copyFile(fromPath, toPath);
  }

  async uploadFile(
    key: string,
    _contentType: string,
    content: string | Buffer
  ) {
    const filepath = this._keyToPath(key);

    await fs.mkdir(path.dirname(filepath), {
      recursive: true,
    });

    await fs.writeFile(filepath, content);
  }

  async getFile(key: string) {
    const filepath = this._keyToPath(key);

    return await fs.readFile(filepath);
  }

  private _keyToPath(key: string) {
    return path.join(LOCAL_MEDIA_PATH, key);
  }
}
