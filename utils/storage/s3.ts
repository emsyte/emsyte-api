import * as AWS from "aws-sdk";
import { StorageDriver } from "./driver";

const { AWS_S3_BUCKET } = process.env;

const s3 = new AWS.S3({
  s3ForcePathStyle: true,
});

export class S3StorageDriver extends StorageDriver {
  getBaseUrl(): string {
    return `https://s3.amazonaws.com/${AWS_S3_BUCKET}/`;
  }

  async getUploadUrl(key: string, contentType: string) {
    const params = {
      Bucket: AWS_S3_BUCKET,
      Key: key,
      ContentType: contentType,
    };

    return await s3.getSignedUrlPromise("putObject", params);
  }

  async delete(key: string) {
    await s3
      .deleteObject({
        Bucket: AWS_S3_BUCKET,
        Key: key,
      })
      .promise();
  }

  async duplicate(fromKey: string, toKey: string) {
    console.log(
      `Attempting to copy ${fromKey} to ${toKey} in bucket ${AWS_S3_BUCKET}`
    );

    await s3
      .copyObject({
        Bucket: AWS_S3_BUCKET,
        Key: toKey,
        CopySource: encodeURI(
          AWS_S3_BUCKET + (fromKey.startsWith("/") ? "" : "/") + fromKey
        ),
      })
      .promise();
  }

  async uploadFile(key: string, contentType: string, content: string | Buffer) {
    await s3
      .putObject({
        Bucket: AWS_S3_BUCKET,
        Key: key,
        Body: content,
        ContentType: contentType,
      })
      .promise();
  }

  async getFile(key: string) {
    const response = await s3
      .getObject({
        Bucket: AWS_S3_BUCKET,
        Key: key,
      })
      .promise();

    return response.Body as Buffer;
  }
}
