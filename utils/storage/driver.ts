import { StorageError } from "./errors";

export abstract class StorageDriver {
  abstract getBaseUrl(): string;

  public getKeyFromUrl(url: string): string {
    const baseURL = this.getBaseUrl();

    const parts = url.split(baseURL);
    if (parts.length === 2) {
      return decodeURIComponent(parts[1]);
    }

    throw new StorageError(`Invalid url: ${url}`);
  }

  public getUrlFromKey(key: string): string {
    return this.getBaseUrl() + key;
  }

  abstract getUploadUrl(key: string, contentType: string): Promise<string>;

  abstract delete(key: string): Promise<void>;
  abstract duplicate(fromKey: string, toKey: string): Promise<void>;
  abstract uploadFile(
    key: string,
    contentType: string,
    content: string | Buffer
  ): Promise<void>;

  abstract getFile(key: string): Promise<Buffer>;
}
