jest.mock("./manager");
import { getDriver as _getDriver } from "./manager";
import {
  deleteFile,
  duplicateFile,
  getObjectKey,
  getUploadUrl,
  parseObjectKey,
} from ".";
import MockDate from "mockdate";
import { StorageDriver } from "./driver";

const getDriver = (_getDriver as unknown) as jest.MockedFunction<
  typeof _getDriver
>;

describe("Storage", () => {
  beforeEach(() => {
    MockDate.set("2021-02-02 00:00:00");
  });
  afterEach(() => {
    MockDate.reset();
  });
  test("getObjectKey", () => {
    MockDate.set("2021-02-02 00:00:00");
    expect(getObjectKey("template-thumbnail", "thumbnail", ".png")).toMatch(
      /template-thumbnail\/2021\/February\/2\/[a-f0-9]{16}\/thumbnail\.png/
    );
  });

  test("parseObjectKey()", () => {
    expect(
      parseObjectKey(
        "template-thumbnail/2021/February/2/87376188fee82c93/thumbnail.png"
      )
    ).toEqual({
      group: "template-thumbnail",
      name: "thumbnail",
      ext: ".png",
    });
  });

  describe("Calls drivers", () => {
    class MockDriver extends StorageDriver {
      getBaseUrl(): string {
        return "https://example.com/media/";
      }
      async getUploadUrl(_key: string, _contentType: string) {
        return "";
      }
      async delete(_key: string) {}
      async duplicate(_fromKey: string, _toKey: string) {}

      async uploadFile(
        _key: string,
        _contentType: string,
        _content: string | Buffer
      ) {}
      async getFile(_key: string): Promise<Buffer> {
        return Buffer.from("", "ascii");
      }
    }

    test("getUploadUrl()", async () => {
      const driver = new MockDriver();
      getDriver.mockReturnValue(driver);
      const spy = jest.spyOn(driver, "getUploadUrl");
      spy.mockReturnValue(Promise.resolve("https://example.com/upload"));

      const { url, uploadTo } = await getUploadUrl(
        "template-thumbnail",
        "image.png",
        "image/png"
      );
      expect(uploadTo).toEqual("https://example.com/upload");
      expect(url).toMatch(
        /https:\/\/example\.com\/media\/template-thumbnail\/2021\/February\/2\/[a-f0-9]+\/image.png/
      );
    });

    test("deleteFile()", async () => {
      const driver = new MockDriver();
      getDriver.mockReturnValue(driver);
      const spy = jest.spyOn(driver, "delete");

      await deleteFile(
        "https://example.com/media/template-thumbnail/2021/February/2/87376188fee82c93/thumbnail.png"
      );

      expect(spy).toHaveBeenCalledWith(
        "template-thumbnail/2021/February/2/87376188fee82c93/thumbnail.png"
      );
    });

    test("duplicateFile()", async () => {
      MockDate.set("2021-02-03 00:00:00");

      const driver = new MockDriver();
      getDriver.mockReturnValue(driver);
      const spy = jest.spyOn(driver, "duplicate");

      const newUrl = await duplicateFile(
        "https://example.com/media/template-thumbnail/2021/February/2/87376188fee82c93/thumbnail.png"
      );

      expect(spy).toHaveBeenCalledWith(
        "template-thumbnail/2021/February/2/87376188fee82c93/thumbnail.png",
        expect.stringMatching(
          /template-thumbnail\/2021\/February\/3\/[a-f0-9]+\/thumbnail.png/
        )
      );
      expect(newUrl).toMatch(
        /https:\/\/example\.com\/media\/template-thumbnail\/2021\/February\/3\/[a-f0-9]+\/thumbnail.png/
      );
    });
  });
});
