import { randomBytes } from "crypto";
import * as moment from "moment";
import * as path from "path";
import { getDriver } from "./manager";

export function getObjectKey(group: string, name: string, file_ext: string) {
  const today = moment();

  const prefix = randomBytes(8).toString("hex");

  return `${group}/${today.format("YYYY")}/${today.format(
    "MMMM"
  )}/${today.format("D")}/${prefix}/${name}${file_ext}`;
}

export function parseObjectKey(key: string) {
  const match = key.match(
    /(?<group>[a-z_-]+)\/[0-9]{4}\/[a-zA-Z]+\/[0-9]+\/[a-z0-9]+\/(?<filename>.+)/
  );
  const { group, filename } = match.groups;
  const { name, ext } = path.parse(filename);
  return { group, name, ext };
}

/**
 * Return a duplicated version of the given key.
 *
 * @param key The key to create the duplicate of.
 */
export function getDuplicateKey(key: string) {
  const { group, name, ext } = parseObjectKey(key);
  return getObjectKey(group, name, ext);
}

/**
 * Duplicate a given file in the file storage and returns the new url
 *
 * @param url The url to the file to duplicate
 */
export async function duplicateFile(url: string): Promise<string> {
  const driver = getDriver();
  const key = driver.getKeyFromUrl(url);
  const toKey = getDuplicateKey(key);

  await driver.duplicate(key, toKey);
  return driver.getUrlFromKey(toKey);
}

/**
 * Delete a given file from the file storages.
 *
 * @param url The url to the file to delete
 */
export async function deleteFile(url: string) {
  const driver = getDriver();
  await driver.delete(driver.getKeyFromUrl(url));
}

/**
 * Get the URL to upload a particular file
 *
 * @param group Name of the group this file will be namespaced to
 * @param filename Name of the file to be uploaded
 * @param contentType The mime type of the file to be uploaded
 */
export async function getUploadUrl(
  group: string,
  filename: string,
  contentType: string
) {
  const file_ext = path.extname(filename);
  const name = filename.slice(0, -file_ext.length);

  const key = getObjectKey(group, name, file_ext);

  const driver = getDriver();
  const uploadTo = await driver.getUploadUrl(key, contentType);
  const url = driver.getUrlFromKey(key);
  return { uploadTo, url };
}
