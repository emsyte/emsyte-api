import { APIResponse } from "@emsyte/common/api";

export function getResponse<T>(data: T): APIResponse<T> {
  return { data };
}
