interface ResponseConfig {
  headers: { [key: string]: string };
}

export class Response<T> {
  statusCode?: number;
  protected _body: T;
  protected _config: Partial<ResponseConfig>;

  constructor(body: T, config?: Partial<ResponseConfig>) {
    this._body = body;
    this._config = config ?? {};
  }

  get headers() {
    return this._config.headers ?? {};
  }

  get body() {
    return JSON.stringify(this._body);
  }
  get extra() {
    return {};
  }
}

export class BinaryResponse extends Response<Buffer> {
  protected _mime_type: string;

  constructor(
    body: Buffer,
    mime_type: string,
    config?: Partial<ResponseConfig>
  ) {
    super(body, config);
    this._mime_type = mime_type;
  }
  get body() {
    return this._body.toString("base64");
  }

  get headers() {
    const headers = this._config.headers ?? {};
    return {
      ...headers,
      "content-type": this._mime_type,
      "content-length": this._body.length.toString(),
    };
  }

  get extra() {
    return {
      isBase64Encoded: true,
    };
  }
}

export class RedirectResponse extends Response<string> {
  url: string;

  constructor(
    url: string,
    statusCode: number,
    config?: Partial<ResponseConfig>
  ) {
    super("", config);
    this.url = url;
    this.statusCode = statusCode;
  }

  get headers() {
    const headers = this._config.headers ?? {};
    return {
      ...headers,
      Location: this.url,
    };
  }

  get body() {
    return "";
  }
}
