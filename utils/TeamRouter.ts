import { Team, TeamToUser } from "../db/entities/Team";
import RouterError from "./RouterError";
import { HandlerData, HandlerOptions, RESTRouter } from "./ServerlessRouter";
import { TeamUserStatus, TeamStatus } from "@emsyte/common/team";
import { PERMISSIONS } from "@emsyte/common/permissions";
import { APIGatewayProxyEventBase } from "aws-lambda";
import { AuthAttendeeObject, AuthUserObject } from "@emsyte/common/auth";

interface TeamHandlerOptions {
  permissions?: string[];

  /** Should it allow the team to be in the 'disabled' status? */
  allowDisabled?: boolean;
}

interface TeamHandlerData {
  team: Team;
  teamUser: TeamToUser;
}

// TODO: Should we use APIGatewayProxyWithLambdaAuthorizerEvent instead?
type APIGatewayEvent = APIGatewayProxyEventBase<
  AuthUserObject | AuthAttendeeObject
>;

/**
 * Every route requires authentication, and the authenticated user must be a member of the team.
 *
 * Additionally, there will be
 */
export class TeamRouter extends RESTRouter<
  TeamHandlerOptions,
  TeamHandlerData
> {
  protected async _getHandlerData<TRequest, TQuery = {}>(
    event: APIGatewayEvent,
    options: HandlerOptions<TRequest, TQuery, TeamHandlerOptions>
  ): Promise<HandlerData<TRequest, TQuery, TeamHandlerData>> {
    const data = await this._getBaseHandlerData(event, options);
    const team = await this._getTeam(event);

    if (team.status === TeamStatus.DISABLED && !options.allowDisabled) {
      throw new RouterError("Team is disabled", 409, "Team is disabled");
    }

    const teamUser = await this._getTeamUser(team, data.userId);

    this._verifyPermissions(teamUser, options.permissions ?? []);

    return {
      ...data,
      team,
      teamUser,
    };
  }

  protected prepareEvent(event: APIGatewayEvent) {
    const team = event.pathParameters.team;
    const { event: _event, pathQuery } = super.prepareEvent(event);
    _event.pathParameters.team = team;
    return { event: _event, pathQuery };
  }

  protected async _getTeam(event: APIGatewayEvent) {
    const slug = event.pathParameters.team;
    const team = await Team.findOne({ slug });

    if (!team) {
      throw new RouterError("Invalid team", 404, "Invalid team");
    }

    return team;
  }

  protected async _getTeamUser(team: Team, userId: number) {
    const teamUser = await TeamToUser.findOne({
      team,
      user: { id: userId },
    });

    if (!teamUser) {
      throw new RouterError("User isn't member of team", 403, "Forbidden");
    }

    if (teamUser.status === TeamUserStatus.INVITED) {
      throw new RouterError(
        "User hasn't accepted invite yet",
        403,
        "Forbidden"
      );
    }

    return teamUser;
  }

  protected _verifyPermissions(teamUser: TeamToUser, permissions: string[]) {
    const allowed = new Set(PERMISSIONS[teamUser.role]);
    const granted = permissions.every((p) => allowed.has(p));
    if (!granted) {
      throw new RouterError(
        "Insufficient permissions",
        403,
        "Permission Denied."
      );
    }
  }
}
