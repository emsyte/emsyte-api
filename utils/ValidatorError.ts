import RouterError from "./RouterError";

export interface ErrorResult {
  [K: string]: string[] | ErrorResult;
}

export class ValidatorError extends RouterError {
  code: number;
  apiResponse: object;
  constructor(message: string, code?: number, errors?: ErrorResult) {
    super(message, code, message);
    this.name = "ValidatorError";

    if (errors) {
      this.apiResponse = {
        message,
        errors,
      };
    }
  }
}

export default ValidatorError;
