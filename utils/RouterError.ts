class RouterError extends Error {
  defaultApiResponse = {
    404: "Not found.",
    500: "Internal server error.",
    400: "Bad Request.",
  };
  code: number;
  apiResponse: object;
  constructor(message, code?: number, apiResponse?: string) {
    super(message);
    this.code = code || 500;
    this.apiResponse = apiResponse || this.defaultApiResponse[this.code];
    this.apiResponse = { message: this.apiResponse };
    this.name = "RouterError";
  }

  static basic(message: string, code: number) {
    return new RouterError(message, code, message);
  }
}

export default RouterError;
