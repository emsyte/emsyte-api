import { APIGatewayProxyResult } from "aws-lambda";
import { Response } from "./Response";

function buildResponse<T>(
  statusCode: number,
  body: T | Response<T>
): APIGatewayProxyResult {
  const response = body instanceof Response ? body : new Response(body);

  return {
    statusCode: response.statusCode ?? statusCode,
    body: response.body,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      ...response.headers,
    },

    ...response.extra,
  };
}

export default buildResponse;
