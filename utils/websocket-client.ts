import * as AWS from "aws-sdk";
import { client as db } from "./db";
// const db = require("./db")

export default class Client {
  private client: AWS.ApiGatewayManagementApi;

  // allow just passing a single event to setup the client for ease of use
  async _setupClient(event: any) {
    // fetch config from db if none provided and we do not have a client
    if (typeof event !== "object" && !this.client) {
      const item = await db.Client.get({
        TableName: db.MessagesTable,
        Key: {
          [db.Primary.Key]: "APPLICATION",
          [db.Primary.Range]: "WS_CONFIG",
        },
      }).promise();
      event = item.Item;
      event.fromDb = true;
    }

    if (this.client) {
      return;
    }

    if (event.requestContext.apiId) {
      event.requestContext.domainName = `${event.requestContext.apiId}.execute-api.${process.env.API_REGION}.amazonaws.com`;
    }
    const endpoint =
      process.env.NODE_ENV === "production"
        ? `https://${event.requestContext.domainName}/${event.requestContext.stage}`
        : "http://localhost:3001";

    this.client = new AWS.ApiGatewayManagementApi({
      apiVersion: "2018-11-29",
      endpoint: endpoint,
    });

    // temporarily we update dynamodb with most recent info
    // after CF support this can go away, we just do this so a single deployment makes this work
    if (event.fromDb !== true) {
      await db.Client.put({
        TableName: db.MessagesTable,
        Item: {
          [db.Primary.Key]: "APPLICATION",
          [db.Primary.Range]: "WS_CONFIG",
          requestContext: {
            domainName: event.requestContext.domainName,
            stage: event.requestContext.stage,
          },
        },
      }).promise();
    }
  }

  async send(connection, payload) {
    // Cheat and allow event to be passed in
    // this also lets us default to setupClient too
    await this._setupClient(connection);

    let ConnectionId = connection;
    if (typeof connection === "object") {
      ConnectionId = connection.requestContext.connectionId;
    }

    await this.client
      .postToConnection({
        ConnectionId,
        Data: JSON.stringify(payload),
      })
      .promise()
      .catch(async (err) => {
        console.log(JSON.stringify(err));

        if (err.statusCode === 410) {
          const subscriptions = await db.fetchConnectionSubscriptions(
            ConnectionId
          );

          console.log(
            `Found stale connection ${ConnectionId}, unsubscribing from channels:`,
            JSON.stringify(subscriptions, null, 2)
          );

          const unsubscribes = subscriptions.map(async (subscription) =>
            db.Client.delete({
              TableName: db.ConnectionsTable,
              Key: {
                [db.Channel.Connections.Key]: db.parseEntityId(
                  subscription[db.Channel.Primary.Key]
                ),
                [db.Channel.Connections.Range]: ConnectionId,
              },
            }).promise()
          );

          await Promise.all(unsubscribes);
        }
      });

    return true;
  }
}
