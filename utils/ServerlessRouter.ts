import { AuthAttendeeObject, AuthUserObject } from "@emsyte/common/auth";
import { APIGatewayProxyEventBase, Handler } from "aws-lambda";
import * as parser from "lambda-multipart-parser";
import { Path } from "path-parser";
import * as qs from "querystring";
import initializeDatabase from "../db/connection";
import { getAuthorizedUserId } from "../validators/authorizer";
import buildResponse from "./BuildResponse";
import { Response } from "./Response";
import RouterError from "./RouterError";
import { runValidation } from "./validator";
import { FEATURES } from "@emsyte/common/features";

import * as Sentry from "@sentry/node";
// import * as Tracing from "@sentry/tracing";

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    release: process.env.SENTRY_RELEASE,
    dsn:
      "https://83fa1b44cf4d42d1b2ed97cb8371990f@o308847.ingest.sentry.io/5836381",

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 0.8,
  });
}

const SUPPORTED_HTTP_METHODS = [
  "GET",
  "POST",
  "PUT",
  "DELETE",
  "UPDATE",
] as const;

type HTTPMethod = typeof SUPPORTED_HTTP_METHODS[number];

// TODO: Should we use APIGatewayProxyWithLambdaAuthorizerEvent instead?
type APIGatewayEvent = APIGatewayProxyEventBase<
  AuthUserObject | AuthAttendeeObject
>;

class BaseServerlessRouter {
  routes: Partial<Record<HTTPMethod, Record<string, Handler>>> = {};
  connectionPromise: Promise<any>;

  constructor() {
    // istanbul ignore next
    if (process.env.NODE_ENV !== "test") {
      this.connectionPromise = initializeDatabase({ name: "default" });
    }
  }

  _createRoute(path: string, handler: Handler, httpMethod: HTTPMethod) {
    path = path[0] === "/" && path.length > 1 ? path.substr(1) : path;
    const methodHandlers = this.getMethodHandlers(httpMethod);
    methodHandlers[path] = handler;
  }

  resolvePath(path: string, httpMethod: HTTPMethod, resource: string) {
    const methodHandlers = this.getMethodHandlers(httpMethod);

    if (path in methodHandlers) {
      return path;
    }

    const pathQuery = Object.keys(methodHandlers).find(
      (route) => new Path(route).test(path) !== null
    );

    if (pathQuery) {
      return pathQuery;
    }

    throw new RouterError(
      `Endpoint ${httpMethod}:${path} not registered at the Resource "${resource}".`,
      404
    );
  }

  getHandler() {
    const handler: Handler<APIGatewayEvent> = async (
      event,
      context,
      callback
    ) => {
      const transaction = Sentry.startTransaction({
        op: event.httpMethod,
        name: event.path,
      });

      await this.connectionPromise;

      try {
        // Get Response
        const { event: _event, pathQuery } = this.prepareEvent(event);
        const routeHandler = this.getMethodHandlers(
          _event.httpMethod as HTTPMethod
        )[pathQuery];

        // Get Response
        const body = await routeHandler(_event, context, callback);

        // Build Response
        return this.response(body, 200);
      } catch (error) {
        return this.handleError(error);
      } finally {
        transaction.finish();
      }
    };
    return handler;
  }

  getPath(event: APIGatewayEvent) {
    return event.pathParameters && event.pathParameters.any
      ? event.pathParameters.any
      : "/";
  }

  protected prepareEvent(event: APIGatewayEvent) {
    const path = this.getPath(event);

    const httpMethod = event.httpMethod as HTTPMethod;
    const pathQuery = this.resolvePath(path, httpMethod, event.resource);
    event.pathParameters = new Path(pathQuery).test(path);

    return { event, pathQuery };
  }

  private response(body: object, code: number) {
    return buildResponse(code, body);
  }

  private handleError(error: Error) {
    // istanbul ignore next
    if (process.env.NODE_ENV !== "test") {
      console.error(error);
    }

    if (error instanceof RouterError) {
      Sentry.captureMessage(
        `API Warning: ${error.message} (status: ${error.code})`
      );

      return this.response(error.apiResponse, error.code);
    }

    Sentry.captureException(error);

    return this.response({ message: error.message }, 500);
  }

  private getMethodHandlers(httpMethod: HTTPMethod) {
    if (!this.routes[httpMethod]) {
      this.routes[httpMethod] = {};
    }
    return this.routes[httpMethod];
  }
}

type Parser<T = any> = (event: APIGatewayEvent) => Promise<T> | T;

function parseJSON(body: string) {
  if (body === null) {
    return {};
  }

  try {
    return JSON.parse(body) || {};
  } catch (Error) {
    throw new RouterError("Can't parse request.", 400);
  }
}

function getHeader(event: APIGatewayEvent, header: string): string | string[] {
  const headers = { ...event.headers, ...event.multiValueHeaders };
  for (const [key, value] of Object.entries(headers)) {
    if (key.toLocaleLowerCase() === header.toLocaleLowerCase()) {
      return value;
    }
  }
}

function getSingleHeader(event: APIGatewayEvent, header: string): string {
  const value = getHeader(event, header);
  if (Array.isArray(value)) {
    return value[0];
  }
  return value;
}

function normalizeHeaders(event: APIGatewayEvent) {
  const headers: Record<string, string> = {};

  for (const [key, value] of Object.entries(event.headers)) {
    headers[key.toLowerCase()] = value;
  }
  return headers;
}

const defaultParser: Parser = async (event) => {
  const contentType =
    getSingleHeader(event, "Content-Type") ?? "application/json";

  if (contentType.startsWith("application/json")) {
    return parseJSON(event.body);
  } else if (contentType.includes("multipart/form-data")) {
    return await parser.parse(event);
  } else if (contentType.includes("application/x-www-form-urlencoded")) {
    return qs.decode(event.body);
  }

  throw new RouterError(
    "Unrecognized content type: " + contentType,
    415,
    "Unsupported Media Type"
  );
};

export type HandlerOptions<TRequest, TQuery = {}, Extra = {}> = {
  parser?: Parser;
  bodyValidator?: new () => TRequest;
  bodyValidationMessage?: string;

  queryValidator?: new () => TQuery;
  queryValidationMessage?: string;
  allowUnauthenticated?: boolean;
} & Extra;

export type HandlerData<TRequest, TQuery = {}, Extra = {}> = {
  data: TRequest;
  query: TQuery;
  userId: number;
  pathParameters: Record<string, string>;
  headers: Record<string, string>;
  isFeatureEnabled: (feature: string) => boolean;
} & Extra;

export type RequestHandler<TRequest, TResponse, TQuery, Extra = {}> = (
  data: HandlerData<TRequest, TQuery, Extra>
) => Promise<TResponse | Response<TResponse>>;

export abstract class RESTRouter<
  ExtraOptions = {},
  ExtraData = {},
  TEvent extends APIGatewayEvent = APIGatewayEvent
> extends BaseServerlessRouter {
  protected _getWrappedHandler<TRequest, TResponse, TQuery = {}>(
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>,
    handler: RequestHandler<TRequest, TResponse, TQuery, ExtraData>
  ): Handler<TEvent> {
    return async (event) => {
      const data = await this._getHandlerData(event, options);
      return await handler(data);
    };
  }

  protected abstract _getHandlerData<TRequest, TQuery = {}>(
    event: TEvent,
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>
  ): Promise<HandlerData<TRequest, TQuery, ExtraData>>;

  protected async _getBaseHandlerData<TRequest, TQuery = {}>(
    event: TEvent,
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>
  ): Promise<HandlerData<TRequest, TQuery>> {
    const parser = options.parser ?? defaultParser;
    const parsed = ((await parser(event)) as unknown) as TRequest;

    const data = options.bodyValidator
      ? await runValidation(
          options.bodyValidator,
          parsed,
          options.bodyValidationMessage
        )
      : parsed;

    const rawQuery = (event.queryStringParameters as unknown) as TQuery;

    const query = options.queryValidator
      ? await runValidation(
          options.queryValidator,
          rawQuery ?? {},
          options.queryValidationMessage
        )
      : rawQuery;

    const headers = normalizeHeaders(event);

    return {
      data,
      query,
      ...this.getAuthData(event, options),
      headers,
      isFeatureEnabled(feature: string) {
        return FEATURES[feature] ?? true;
      },
      pathParameters: event.pathParameters,
    };
  }

  protected getAuthData<TRequest, TQuery = {}>(
    event: TEvent,
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>
  ) {
    const userId = getAuthorizedUserId(
      event,
      options.allowUnauthenticated ?? false
    );

    if (userId) {
      Sentry.setUser({
        id: userId.toString(),
      });
    }

    return { userId };
  }

  get<TResponse, TQuery = {}>(
    path: string,
    options: HandlerOptions<void, TQuery, ExtraOptions>,
    handler: RequestHandler<void, TResponse, TQuery, ExtraData>
  ) {
    this._createRoute(path, this._getWrappedHandler(options, handler), "GET");
  }

  post<TRequest, TResponse, TQuery = {}>(
    path: string,
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>,
    handler: RequestHandler<TRequest, TResponse, TQuery, ExtraData>
  ) {
    this._createRoute(path, this._getWrappedHandler(options, handler), "POST");
  }

  put<TRequest, TResponse, TQuery = {}>(
    path: string,
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>,
    handler: RequestHandler<TRequest, TResponse, TQuery, ExtraData>
  ) {
    this._createRoute(path, this._getWrappedHandler(options, handler), "PUT");
  }

  delete<TRequest, TResponse, TQuery = {}>(
    path: string,
    options: HandlerOptions<TRequest, TQuery, ExtraOptions>,
    handler: RequestHandler<TRequest, TResponse, TQuery, ExtraData>
  ) {
    this._createRoute(
      path,
      this._getWrappedHandler(options, handler),
      "DELETE"
    );
  }
}
class ServerlessRouter extends RESTRouter {
  constructor() {
    super();

    // Add version endpoint to every app
    this.get("version", { allowUnauthenticated: true }, async () => ({
      commit: process.env.COMMIT_HASH,
      buildDate: process.env.BUILD_DATE,
    }));

    // This allows us to test sentry
    this.post("error", { allowUnauthenticated: true }, async () => {
      throw new Error("this fails!");
    });
  }

  protected _getHandlerData<TRequest, TQuery = {}>(
    event: APIGatewayEvent,
    options: HandlerOptions<TRequest, TQuery>
  ): Promise<HandlerData<TRequest, TQuery>> {
    return this._getBaseHandlerData(event, options);
  }
}

export default ServerlessRouter;
