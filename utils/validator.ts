import { ClassConstructor, plainToClass } from "class-transformer";
import { validate, ValidationError } from "class-validator";
import ValidatorError, { ErrorResult } from "../utils/ValidatorError";

function _processErrors(errors: ValidationError[]) {
  const result: ErrorResult = {};

  for (const error of errors) {
    if ("constraints" in error) {
      result[error.property] = Object.values(error.constraints);
    } else {
      result[error.property] = _processErrors(error.children);
    }
  }

  return result;
}

function processErrors(errors: ValidationError[], errorMessage: string) {
  if (errors.length > 0) {
    const result = _processErrors(errors);

    throw new ValidatorError(errorMessage, 422, result);
  }
}

export async function runValidation<T extends {}>(
  validator: ClassConstructor<T>,
  body: any,
  errorMessage?: string
) {
  const instance = plainToClass(validator, body);
  // whitelist only allows the properties of the validator
  const errors: ValidationError[] = await validate(instance, {
    whitelist: true,
  });

  processErrors(errors, errorMessage ?? "Validation failed");

  return instance;
}
