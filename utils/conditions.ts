/**
 * Function to help the typescript compiler know to fail if some possible values aren't covered in a switch statement
 *
 * For example:
 *
 * ```
 * enum Color {
 *   Red,
 *   Blue,
 *   Green
 * }
 *
 * function getColorName(color: Color) {
 *   switch (color) {
 *     case Color.Red:
 *       return "Red";
 *     case Color.Blue:
 *       return "Blue";
 *     default:
 *       // This won't compile because `color` could be Green, and unknownValue only accepts never
 *       return `Unknown color: ${unknownValue(color)}`;
 *   }
 * }
 *```
 */
export function unknownValue(value: never) {
  return value;
}
