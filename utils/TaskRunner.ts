import { Handler } from "aws-lambda";
import { Connection } from "typeorm";
import initializeDatabase from "../db/connection";
import * as Sentry from "@sentry/node";

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    release: process.env.SENTRY_RELEASE,
    dsn:
      "https://83fa1b44cf4d42d1b2ed97cb8371990f@o308847.ingest.sentry.io/5836381",

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1,
  });
}

export abstract class TaskRunner<TEvent, TResult> {
  connectionPromise: Promise<Connection>;

  constructor() {
    if (process.env.NODE_ENV !== "test") {
      this.connectionPromise = initializeDatabase({ name: "default" });
    }
  }

  abstract run(event: TEvent): Promise<TResult>;

  getHandler() {
    const handler: Handler<TEvent, TResult> = async (event) => {
      await this.connectionPromise;
      return await this.run(event);
    };
    return handler;
  }
}
