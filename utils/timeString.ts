export function splitTimeString(timeString: string) {
  return timeString.split(":").map((n) => parseInt(n)) as [
    number,
    number,
    number
  ];
}
