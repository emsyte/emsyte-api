import { APIGatewayEvent, Handler } from "aws-lambda";
import { AuthType } from "@emsyte/common/auth";

const READ_METHODS = ["get"] as const;
const WRITE_METHODS = ["post", "put", "delete"] as const;

interface APICallerOptions {
  /** User ID, or null for no user */
  auth?: number | null;

  /** Request body */
  body?: object;

  httpMethod?: string;

  team?: string;

  type?: AuthType;

  claims?: any;

  pathParameters?: {
    [name: string]: string;
  };
}

interface Response<T> {
  statusCode: number;
  data: T;
}

interface BaseAPICaller {
  (path: string, options?: APICallerOptions): Promise<object>;
}

type APICaller = BaseAPICaller &
  {
    [K in typeof READ_METHODS[number]]: <T>(
      path: string,
      options?: APICallerOptions
    ) => Promise<Response<T>>;
  } &
  {
    [K in typeof WRITE_METHODS[number]]: <T>(
      path: string,
      body: any,
      options?: APICallerOptions
    ) => Promise<Response<T>>;
  };

function parsePath(rawPath: string) {
  rawPath =
    rawPath[0] === "/" && rawPath.length > 1 ? rawPath.substr(1) : rawPath;
  const [path, query] = rawPath.split("?");

  const searchParams = new (global as any).URLSearchParams(query);
  const queryStringParameters = {};

  searchParams.forEach((value, key) => {
    queryStringParameters[key] = value;
  });
  return { path, queryStringParameters };
}

async function callHandler(
  path: string,
  options: APICallerOptions,
  handler: Handler<APIGatewayEvent>
): Promise<Response<any>> {
  const { path: _path, queryStringParameters } = parsePath(path);

  const requestContext: any = {
    authorizer: {
      claims: {
        id: options.auth,
        type: options.type ?? AuthType.USER,
        ...options.claims,
      },
    },
  };

  const team = options.team;

  const response = await handler(
    {
      body: options.body ? JSON.stringify(options.body) : null,
      headers: {},
      multiValueHeaders: {},
      httpMethod: options.httpMethod.toUpperCase() ?? "GET",
      isBase64Encoded: false,
      path: _path,
      pathParameters: {
        any: _path,
        team,
        ...options.pathParameters,
      },
      queryStringParameters,
      multiValueQueryStringParameters: {},
      stageVariables: {},
      requestContext,
      resource: "",
    },
    {} as any,
    () => {}
  );

  const _response = {
    statusCode: response.statusCode,
    data: response.body && JSON.parse(response.body),
  };
  return _response;
}

export function getAPICaller(handler: Handler<APIGatewayEvent>) {
  const caller: BaseAPICaller = async (path, options = {}) => {
    return callHandler(path, options, handler);
  };

  for (const method of READ_METHODS) {
    (caller as APICaller)[method] = (path, options) =>
      caller(path, { ...options, httpMethod: method.toUpperCase() }) as any;
  }

  for (const method of WRITE_METHODS) {
    (caller as APICaller)[method] = (path, body, options) =>
      caller(path, {
        ...options,
        httpMethod: method.toUpperCase(),
        body,
      }) as any;
  }

  return caller as APICaller;
}
