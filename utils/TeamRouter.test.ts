import { TeamRouter } from "./TeamRouter";
import { getAPICaller } from "./testing/api";

const router = new TeamRouter();
router.get("test", {}, async () => ({ status: "ok" }));

router.post(
  "permissions",
  {
    permissions: ["webinar:delete"],
  },
  async () => ({ status: "ok" })
);

const handler = router.getHandler();

describe("TeamRouter", () => {
  const caller = getAPICaller(handler);

  test("Works with proper team-user auth", async () => {
    const response = await caller.get("test", {
      team: "team-free",
      auth: 1,
    });

    expect(response.data).toEqual({ status: "ok" });
  });

  test("Reject invalid team name", async () => {
    const response = await caller.get("test", {
      team: "bad",
      auth: 1,
    });

    expect(response.data).toEqual({ message: "Invalid team" });
  });

  test("Reject uninvited team member", async () => {
    const response = await caller.get("test", {
      team: "team-free",
      auth: 2,
    });

    expect(response.data).toEqual({ message: "Forbidden" });
  });

  test("Allow team member who has valid permissions", async () => {
    const response = await caller.post(
      "permissions",
      {},
      {
        team: "team-free",
        auth: 1,
      }
    );

    expect(response.data).toEqual({ status: "ok" });
  });

  test("Disallow team member who has invalid permissions", async () => {
    const response = await caller.post(
      "permissions",
      {},
      {
        team: "team-free",
        auth: 5,
      }
    );

    expect(response.data).toEqual({ message: "Permission Denied." });
  });

  test("Doesn't allow performing actions on disabled team", async () => {
    const response = await caller.get("test", {
      auth: 10,
      team: "team-disabled",
    });

    expect(response.statusCode).toEqual(409);
    expect(response.data).toEqual({
      message: "Team is disabled",
    });
  });
});
