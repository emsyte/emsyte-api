import { splitTimeString } from "./timeString";

describe("splitTimeString", () => {
  test("splitTimeString()", () => {
    expect(splitTimeString("01:30:00")).toEqual([1, 30, 0]);
  });
});
