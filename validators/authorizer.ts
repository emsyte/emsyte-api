import {
  APIGatewayTokenAuthorizerHandler,
  APIGatewayAuthorizerResult,
  APIGatewayProxyEventBase,
} from "aws-lambda";
import * as jwt from "jsonwebtoken";
import ValidatorError from "../utils/ValidatorError";
import {
  AuthType,
  AuthAttendeeObject,
  AuthUserObject,
} from "@emsyte/common/auth";

const APP_SECRET = process.env.APP_SECRET || "not_a_secret";

const generatePolicy = (
  principalId: number,
  effect: string,
  resource: string,
  context: any
): APIGatewayAuthorizerResult => {
  const authResponse: any = {
    context,
  };

  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument: any = {};
    policyDocument.Version = "2012-10-17";
    policyDocument.Statement = [];
    const statementOne: any = {};
    statementOne.Action = "execute-api:Invoke";
    statementOne.Effect = effect;
    // statementOne.Resource = resource;
    statementOne.Resource = "*";
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
};

export const handler: APIGatewayTokenAuthorizerHandler = async (
  event,
  _context,
  callback
) => {
  try {
    // check header or url parameters or post parameters for token
    if (!event.authorizationToken) {
      throw new Error("Unauthorized");
    }

    const split = event.authorizationToken.split("Bearer");

    if (split.length !== 2) {
      throw new Error("Unauthorized");
    }

    const token = split[1].trim();

    // verifies secret and checks exp
    const decoded = jwt.verify(token, APP_SECRET) as any;
    return generatePolicy(decoded.id, "Allow", event.methodArn, decoded);
  } catch (error) {
    // istanbul ignore next
    if (process.env.NODE_ENV !== "test") {
      console.log(error);
    }
    if (process.env.NODE_ENV === "development") {
      // TODO: why is this needed to work with serverless offline?
      callback(null, "Unauthorized" as any);
    } else {
      throw new Error("Unauthorized");
    }
  }
};

// TODO: Should we use APIGatewayProxyWithLambdaAuthorizerEvent instead?
type APIGatewayEvent = APIGatewayProxyEventBase<
  AuthUserObject | AuthAttendeeObject
>;

function getEventClaims(event: APIGatewayEvent) {
  if (process.env.NODE_ENV === "production") {
    return event.requestContext.authorizer ?? {};
  }

  // Serverless offline doesn't pass the auth context, but it does add the claims
  return (event.requestContext.authorizer as any)?.claims ?? {};
}

export const getAuthorizedUserId = (
  event: APIGatewayEvent,
  allowUnauthorized: boolean = false
): number => {
  const { id, type } = getEventClaims(event);

  if ((!id && !allowUnauthorized) || type === AuthType.ATTENDEE) {
    throw new ValidatorError("Unauthorized.", 401);
  }

  return id ? +id : undefined;
};

export const getAuthorizedAttendeeOrUserId = (
  event: APIGatewayEvent,
  allowUnauthorized: boolean = false
): { userId?: number; attendeeId?: number } => {
  const { id, type, ...claims } = getEventClaims(event);

  if (!id && !allowUnauthorized) {
    throw new ValidatorError("Unauthorized.", 401);
  }

  if (type === AuthType.USER) {
    return { userId: +id };
  }

  // Attendee must be active
  if (!claims.active) {
    throw new ValidatorError("Unauthorized.", 401);
  }

  return { attendeeId: +id };
};
