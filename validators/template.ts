import {
  APITemplateCreateData,
  APITemplateInstanceCreateData,
  APITemplateInstanceUpdateData,
  APITemplateUpdateData,
} from "@emsyte/common/template";
import { IsInt, IsJSON, IsNumber, IsOptional, IsString } from "class-validator";

class GrapesJSValidator {
  @IsOptional()
  @IsJSON()
  assets: string;

  @IsOptional()
  @IsJSON()
  components: string;

  @IsOptional()
  @IsString()
  css: string;

  @IsOptional()
  @IsString()
  html: string;

  @IsOptional()
  @IsJSON()
  styles: string;
}

export class CreateTemplateInstanceValidator
  implements APITemplateInstanceCreateData {
  @IsNumber()
  parentId: number;
}

export class UpdateTemplateInstanceValidator
  extends GrapesJSValidator
  implements APITemplateInstanceUpdateData {
  @IsString()
  thumbnail: string;
}

export class CreateTemplateValidator implements APITemplateCreateData {
  @IsString()
  name: string;

  @IsInt()
  instanceId: number;
}

export class UpdateTemplateValidator implements APITemplateUpdateData {
  @IsOptional()
  @IsString()
  name: string;

  @IsInt()
  @IsOptional()
  instanceId: number;
}
