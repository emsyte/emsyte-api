import {
  isNumberString,
  matches,
  registerDecorator,
  ValidationOptions,
} from "class-validator";
import { splitTimeString } from "../utils/timeString";
import ValidatorError from "../utils/ValidatorError";

export function getIntFromPath(
  pathParameters: Record<string, string>,
  param: string
) {
  const value = pathParameters[param];

  if (!isNumberString(value, { no_symbols: true })) {
    throw new ValidatorError(
      `Invalid path parameter. Expected "${param}" to be an integer, found "${value}"`,
      400
    );
  }
  return parseInt(value);
}

export function isTimezone(value: any) {
  if (!value) {
    return false;
  }
  try {
    Intl.DateTimeFormat(undefined, { timeZone: value });
    return true;
  } catch (ex) {
    return false;
  }
}

export function IsTimezone(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "isTimezone",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate: isTimezone,
        defaultMessage: (args) => `${args.property} must be a valid timezone`,
      },
    });
  };
}

function isInRange(value: number, min: number, max: number) {
  return value >= min && value <= max;
}

export function isTimeString(value: any) {
  if (!matches(value, /^[0-9]{2}:[0-9]{2}:[0-9]{2}$/)) {
    return false;
  }

  const [hours, minutes, seconds] = splitTimeString(value);

  return (
    isInRange(hours, 0, 23) &&
    isInRange(minutes, 0, 59) &&
    isInRange(seconds, 0, 59)
  );
}

export function IsTimeString(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "isTimeString",
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate: isTimeString,
        defaultMessage: (args) =>
          `${args.property} must be a valid time string`,
      },
    });
  };
}
