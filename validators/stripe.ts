import { IsString, IsOptional } from "class-validator";

export class CreateCustomerValidator {
  @IsString()
  token: string;
}

export class CreateSubscriptionValidator {
  // TODO: This should come from an enum
  @IsString()
  price: string;

  @IsOptional()
  @IsString()
  coupon: string;
}
