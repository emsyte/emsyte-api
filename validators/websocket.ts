import { IsArray, IsBoolean, IsOptional, IsString } from "class-validator";

interface sendHistory {
  roomId: string;
  lastKey: string;
}

export class SubscribeBodyValidator {
  @IsString()
  channelId: string;

  @IsString()
  token: string;

  @IsOptional()
  @IsArray()
  sendHistories: Array<sendHistory>;
}

export class LeaveChannelBodyValidator {
  @IsString()
  channelId: string;
}

export class SendMessageBodyValidator {
  @IsString()
  channelId: string;

  @IsString()
  content: string;

  @IsOptional()
  sendTo: string;

  @IsOptional()
  @IsString()
  token: string;

  @IsOptional()
  @IsBoolean()
  question: boolean;

  @IsOptional()
  @IsString()
  replyTo: string;
}

export class HistoryChannelBodyValidator {
  @IsString()
  channelId: string;

  @IsOptional()
  roomId: string;

  @IsOptional()
  @IsString()
  from: string;

  @IsOptional()
  @IsString()
  to: string;
}

export class UsersChannelBodyValidator {
  @IsString()
  channelId: string;
}

export class RemoveMessageBodyValidator {
  @IsString()
  channelId: string;

  @IsString()
  messageId: string;
}
