import ValidatorError from "../utils/ValidatorError";

const getSettings = (event) => {
  const { id } = event.pathParameters;
  if (!id) {
    throw new ValidatorError("Missing fields.", 400);
  }
  return { id };
};

export const settingsValidator = { getSettings };
