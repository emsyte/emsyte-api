import { getAuthorizedUserId, handler } from "./authorizer";
import * as jwt from "jsonwebtoken";
import { AuthType } from "@emsyte/common/auth";

const APP_SECRET = process.env.APP_SECRET || "not_a_secret";

describe("Authorizer", () => {
  describe("getAuthorizedUserId()", () => {
    test("Should get user ID from event", () => {
      const userId = getAuthorizedUserId({
        requestContext: {
          authorizer: { claims: { id: 1, type: AuthType.USER } },
        },
      } as any);
      expect(userId).toEqual(1);
    });

    test("Throw error if no user id is provided", () => {
      expect(() =>
        getAuthorizedUserId({
          requestContext: {
            authorizer: null,
          },
        } as any)
      ).toThrow();
    });

    test("Throw error type is ATTENDEE", () => {
      expect(() =>
        getAuthorizedUserId({
          requestContext: {
            authorizer: {
              claims: {
                id: 1,
                type: AuthType.ATTENDEE,
              },
            },
          },
        } as any)
      ).toThrow();
    });

    test("Return undefined if allowUnauthorized is true", () => {
      expect(
        getAuthorizedUserId(
          {
            requestContext: {
              authorizer: null,
            },
          } as any,
          true
        )
      ).toBeUndefined();
    });
  });

  describe("authorizer", () => {
    test("Authorizes valid tokens", async () => {
      const token = jwt.sign(
        {
          id: 1,
          email: "johndoe1@example.com",
          firstName: "John",
          lastName: "Doe",
        },
        APP_SECRET,
        {
          expiresIn: "24h",
        }
      );
      const result = await handler(
        {
          type: "TOKEN",
          authorizationToken: "Bearer " + token,
          methodArn: "<resource>",
        },
        null as any,
        null as any
      );

      expect(result).toEqual({
        principalId: 1,
        policyDocument: {
          Version: "2012-10-17",
          Statement: [
            {
              Action: "execute-api:Invoke",
              Effect: "Allow",
              Resource: "*",
            },
          ],
        },
        context: expect.any(Object),
      });
    });

    test("Unauthorized if no token is provided", async () => {
      const result = handler(
        {
          type: "TOKEN",
          authorizationToken: undefined,
          methodArn: "<resource>",
        },
        null as any,
        null as any
      );
      await expect(result).rejects.toThrowError("Unauthorized");
    });

    test("Unauthorized if malformed token is provided", async () => {
      const result = handler(
        {
          type: "TOKEN",
          authorizationToken: "Bearer <bad token>",
          methodArn: "<resource>",
        },
        null as any,
        null as any
      );
      await expect(result).rejects.toThrowError("Unauthorized");
    });

    test("Unauthorized if no Bearer is provided", async () => {
      const result = handler(
        {
          type: "TOKEN",
          authorizationToken: "<bad token>",
          methodArn: "<resource>",
        },
        null as any,
        null as any
      );
      await expect(result).rejects.toThrowError("Unauthorized");
    });
  });
});
