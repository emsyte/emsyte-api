import { IntegrationProvider } from "@emsyte/common/integration";
import {
  APINotificationCreateData,
  APINotificationUpdateData,
  NotificationDeliveryMethod,
  NotificationType,
} from "@emsyte/common/notification";
import {
  APINotificationGatewaySetData,
  APIWebinarAddEventVideoUrl,
  APIWebinarAddRecurrenceData,
  APIWebinarAttendeeCreateData,
  APIWebinarChatCreateData,
  APIWebinarChatUpdateData,
  APIWebinarCreateData,
  APIWebinarOfferCreateData,
  APIWebinarOfferUpdateData,
  APIWebinarUpdateData,
  Frequency,
  WebinarChatLabel,
  WebinarSettingsObject,
  WebinarType,
} from "@emsyte/common/webinar";
import { Transform, Type } from "class-transformer";
import {
  Allow,
  IsBoolean,
  isBoolean,
  isBooleanString,
  IsEmail,
  IsEnum,
  IsIn,
  IsInt,
  IsISO8601,
  IsJWT,
  IsNumber,
  IsObject,
  IsOptional,
  IsPhoneNumber,
  IsPositive,
  IsString,
  IsUrl,
  Max,
  Min,
  ValidateIf,
  ValidateNested,
} from "class-validator";
import { IsTimezone } from "./utils";

class WebinarSettingsValidator implements WebinarSettingsObject {
  @IsUrl(
    {
      require_tld: process.env.NODE_ENV === "production",
    },
    {
      message: "Post Registration URL must be a valid URL",
    }
  )
  postRegistrationUrl: string;

  @IsUrl(
    {
      require_tld: process.env.NODE_ENV === "production",
    },
    {
      message: "Post View URL must be a valid URL",
    }
  )
  postViewUrl: string;

  @IsString()
  registrationPageScript: string;

  @IsString()
  thankyouPageScript: string;

  @IsString()
  registrationFormScript: string;

  @IsString()
  liveRoomScript: string;

  @IsString()
  replayRoomScript: string;
}

export class NotificationGatewaySetValidator
  implements APINotificationGatewaySetData {
  @IsEnum(IntegrationProvider)
  provider: IntegrationProvider;

  @IsObject()
  settings: any;
}

export class CalendarEventsValidator {
  @IsOptional()
  @IsISO8601()
  from?: string;

  @IsOptional()
  @IsISO8601()
  to?: string;

  @IsOptional()
  @IsInt()
  @IsPositive()
  @Type(() => Number)
  limit?: number;

  @IsOptional()
  @IsIn(["ASC", "DESC"])
  order?: "ASC" | "DESC";
}

export class NotificationGatewayQueryValidator {
  @IsIn(["email", "sms"])
  type: "email" | "sms";
}

export class NotificationGatewayFieldsQueryValidator {
  @IsEnum(IntegrationProvider)
  provider: IntegrationProvider;
}

export class WebinarUpdateData implements APIWebinarUpdateData {
  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsString()
  thumbnail: string;

  @IsString()
  @IsOptional()
  name: string;

  @Type(() => Number)
  @IsNumber()
  @IsOptional()
  replayExpiration: number;

  @IsOptional()
  @Transform(({ value }) => {
    if (isBoolean(value)) {
      return value;
    } else if (isBooleanString(value)) {
      return value === "true";
    }
    return value;
  })
  @IsBoolean()
  isPaid: boolean;

  @IsOptional()
  @Type(() => Number)
  @IsInt()
  registrationFee: number;

  @IsOptional()
  @IsString()
  checkoutLink: string;

  @IsOptional()
  @IsString()
  redirectLink: string;

  @IsOptional()
  @Type(() => WebinarSettingsValidator)
  @ValidateNested()
  settings: WebinarSettingsValidator;
}

export class WebinarCreateData implements APIWebinarCreateData {
  @IsString()
  name: string;

  @IsEnum(WebinarType)
  type: WebinarType;

  @IsString()
  videoUrl: string;

  @Type(() => Number)
  @IsNumber()
  duration: number;

  @Type(() => Number)
  @IsNumber()
  replayExpiration: number;

  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsString()
  thumbnail: string;

  @IsOptional()
  @Transform(({ value }) => {
    if (isBoolean(value)) {
      return value;
    } else if (isBooleanString(value)) {
      return value === "true";
    }
    return value;
  })
  @IsBoolean()
  isPaid: boolean;

  @IsOptional()
  @Type(() => Number)
  @IsInt()
  registrationFee: number;

  @IsOptional()
  @IsString()
  checkoutLink: string;

  @IsOptional()
  @IsString()
  redirectLink: string;

  @IsOptional()
  @Type(() => WebinarSettingsValidator)
  @ValidateNested()
  settings: WebinarSettingsValidator;
}

export class WebinarAddRecurrenceData implements APIWebinarAddRecurrenceData {
  @IsInt({
    each: true,
  })
  @Min(0, {
    each: true,
  })
  @Max(6, {
    each: true,
  })
  days: number[];

  @IsEnum(Frequency)
  frequency: Frequency;

  @IsTimezone()
  timezone: string;

  @IsISO8601()
  start: string;

  @IsISO8601()
  end: string;
}

export class WebinarAddEventVideoUrlData implements APIWebinarAddEventVideoUrl {
  @IsString()
  @IsUrl()
  videoUrl: string;
}

export class WebinarOfferCreateData implements APIWebinarOfferCreateData {
  @Allow()
  id?: number;

  @IsNumber()
  @Type(() => Number)
  start: number;

  @IsNumber()
  @Type(() => Number)
  end: number;

  @IsString()
  name: string;

  @IsString()
  buttonText: string;

  @IsString()
  description: string;

  @IsString()
  checkout: string;

  @IsString()
  thumbnail: string;

  @IsNumber()
  @Type(() => Number)
  price: number;

  @IsOptional()
  files?: string[];
}

export class WebinarOfferUpdateData
  extends WebinarOfferCreateData
  implements APIWebinarOfferUpdateData {}

export class WebinarAttendeeCreateData implements APIWebinarAttendeeCreateData {
  @IsEmail()
  email: string;

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsTimezone()
  timezone: string;

  @IsOptional()
  @IsPhoneNumber(null)
  @ValidateIf((e) => (e.phone ?? "").trim() !== "")
  phone?: string;
}

export class WebinarAttendeeRegisterData {
  @IsJWT()
  token: string;
}

export class NotificationUpdateValidator implements APINotificationUpdateData {
  @IsString()
  subject: string;

  @IsString()
  content: string;

  @IsInt()
  hoursOffset: number;
}

export class NotificationCreateValidator
  extends NotificationUpdateValidator
  implements APINotificationCreateData {
  @IsEnum(NotificationType)
  type: NotificationType;

  @IsEnum(NotificationDeliveryMethod)
  deliveryMethod: NotificationDeliveryMethod;
}

export class WebinarChatCreateValidator implements APIWebinarChatCreateData {
  @IsNumber()
  @Transform(({ value }) => parseInt(value))
  offset_timestamp: number;

  @IsString()
  name: string;

  @IsEnum(WebinarChatLabel)
  label: WebinarChatLabel;

  @IsString()
  content: string;
}

export class WebinarChatUploadValidator {
  @ValidateNested({ each: true })
  @Type(() => WebinarChatCreateValidator)
  records: WebinarChatCreateValidator[];
}

export class WebinarChatUpdateValidator
  extends WebinarChatCreateValidator
  implements APIWebinarChatUpdateData {}
