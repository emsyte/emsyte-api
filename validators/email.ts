import { IsEmail, IsString } from "class-validator";

class BaseEmailValidator {
  @IsEmail()
  email: string;

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;
}

export class WelcomeEmailValidator extends BaseEmailValidator {}
export class DeactivateEmailValidator extends BaseEmailValidator {}

export class InviteEmailValidator extends BaseEmailValidator {
  @IsString()
  temp: string;

  @IsString()
  newAccount: boolean;

  @IsString()
  inviter: string;
}

export class InvoiceEmailValidator extends BaseEmailValidator {
  @IsString()
  product: string;

  @IsString()
  description: string;

  @IsString()
  price: string;
}
