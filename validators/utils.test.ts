import { isTimeString, isTimezone } from "./utils";

describe("utils", () => {
  test("isTimezone()", () => {
    expect(isTimezone("America/Chicago")).toBeTruthy();
    expect(isTimezone("Bar/Foo")).toBeFalsy();
  });
  test("isTimeString", () => {
    expect(isTimeString(undefined)).toBeFalsy();
    expect(isTimeString(null)).toBeFalsy();
    expect(isTimeString("")).toBeFalsy();
    expect(isTimeString("123213:23423:243223")).toBeFalsy();
    expect(isTimeString("13:30:000")).toBeFalsy();
    expect(isTimeString("13:30")).toBeFalsy();
    expect(isTimeString("1:30:00")).toBeFalsy();

    expect(isTimeString("13:30:00")).toBeTruthy();
    expect(isTimeString("01:00:00")).toBeTruthy();
    expect(isTimeString("23:59:59")).toBeTruthy();
    expect(isTimeString("00:00:00")).toBeTruthy();

    expect(isTimeString("25:30:00")).toBeFalsy();
    expect(isTimeString("22:60:00")).toBeFalsy();
    expect(isTimeString("22:59:60")).toBeFalsy();
  });
});
