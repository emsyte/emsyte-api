import {
  APIEditTeamData,
  APIRegisterTeamData,
  TEAM_SLUG_PATTERN,
  APIInviteTeamMemberData,
  TeamUserRole,
  APIEditTeamMemberData,
} from "@emsyte/common/team";
import {
  IsString,
  Matches,
  IsEmail,
  IsEnum,
  IsOptional,
} from "class-validator";

export class CheckNameValidator {
  @Matches(TEAM_SLUG_PATTERN)
  slug: string;
}

export class RegisterTeamValidator implements APIRegisterTeamData {
  @IsString()
  name: string;

  @IsString()
  price: string;

  @IsString()
  source: string;

  @IsOptional()
  @IsString()
  coupon?: string;
}

export class EditTeamValidator implements APIEditTeamData {
  @IsString()
  name: string;
}

export class InviteTeamMemberValidator implements APIInviteTeamMemberData {
  @IsEmail()
  email: string;

  @IsEnum(TeamUserRole)
  role: TeamUserRole;
}

export class EditTeamMemberValidator implements APIEditTeamMemberData {
  @IsEnum(TeamUserRole)
  role: TeamUserRole;
}
