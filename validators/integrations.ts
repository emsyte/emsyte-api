import {
  APIIntegrationSetData,
  IntegrationProvider,
} from "@emsyte/common/integration";
import {
  IsBoolean,
  IsIn,
  IsObject,
  IsOptional,
  IsString,
} from "class-validator";

export class SetIntegrationValidator implements APIIntegrationSetData {
  @IsIn(Object.values(IntegrationProvider))
  @IsString()
  provider: IntegrationProvider;

  @IsObject()
  settings: Record<string, string>;

  @IsOptional()
  @IsBoolean()
  active?: boolean;
}
