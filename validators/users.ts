import {
  APIAuthLoginData,
  APIAuthRefreshTokenData,
  APIAuthRegisterData,
  APIChangePasswordData,
  APIUserForgotPasswordConfirmData,
  APIUserForgotPasswordData,
  APIUserUpdateData,
} from "@emsyte/common/user";
import { IsString, IsEmail, IsOptional } from "class-validator";

export class UserRegisterValidator implements APIAuthRegisterData {
  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsEmail()
  email: string;

  @IsString()
  teamName: string;

  // TODO: Validate password strength
  @IsString()
  password: string;

  @IsString()
  price: string;

  @IsString()
  source: string;

  @IsOptional()
  @IsString()
  coupon?: string;
}

export class UserLoginValidator implements APIAuthLoginData {
  @IsEmail()
  email: string;

  @IsString()
  password: string;
}

export class RefreshTokenValidator implements APIAuthRefreshTokenData {
  @IsString()
  token: string;

  @IsString()
  refreshToken: string;
}

export class UserUpdateValidator implements APIUserUpdateData {
  @IsString()
  firstName: string;
  @IsString()
  lastName: string;
  @IsEmail()
  email: string;
}

export class ChangePasswordValidator implements APIChangePasswordData {
  @IsString()
  password: string;
  @IsString()
  newPassword: string;
}

export class UserForgotPasswordValidator implements APIUserForgotPasswordData {
  @IsEmail()
  email: string;
}

export class UserForgotPasswordConfirmValidator
  implements APIUserForgotPasswordConfirmData {
  @IsString()
  password: string;
  @IsString()
  token: string;
}
