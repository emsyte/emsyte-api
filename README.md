# Emsyte
[![pipeline status](https://gitlab.com/tailored-labs/emsyte-api/badges/master/pipeline.svg)](https://gitlab.com/tailored-labs/emsyte-api/-/commits/master)
[![coverage report](https://gitlab.com/tailored-labs/emsyte-api/badges/master/coverage.svg)](https://gitlab.com/tailored-labs/emsyte-api/-/commits/master)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/emsyte/emsyte-api)


Emsyte API, worker, and admin application


## Getting Started

Before you being, you'll need to setup credentials with AWS so that [serverless](http://serverless.com) will function properly. There are a [number of ways](http://slss.io/aws-creds-setup) to do that, but to get started quickly, you can simply set the AWS credentials using environment variables:

```bash
export AWS_ACCESS_KEY_ID=access-key-id
export AWS_SECRET_ACCESS_KEY=access-key-secret
```

Unless you're deploying, you shouldn't actually need to use real AWS credentials, so you can copy-paste the commands above without changing the values.

To start the app, you should only need to run these commands:

```bash
yarn install
yarn start
```

## Advanced Usage

Running `yarn start` actually runs 5 different commands:

1. `yarn kill` - Kills the current database container if it has been created
2. `yarn db` - Spins up a docker container for the Postgres database
3. `yarn typeorm migration:run` - Runs the database migrations using [Typeorm](http://typeorm.io)
4. `yarn load-fixtures` - Loads sample data into the database
5. `yarn offline` - Starts the app using serverless offline plugin

If you run into issues with `yarn start`, try running each of the commands separately to find the one that's failing.

## Chat
A little more setup is required to use the chat websocket connections locally. You'll need to install a local version of dynamodb which can be done using:

```bash
npx sls dynamodb install
```

Then, you'll need to start the app using:

```bash
npx sls offline start --debug --useChildProcesses
```

🐋 DynamoDB can also be ran on a Docker container using
```bash
docker run -p 4002:8000/tcp amazon/dynamodb-local:latest --name dynamodb
```

[fancy GUI tool to look into local DynamoDB contents](https://medium.com/swlh/a-gui-for-local-dynamodb-dynamodb-admin-b16998323f8e)

### Using the chat

The chat works with a websocket. An easy way to test it is with [wscat](https://www.npmjs.com/package/wscat):

```bash
wscat -c ws://localhost:3001

Connected (press CTRL+C to quit)

> {"action": "joinChannel", "channelId": "event-23", "token": "auth token"}
> {"action": "sendMessage", "content": "Hello world!", "channelId": "event-23"}
< {"event": "general", "messageId": "someRandomId", "channelId": "event-23", "name": "Mr Tester", "content": "Hello world!"}
> {"action": "sendMessage", "content": "This is a reply", "replyTo": "someRandomId"}
< {"action": "general", "channelId": "event-23", "name", "Mr Tester", "content": "This is a reply", "replyTo": {"name": "Mr Tester", "content": "Hello world!"}}
> {"action": "removeMessage", "messageId": "the message you want to remove"}
```