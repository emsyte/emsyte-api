import { IntegrationProvider } from "@emsyte/common/integration";
import { Integration } from "../db/entities/Integration";
import { NotificationGateway } from "../db/entities/NotificationsGateway";
import { UnreachableCaseError } from "../utils/errors";
import { AutoResponderProvider } from "./base";
import { ActiveCampaignProvider } from "./providers/ActiveCampaign";
import { ConstantContactProvider } from "./providers/ConstantContact";
import { HubSpotProvider } from "./providers/HubSpot";
import { SendGridProvider } from "./providers/SendGrid";
import { TwilioProvider } from "./providers/Twilio";

function _getIntegrationProvider(provider: IntegrationProvider, settings: any) {
  switch (provider) {
    case IntegrationProvider.MAILCHIMP:
      throw new Error(`Provider ${provider} not implemented yet`);

    case IntegrationProvider.HUB_SPOT:
      return new HubSpotProvider(settings);

    case IntegrationProvider.ACTIVE_CAMPAIGN:
      return new ActiveCampaignProvider(settings);

    case IntegrationProvider.SEND_GRID:
      return new SendGridProvider(settings);

    case IntegrationProvider.TWILIO:
      return new TwilioProvider(settings);
    case IntegrationProvider.CONSTANT_CONTACT:
      return new ConstantContactProvider(settings);
    default:
      throw new UnreachableCaseError(provider);
  }
}

type Provider = ReturnType<typeof _getIntegrationProvider>;

function getIntegrationProvider(integration: Integration) {
  return _getIntegrationProvider(integration.provider, integration.settings);
}

/**
 * Get an integration provider from a notification gateway.
 *
 * @param gateway Notification gateway from the DB
 */
export function getGatewayIntegration(gateway: NotificationGateway) {
  const provider = getIntegrationProvider(gateway.integration);

  provider.setGatewaySettings(gateway.settings);
  return provider;
}

/**
 * Validates settings and return new settings
 *
 * @param provider Integration provider type
 * @param settings Settings to validate
 */
export async function validateProviderSettings(
  provider: IntegrationProvider,
  settings: any
) {
  const integration = _getIntegrationProvider(provider, settings);
  await integration.validateSettings();
  return integration.getSettingsUpdates() ?? settings;
}

/**
 * Update the integration settings in the DB if needed
 *
 * @param integration DB integration object
 * @param provider Integration provider object
 */
export async function updateIntegrationSettings(
  integration: Integration,
  provider: Provider
) {
  const settings = provider.getSettingsUpdates();
  if (settings) {
    await integration.update({ settings });
  }
}

/**
 * Get the fields available for an auto responder integration.
 *
 * @param integration DB integration object
 */
export async function getIntegrationFields(integration: Integration) {
  const provider = getIntegrationProvider(integration);
  if (!(provider instanceof AutoResponderProvider)) {
    throw new Error("Invalid integration provider: " + integration.provider);
  }

  try {
    return await provider.getFields();
  } finally {
    await updateIntegrationSettings(integration, provider);
  }
}
