import { IsString } from "class-validator";
import { Attendee } from "../db/entities/Attendee";
import {
  AutoResponderProvider,
  BaseProvider,
  EmailProvider,
  SMSProvider,
} from "./base";
import { AutoResponderField, Email, FieldName, SMS } from "./types";

class Validator {
  @IsString()
  test: string;
}

describe("Integrations Base", () => {
  describe("BaseProvider", () => {
    class Provider extends BaseProvider<Validator, Validator> {
      settingsValidator = Validator;
      gatewaySettingsValidator = Validator;

      async checkSettings(): Promise<void> {}

      async checkGatewaySettings(): Promise<void> {}
    }

    test("Validates settings", async () => {
      const provider = new Provider({} as any);
      await expect(provider.validateSettings()).rejects.toThrowError();
    });

    test("Invalid if settings check fails", async () => {
      const provider = new Provider({
        test: "test",
      });
      jest
        .spyOn(provider, "checkSettings")
        .mockRejectedValueOnce(new Error("Unable to communicate with service"));
      await expect(provider.validateSettings()).rejects.toThrowError(
        "Unable to communicate with service"
      );
    });

    test("Validates gateway settings", async () => {
      const provider = new Provider({ test: "" });
      await expect(provider.validateGatewaySettings()).rejects.toThrowError();
    });

    test("Invalid if gateway settings check fails", async () => {
      const provider = new Provider({
        test: "test",
      });
      jest
        .spyOn(provider, "checkGatewaySettings")
        .mockRejectedValueOnce(new Error("Unable to communicate with service"));

      provider.setGatewaySettings({ test: "" });

      await expect(provider.validateGatewaySettings()).rejects.toThrowError(
        "Unable to communicate with service"
      );
    });
  });

  describe("EmailProvider", () => {
    class Provider extends EmailProvider<Validator> {
      settingsValidator = Validator;

      // @ts-ignore: unused variable
      async sendEmail(email: Email): Promise<void> {}

      async checkSettings(): Promise<void> {}

      async checkGatewaySettings(): Promise<void> {}
    }

    test("No gateway settings needed", async () => {
      const provider = new Provider({ test: "" });
      await provider.validateSettings();
      await provider.validateGatewaySettings();
    });
  });

  describe("SMSProvider", () => {
    class Provider extends SMSProvider<Validator> {
      settingsValidator = Validator;

      // @ts-ignore: unused variable
      sendSMS(sms: SMS): Promise<void> {
        throw new Error("Method not implemented.");
      }

      async checkSettings(): Promise<void> {}

      async checkGatewaySettings(): Promise<void> {}
    }

    test("No gateway settings needed", async () => {
      const provider = new Provider({ test: "" });
      await provider.validateSettings();
      await provider.validateGatewaySettings();
    });
  });

  describe("AutoResponderProvider", () => {
    class Provider extends AutoResponderProvider<Validator> {
      settingsValidator = Validator;

      async addContact(
        // @ts-ignore
        attendee: Attendee,
        // @ts-ignore
        fieldMapping: Record<FieldName, string>
      ): Promise<void> {}

      async getFields(): Promise<AutoResponderField[]> {
        return [];
      }

      protected async checkSettings(): Promise<void> {}
    }

    test("Validates gateway settings", async () => {
      const provider = new Provider({ test: "" });
      provider.setGatewaySettings({
        date: "",
        live_link: "",
        replay_link: "",
      } as any);

      await expect(provider.validateGatewaySettings()).rejects.toThrowError();
    });

    test("Checks gateway field mapping contains valid fields", async () => {
      const provider = new Provider({ test: "" });
      // jest.spyOn(provider, "getFields").;
      provider.setGatewaySettings({
        fields: {
          date: "date",
          live_link: "live_link",
          replay_link: "replay_link",
          time: "time",
          timezone: "timezone",
          webinar_name: "webinar_name",
        },
      });

      await expect(provider.validateGatewaySettings()).rejects.toThrowError();
    });

    test("Checks gateway field mapping is valid with valid fields", async () => {
      const provider = new Provider({ test: "" });
      jest.spyOn(provider, "getFields").mockReturnValue(
        Promise.resolve([
          {
            label: "",
            value: "webinar_name",
          },
          {
            label: "",
            value: "date",
          },
          {
            label: "",
            value: "time",
          },
          {
            label: "",
            value: "live_link",
          },
          {
            label: "",
            value: "replay_link",
          },
          {
            label: "",
            value: "timezone",
          },
        ])
      );
      provider.setGatewaySettings({
        fields: {
          date: "date",
          live_link: "live_link",
          replay_link: "replay_link",
          time: "time",
          timezone: "timezone",
          webinar_name: "webinar_name",
        },
      });

      await provider.validateGatewaySettings();
    });
  });
});
