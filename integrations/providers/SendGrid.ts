import axios from "axios";
import { IsEmail, IsString } from "class-validator";
import { EmailProvider } from "../base";
import { Email } from "../types";

class SettingsValidator {
  @IsString()
  apiKey: string;

  @IsString()
  fromName: string;

  @IsEmail()
  fromEmail: string;
}

export class SendGridProvider extends EmailProvider<SettingsValidator> {
  settingsValidator = SettingsValidator;

  async sendEmail(email: Email): Promise<void> {
    // Who needs an SDK package?
    const data = {
      from: {
        email: this.settings.fromEmail,
        name: this.settings.fromName,
      },
      personalizations: [
        {
          to: [
            {
              email: email.to.email,
              name: email.to.name,
            },
          ],
        },
      ],
      subject: email.subject,
      content: [
        {
          type: "text/html",
          value: email.content,
        },
      ],
    };

    try {
      await axios.post("https://api.sendgrid.com/v3/mail/send", data, {
        headers: {
          Authorization: "Bearer " + this.settings.apiKey,
        },
      });
    } catch (err) {
      throw new Error(err.response.data.errors[0].message);
    }
  }

  protected async checkSettings() {
    await this.sendEmail({
      content: "Test",
      subject: "Configuration Test",
      to: {
        email: "test@example.com",
        name: "Test",
      },
    });
  }
}
