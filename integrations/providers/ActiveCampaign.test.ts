import { ActiveCampaignProvider } from "./ActiveCampaign";
import MockAdapter from "axios-mock-adapter";
import axios from "axios";

describe("Active Campaign Integration Provider", () => {
  test("Can list fields", async () => {
    const mock = new MockAdapter(axios);

    mock
      .onGet("https://emsyte53229.api-us1.com/api/3/fields")
      .reply(async (config) => {
        expect(config.headers["Api-Token"]).toEqual("my-test-api-token");

        return [
          200,
          {
            fields: [
              { title: "Live Link", id: "1" },
              { title: "Replay Link", id: "2" },
              { title: "Date", id: "3" },
              { title: "Time", id: "4" },
              { title: "Time zone", id: "5" },
              { title: "Webinar Name", id: "6" },
              { title: "Test", id: "7" },
            ],
          },
        ];
      });

    const provider = new ActiveCampaignProvider({
      api_url: "https://emsyte53229.api-us1.com",
      api_key: "my-test-api-token",
    });

    const fields = await provider.getFields();

    expect(fields).toEqual([
      { label: "Live Link", value: "1" },
      { label: "Replay Link", value: "2" },
      { label: "Date", value: "3" },
      { label: "Time", value: "4" },
      { label: "Time zone", value: "5" },
      { label: "Webinar Name", value: "6" },
      { label: "Test", value: "7" },
    ]);
  });
});
