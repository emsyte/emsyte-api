import axios from "axios";
import { IsString } from "class-validator";
import { AutoResponderProvider } from "../base";
import { AutoResponderField, Contact, FieldMapping, FieldName } from "../types";

class SettingsValidator {
  @IsString()
  api_key: string;
}

export class HubSpotProvider extends AutoResponderProvider<SettingsValidator> {
  settingsValidator = SettingsValidator;

  async addContact(contact: Contact, fieldValues: FieldMapping): Promise<void> {
    const extra = Object.entries(fieldValues).reduce<Record<string, string>>(
      (props, [field, value]) => {
        props[this.gatewaySettings.fields[field as FieldName]] = value;
        return props;
      },
      {}
    );

    await axios.post(
      "https://api.hubapi.com/crm/v3/objects/contacts",
      {
        properties: {
          // TODO: Ideally, these 4 would also be configured
          email: contact.email,
          firstname: contact.firstName,
          lastname: contact.lastName,
          phone: contact.phone,
          ...extra,
        },
      },
      {
        params: {
          hapikey: this.settings.api_key,
        },
      }
    );
  }

  async getFields(): Promise<AutoResponderField[]> {
    const response = await axios.get(
      "https://api.hubapi.com/crm/v3/properties/contacts",
      {
        params: {
          hapikey: this.settings.api_key,
        },
      }
    );
    return response.data.results.map((result) => ({
      label: result.label,
      value: result.name,
    }));
  }

  protected async checkSettings(): Promise<void> {
    await this.getFields();
  }
}
