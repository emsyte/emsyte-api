import axios, { AxiosInstance } from "axios";
import { IsString } from "class-validator";
import { AutoResponderProvider } from "../base";
import { AutoResponderField, Contact, FieldMapping } from "../types";

class SettingsValidator {
  @IsString()
  authorization_code: string;

  @IsString()
  access_token: string;

  @IsString()
  refresh_token: string;
}

const client_id = "b293c218-25e5-41ef-9a44-bdadf0653cff";
const client_secret = "7cGEKRhGUStxAaOj-VjfGw";
const redirect_uri = process.env.APP_DOMAIN + "/oauth/constantcontact";

export class ConstantContactProvider extends AutoResponderProvider<SettingsValidator> {
  settingsValidator = SettingsValidator;

  axios: AxiosInstance;

  constructor(settings: SettingsValidator) {
    super(settings);

    this.axios = axios.create();

    this._setupAxiosInterceptors();
  }

  private _setupAxiosInterceptors() {
    // Refresh Token Configuration
    this.axios.interceptors.response.use(
      (response) => response,
      async (error) => {
        const is_auth_error = error.response?.status === 401;

        if (!is_auth_error) {
          return Promise.reject(error);
        }

        await this.refreshTokens();

        error.config.headers["Authorization"] =
          "Bearer " + this.settings.access_token;

        // Retry the original request
        return this.axios.request(error.config);
      }
    );
  }

  private async getAccessTokenFromCode() {
    const response = await axios.post(
      "https://idfed.constantcontact.com/as/token.oauth2",
      {},
      {
        params: {
          code: this.settings.authorization_code,
          redirect_uri,
          grant_type: "authorization_code",
        },
        auth: {
          username: client_id,
          password: client_secret,
        },
      }
    );

    const { access_token, refresh_token } = response.data;
    this.updateSettings({
      authorization_code: "",
      access_token,
      refresh_token,
    });
    this.updateAxiosAuth();
  }

  private async refreshTokens() {
    const response = await axios.post(
      `https://idfed.constantcontact.com/as/token.oauth2`,
      {},
      {
        params: {
          refresh_token: this.settings.refresh_token,
          grant_type: "refresh_token",
        },
        auth: {
          username: client_id,
          password: client_secret,
        },
      }
    );

    const { access_token, refresh_token } = response.data;

    this.updateSettings({
      authorization_code: "",
      access_token,
      refresh_token,
    });
    this.updateAxiosAuth();
  }

  private updateAxiosAuth() {
    this.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${this.settings.access_token}`;
  }

  async addContact(contact: Contact, fieldValues: FieldMapping): Promise<void> {
    await axios.post("https://api.cc.email/v3/contacts/sign_up_form", {
      email_address: contact.email,
      first_name: contact.firstName,
      last_name: contact.lastName,
      phone_number: contact.phone,
      custom_fields: Object.entries(fieldValues).map(([field, value]) => ({
        custom_field_id: this.gatewaySettings.fields[field],
        value,
      })),
    });
  }

  async getFields(): Promise<AutoResponderField[]> {
    const response = await this.axios.get(
      "https://api.cc.email/v3/contact_custom_fields"
    );

    return response.data.custom_fields.map((result) => ({
      label: result.label,
      value: result.custom_field_id,
    }));
  }

  protected async checkSettings(): Promise<void> {
    if (this.settings.authorization_code && !this.settings.access_token) {
      await this.getAccessTokenFromCode();
    }

    await this.getFields();
  }
}
