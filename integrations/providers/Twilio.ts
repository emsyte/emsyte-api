import axios from "axios";
import { IsPhoneNumber, IsString } from "class-validator";
import * as qs from "querystring";
import { SMSProvider } from "../base";
import { SMS } from "../types";

class SettingsValidator {
  @IsString()
  accountSid: string;

  @IsString()
  authToken: string;

  @IsPhoneNumber(null)
  fromPhone: string;
}
export class TwilioProvider extends SMSProvider<SettingsValidator> {
  settingsValidator = SettingsValidator;

  async sendSMS(message: SMS) {
    await axios.post(
      `https://api.twilio.com/2010-04-01/Accounts/${this.settings.accountSid}/Messages.json`,
      qs.stringify({
        Body: message.content,
        To: message.phone,
        From: this.settings.fromPhone,
      }),
      {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        auth: {
          username: this.settings.accountSid,
          password: this.settings.authToken,
        },
      }
    );
  }

  async checkSettings() {
    const response = await axios.get(
      `https://api.twilio.com/2010-04-01/Accounts/${this.settings.accountSid}/IncomingPhoneNumbers.json`,

      {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        auth: {
          username: this.settings.accountSid,
          password: this.settings.authToken,
        },
      }
    );

    for (const incomingPhoneNumber of response.data.incoming_phone_numbers) {
      if (incomingPhoneNumber.phone_number === this.settings.fromPhone) {
        return;
      }
    }

    throw new Error(
      `Phone number ${this.settings.fromPhone} in not provisioned in your Twilio account`
    );
  }
}
