import axios from "axios";
import { IsString } from "class-validator";
import { AutoResponderProvider } from "../base";
import { AutoResponderField, Contact, FieldMapping, FieldName } from "../types";

class SettingsValidator {
  @IsString()
  api_url: string;

  @IsString()
  api_key: string;
}

export class ActiveCampaignProvider extends AutoResponderProvider<SettingsValidator> {
  settingsValidator = SettingsValidator;

  async addContact(contact: Contact, fieldValues: FieldMapping): Promise<void> {
    await axios.post(
      `${this.settings.api_url}/api/3/contact/sync`,
      {
        contact: {
          email: contact.email,
          firstName: contact.firstName,
          lastName: contact.lastName,
          phone: contact.phone,
          fieldValues: Object.entries(fieldValues).map(([field, value]) => ({
            field: this.gatewaySettings.fields[field as FieldName],
            value,
          })),
        },
      },
      {
        headers: {
          "Api-Token": this.settings.api_key,
        },
      }
    );
  }

  async getFields(): Promise<AutoResponderField[]> {
    const response = await axios.get(this.settings.api_url + "/api/3/fields", {
      headers: { "Api-Token": this.settings.api_key },
    });

    return response.data.fields.map((field) => ({
      label: field.title,
      value: field.id,
    }));
  }

  protected async checkSettings(): Promise<void> {
    await this.getFields();
  }
}
