import { HubSpotProvider } from "./HubSpot";
import MockAdapter from "axios-mock-adapter";
import axios from "axios";

describe("Hubspot Integration Provider", () => {
  test("Can list fields", async () => {
    const mock = new MockAdapter(axios);

    mock
      .onGet("https://api.hubapi.com/crm/v3/properties/contacts")
      .reply(async (config) => {
        expect(config.params["hapikey"]).toEqual("my-test-api-token");

        return [
          200,
          {
            results: [
              {
                updatedAt: "2019-08-09T21:09:49.092Z",
                createdAt: "2019-08-06T02:41:06.638Z",
                name: "company_size",
                label: "Company size",
                type: "string",
                fieldType: "text",
                description:
                  "A contact's company size. This property is required for the Facebook Ads Integration. This property will be automatically synced via the Lead Ads tool",
                groupName: "contactinformation",
                options: [],
                displayOrder: -1,
                calculated: false,
                externalOptions: false,
                hasUniqueValue: false,
                hidden: false,
                hubspotDefined: true,
                modificationMetadata: {
                  archivable: false,
                  readOnlyDefinition: false,
                  readOnlyValue: false,
                },
                formField: true,
              },
            ],
          },
        ];
      });

    const provider = new HubSpotProvider({
      api_key: "my-test-api-token",
    });

    const fields = await provider.getFields();
    expect(fields).toEqual([
      {
        label: "Company size",
        value: "company_size",
      },
    ]);
  });

  test("Can add contact", async () => {
    const mock = new MockAdapter(axios);

    mock
      .onPost("https://api.hubapi.com/crm/v3/objects/contacts")
      .reply(async (config) => {
        expect(config.params["hapikey"]).toEqual("my-test-api-token");

        return [
          200,
          {
            results: [
              {
                id: "2351",
                properties: {
                  company: "Biglytics",
                  createdate: "2021-07-21T04:52:45.785Z",
                  email: "bcooper@biglytics.net",
                  firstname: "Bryan",
                  hs_calculated_phone_number: "+18779290687",
                  hs_calculated_phone_number_country_code: "US",
                  hs_email_domain: "biglytics.net",
                  hs_is_unworked: "true",
                  hs_marketable_status: "false",
                  hs_marketable_until_renewal: "false",
                  hs_object_id: "2351",
                  hs_searchable_calculated_phone_number: "8779290687",
                  lastmodifieddate: "2021-07-21T04:52:45.785Z",
                  lastname: "Cooper",
                  phone: "(877) 929-0687",
                  website: "http://biglytics.net",
                },
                createdAt: "2021-07-21T04:52:45.785Z",
                updatedAt: "2021-07-21T04:52:45.785Z",
                archived: false,
              },
            ],
          },
        ];
      });
    const provider = new HubSpotProvider({
      api_key: "my-test-api-token",
    });
    provider.setGatewaySettings({
      fields: {
        date: "webinar_date",
        time: "webinar_time",
        timezone: "webinar_timezone",
        webinar_name: "webinar_name",
        replay_link: "webinar_replay_link",
        live_link: "webinar_live_link",
      },
    });
    await provider.addContact(
      {
        firstName: "Isaac",
        lastName: "Weaver",
        phone: "",
        email: "isaac@emsyte.io",
      },
      {
        date: "webinar_date",
        time: "webinar_time",
        timezone: "webinar_timezone",
        webinar_name: "webinar_name",
        replay_link: "webinar_replay_link",
        live_link: "webinar_live_link",
      }
    );
  });
});
