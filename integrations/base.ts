import { Type } from "class-transformer";
import { IsOptional, IsString, ValidateNested } from "class-validator";
import { Attendee } from "../db/entities/Attendee";
import RouterError from "../utils/RouterError";
import { runValidation } from "../utils/validator";
import {
  AutoResponderField,
  Email,
  FieldMapping,
  FieldName,
  FIELDS,
  SMS,
} from "./types";

interface Validator<T> {
  new (): T;
}

export abstract class BaseProvider<TSettings, TGatewaySettings> {
  public readonly settingsValidator: Validator<TSettings>;
  public readonly gatewaySettingsValidator: Validator<TGatewaySettings>;

  protected settings: TSettings;
  protected gatewaySettings: TGatewaySettings;

  constructor(settings: TSettings) {
    this.settings = settings;
  }

  private _settingsDirty = false;

  /**
   * Allow a provider to mark settings as needing to be updated
   *
   * @param settings The new settings to set
   */
  protected updateSettings(settings: TSettings) {
    this.settings = settings;
    this._settingsDirty = true;
  }

  /**
   * Get the new settings if there are any updates needed
   */
  getSettingsUpdates() {
    if (this._settingsDirty) {
      return this.settings;
    }
    return null;
  }

  /**
   * Validate the settings
   */
  async validateSettings() {
    if (this.settingsValidator) {
      await runValidation(
        this.settingsValidator,
        this.settings,
        "Invalid settings"
      );
    }

    await this.checkSettings();
  }

  async validateGatewaySettings() {
    if (this.gatewaySettingsValidator) {
      await runValidation(
        this.gatewaySettingsValidator,
        this.gatewaySettings,
        "Invalid gateway settings"
      );
    }

    await this.checkGatewaySettings();
  }

  setGatewaySettings(settings: TGatewaySettings) {
    this.gatewaySettings = settings;
  }

  getGatewaySettings() {
    return this.gatewaySettings;
  }

  /**
   * Check that the provided settings can be used to communicate with the service
   *
   * @throws If it can't communicate with the service using the settings
   */
  protected abstract checkSettings(): Promise<void>;

  /**
   * Check that the provided gateway settings can be used to communicate with the service
   *
   * @throws If it can't communicate with the service using the gateway settings
   */
  protected abstract checkGatewaySettings(): Promise<void>;
}

export abstract class EmailProvider<TSettings> extends BaseProvider<
  TSettings,
  {}
> {
  /**
   * Send an email
   *
   * @param email The email object so send
   */
  abstract sendEmail(email: Email): Promise<void>;

  protected async checkGatewaySettings(): Promise<void> {}
}

export abstract class SMSProvider<TSettings> extends BaseProvider<
  TSettings,
  {}
> {
  /**
   * Send an SMS message
   *
   * @param sms The SMS message to send
   */
  abstract sendSMS(sms: SMS): Promise<void>;

  protected async checkGatewaySettings(): Promise<void> {}
}

class AutoResponderGatewayFieldMapping implements FieldMapping {
  @IsString()
  webinar_name: string;

  @IsString()
  live_link: string;

  @IsString()
  @IsOptional()
  replay_link?: string;

  @IsString()
  date: string;

  @IsString()
  time: string;

  @IsString()
  timezone: string;
}

class AutoResponderGatewayValidator {
  @ValidateNested()
  @Type(() => AutoResponderGatewayFieldMapping)
  fields: AutoResponderGatewayFieldMapping;
}

export abstract class AutoResponderProvider<TSettings> extends BaseProvider<
  TSettings,
  { fields: FieldMapping }
> {
  gatewaySettingsValidator = AutoResponderGatewayValidator;
  /**
   * Send an email
   *
   * @param email The email object so send
   * @param fieldMapping The configured field mapping
   */
  abstract addContact(
    attendee: Attendee,
    fieldMapping: FieldMapping
  ): Promise<void>;

  /**
   * Get the available fields that can be configured for this integration
   */
  abstract getFields(): Promise<AutoResponderField[]>;

  /**
   * Validate that a given field mapping is valid
   *
   * @param mapping The configured field mapping
   */
  async checkGatewaySettings() {
    const fields = await this.getFields();
    const fieldValues = fields.map((field) => field.value);

    for (const [field, fieldValue] of Object.entries(
      this.gatewaySettings.fields
    )) {
      if (!FIELDS.includes(field as FieldName)) {
        throw RouterError.basic(`Can't configure field "${field}"`, 422);
      }

      if (!fieldValues.includes(fieldValue)) {
        throw RouterError.basic(`Unknown field "${field}"`, 422);
      }
    }
  }
}
