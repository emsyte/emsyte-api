export interface EmailName {
  email: string;
  name: string;
}

export interface Email {
  to: EmailName;
  subject: string;
  content: string;
}

export interface SMS {
  phone: string;
  content: string;
}

export const FIELDS = [
  "live_link",
  "replay_link",
  "date",
  "time",
  "timezone",
  "webinar_name",
] as const;

export type FieldName = typeof FIELDS[number];

export interface AutoResponderField {
  label: string;
  value: string;
}

export type FieldMapping = Partial<Record<FieldName, string>>;

export interface Contact {
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
}
