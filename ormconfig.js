const baseOptions = {
  entities: ["db/entities/**/*.ts"],
  migrations: ["db/migration/**/*.ts"],
  subscribers: ["db/subscriber/**/*.ts"],
  cli: {
    entitiesDir: "db/entities",
    migrationsDir: "db/migration",
    subscribersDir: "db/subscriber",
  },
};

module.exports = [
  {
    name: "default",
    username: "emsyte",
    password: "emsyte",
    database: "justwebinar",
    host: "127.0.0.1",
    port: "5432",
    type: "postgres",
    // synchronize: true,

    ...baseOptions,
  },
  {
    name: "staging",
    type: "aurora-data-api-pg",
    database: "justwebinar",
    secretArn:
      "arn:aws:secretsmanager:us-east-1:592292237331:secret:db-justwebinar-staging-LnA9wC",
    resourceArn:
      "arn:aws:rds:us-east-1:592292237331:cluster:db-justwebinar-staging",
    region: "us-east-1",

    ...baseOptions,
  },
];
