"use strict";

module.exports = {
  generateRandomData,
};

// Make sure to "npm install faker" first.
const Faker = require("faker");

function generateRandomData(userContext, events, done) {
  userContext.vars.firstName = Faker.name.firstName();
  userContext.vars.lastName = Faker.name.lastName();
  userContext.vars.email = Faker.internet.exampleEmail();

  // continue with executing the scenario:
  return done();
}
