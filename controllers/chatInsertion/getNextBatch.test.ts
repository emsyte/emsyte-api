import { handler } from "./getNextBatch";
import { OutputBatchEvent } from "./types";
import * as moment from "moment";

function getExpectedBatch(
  index: number | null,
  resultSize: number
): OutputBatchEvent {
  return {
    eventId: 1,
    index,
    chats: Array.from({ length: resultSize }).map(() => ({
      channelId: "event-1",
      content: expect.any(String),
      name: expect.any(String),
      label: expect.any(String),
      timestamp: expect.any(String),
    })),
  };
}

function assertInOrder(chats: OutputBatchEvent["chats"]) {
  chats.reduce((prev, cur) => {
    if (prev) {
      const isBefore = moment(cur.timestamp).isBefore(moment(prev.timestamp));
      if (isBefore) {
        console.error(prev, cur);
      }
      expect(isBefore).toBeFalsy();
    }

    return cur;
  }, null);
}

describe("getNextBatch task", () => {
  test("Can get first batch", async () => {
    const result = await handler(
      {
        eventId: 1,
      },
      null,
      null
    );
    expect(result).toEqual(getExpectedBatch(100, 100));
    if (result) {
      assertInOrder(result.chats);
    }
  });

  test("Can get second batch", async () => {
    const result = await handler(
      {
        eventId: 1,
        index: 100,
      },
      null,
      null
    );

    expect(result).toEqual(getExpectedBatch(200, 100));
    if (result) {
      assertInOrder(result.chats);
    }
  });

  test("Can get third batch", async () => {
    const result = await handler(
      {
        eventId: 1,
        index: 200,
      },
      null,
      null
    );

    expect(result).toEqual(getExpectedBatch(300, 100));
    if (result) {
      assertInOrder(result.chats);
    }
  });

  test("Can get forth batch", async () => {
    const result = await handler(
      {
        eventId: 1,
        index: 300,
      },
      null,
      null
    );

    expect(result).toEqual(getExpectedBatch(null, 50));
    if (result) {
      assertInOrder(result.chats);
    }
  });
});
