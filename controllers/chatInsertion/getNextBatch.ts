import { ChatMessage } from "../../db/entities/ChatMessage";
import { Event } from "../../db/entities/Event";
import { TaskRunner } from "../../utils/TaskRunner";
import { BatchEvent, ChatToInsert, OutputBatchEvent } from "./types";
import * as moment from "moment";

const BATCH_SIZE = 100;

function serializeChat(event: Event, chat: ChatMessage): ChatToInsert {
  return {
    content: chat.content,
    name: chat.name,
    label: chat.label,
    timestamp: moment(event.start)
      .add(chat.offset_timestamp, "seconds")
      .toISOString(),
    channelId: `event-${event.id}`,
  };
}

class BatchHandler extends TaskRunner<BatchEvent, OutputBatchEvent> {
  async run(event: BatchEvent) {
    const index = event.index ?? 0;
    const event_ = await Event.findOne(event.eventId);

    const [chats, count] = await ChatMessage.findAndCount({
      where: {
        webinar: { id: event_.webinarId },
      },
      order: {
        offset_timestamp: "ASC",
      },
      take: BATCH_SIZE,
      skip: index,
    });

    const nextIndex = index + BATCH_SIZE;

    return {
      ...event,
      index: nextIndex < count ? nextIndex : null,
      chats: chats.map((chat) => serializeChat(event_, chat)),
    };
  }
}

export const handler = new BatchHandler().getHandler();
