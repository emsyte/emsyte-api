export interface ChatToInsert {
  timestamp: string;
  name: string;
  content: string;
  channelId: string;
  label: string;
}

export interface BatchEvent {
  eventId: number;
  index?: number;
}

export interface OutputBatchEvent extends BatchEvent {
  chats: ChatToInsert[];
}
