import { sendSingleMessage } from "../websocket";
import { ChatToInsert } from "./types";

export async function handler(event: ChatToInsert) {
  // TODO: Ensure that the event is sent the correct data
  await sendSingleMessage(
    {
      channelId: event.channelId,
      content: event.content,
      question: false,
      replyTo: null,
      sendTo: "general",
    },
    {
      name: event.name,
      label: event.label,
      userId: "",
    }
  );
}
