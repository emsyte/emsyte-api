import {
  APITemplateCreateData,
  APITemplateInstanceCreateData,
  APITemplateInstanceUpdateData,
  APITemplateUpdateData,
  TemplateGrapesJSData,
  TemplateType,
} from "@emsyte/common/template";
import { WebinarStatus } from "@emsyte/common/webinar";
import * as handlebars from "handlebars";
import * as moment from "moment";
import { DeepPartial, FindOneOptions, MoreThan } from "typeorm";
import { Attendee } from "../db/entities/Attendee";
import { Event } from "../db/entities/Event";
import { Notification } from "../db/entities/Notification";
import { Team } from "../db/entities/Team";
import { Template, TemplateInstance } from "../db/entities/Template";
import { Webinar } from "../db/entities/Webinar";
import { serializeAttendee } from "../lib/serializers/attendeeSerializer";
import { serializeEvent } from "../lib/serializers/eventSerializer";
import { serializeTeam } from "../lib/serializers/teamSerializer";
import { serializeWebinar } from "../lib/serializers/webinarSerializer";
import { unknownValue } from "../utils/conditions";
import RouterError from "../utils/RouterError";
import { deleteFile, duplicateFile } from "../utils/storage";
import { getWebinar } from "./webinars/base";

const FIELDS = ["assets", "components", "css", "html", "styles"] as const;

const { EVENTS_DOMAIN } = process.env;

function getBodyExtra(webinar: Webinar, type: TemplateType) {
  if (type === TemplateType.REGISTRATION) {
    return `
    ${webinar.settings.registrationPageScript}
    <script src="${EVENTS_DOMAIN}/embed.js" id="justwebinar-script" async></script>
    `;
    // TODO:
  } else if (type === TemplateType.CONFIRMATION) {
    return webinar.settings.thankyouPageScript;
  }

  return "";
}

// Don't error if deleting fails
async function silentDelete(path: string) {
  try {
    await deleteFile(path);
  } catch (error) {
    console.warn(`Couldn't delete file ${path}`);
    console.error(error);
  }
}

async function deleteThumbnailIfChanged(
  thumbnail: string,
  newThumbnail: string
) {
  if (thumbnail === newThumbnail) {
    return;
  }

  silentDelete(thumbnail);
}

function getContent(data: TemplateGrapesJSData) {
  const content = FIELDS.reduce<Record<typeof FIELDS[number], string>>(
    (content, field) => {
      content[field] = data[field];
      return content;
    },
    {} as any
  );
  return JSON.stringify(content);
}

export async function doGetTemplate(
  team: Team,
  templateId: number,
  write = false
) {
  const template = await Template.findOne(templateId);

  if (!template || (!template.base && template.teamId !== team.id)) {
    throw new RouterError("Template not found", 404, "Template not found");
  }

  if (write && template.base) {
    throw RouterError.basic("Can't edit base template", 403);
  }

  return template;
}

export const doGetAllTemplates = async (team: Team) => {
  const templates = await Template.createQueryBuilder("template")
    .select([
      "template.name",
      "template.type",
      "template.thumbnail",
      "template.base",
      "template.id",
      "template.teamId",
    ])
    .where('template."teamId" = :teamId', { teamId: team.id })
    .orWhere("base=true")
    .getMany();

  return templates;
};

export async function doGetTemplateInstances(team: Team, webinarId: number) {
  const webinar = await getWebinar(team, webinarId);
  return await TemplateInstance.find({
    webinar,
  });
}

export async function doCreateTemplateInstance(
  team: Team,
  webinarId: number,
  data: APITemplateInstanceCreateData
) {
  const webinar = await getWebinar(team, webinarId);
  const { content, thumbnail, type } = await doGetTemplate(team, data.parentId);

  const newThumbnail = await tryDuplicateFile(thumbnail);

  return await TemplateInstance.create({
    team,
    content,
    thumbnail: newThumbnail,
    type,
    webinar,
    parentId: data.parentId,
  }).save();
}

export async function doDeleteTemplateInstance(team: Team, instanceId: number) {
  const instance = await doGetTemplateInstance(team, instanceId);
  await silentDelete(instance.thumbnail);
  return await instance.remove();
}

export async function doGetTemplateInstance(
  team: Team,
  instanceId: number,
  options?: FindOneOptions<TemplateInstance>
) {
  const instance = await TemplateInstance.findOne(
    {
      team,
      id: instanceId,
    },
    options
  );
  if (!instance) {
    throw RouterError.basic("Template instance not found", 404);
  }
  return instance;
}

export async function doUpdateTemplateInstance(
  team: Team,
  instanceId: number,
  data: APITemplateInstanceUpdateData
) {
  const templateInstance = await doGetTemplateInstance(team, instanceId);
  const content = getContent(data);

  await deleteThumbnailIfChanged(templateInstance.thumbnail, data.thumbnail);

  return templateInstance.update({
    content,
    thumbnail: data.thumbnail,
  });
}

export async function doResetTemplateInstance(team: Team, instanceId: number) {
  const instance = await doGetTemplateInstance(team, instanceId, {
    relations: ["parent"],
  });
  const { content, thumbnail } = instance.parent;

  const newThumbnail = await tryDuplicateFile(thumbnail);
  await deleteThumbnailIfChanged(instance.thumbnail, newThumbnail);

  return await instance.update({ content, thumbnail: newThumbnail });
}

export async function doCreateTemplateFromInstance(
  team: Team,
  data: APITemplateCreateData
) {
  const instance = await doGetTemplateInstance(team, data.instanceId);
  const { content, thumbnail, type } = instance;

  const newThumbnail = await tryDuplicateFile(thumbnail);

  return await Template.create({
    base: false,
    team,
    name: data.name,
    type,
    content,
    thumbnail: newThumbnail,
  }).save();
}

async function tryDuplicateFile(thumbnail: string) {
  try {
    return await duplicateFile(thumbnail);
  } catch (err) {
    console.log("Failed to duplicate file");
    console.error(err);
    return "";
  }
}

export async function doUpdateTemplateFromInstance(
  team: Team,
  templateId: number,
  data: APITemplateUpdateData
) {
  const template = await doGetTemplate(team, templateId, true);

  const update: DeepPartial<Template> = {};

  if (data.name) {
    Object.assign(update, { name: data.name });
  }

  if (typeof data.instanceId === "number") {
    const instance = await doGetTemplateInstance(team, data.instanceId);

    const { content, thumbnail } = instance;
    const newThumbnail = await tryDuplicateFile(thumbnail);
    deleteThumbnailIfChanged(template.thumbnail, newThumbnail);

    Object.assign(update, {
      content,
      thumbnail: newThumbnail,
    });
  }

  return await template.update(update);
}

export async function doDeleteTemplate(team: Team, templateId: number) {
  const template = await doGetTemplate(team, templateId, true);

  await silentDelete(template.thumbnail);

  const instances = await TemplateInstance.createQueryBuilder()
    .select(["thumbnail"])
    .where({ parentId: template.id })
    .getRawMany<{ id: number; thumbnail: string }>();

  await Promise.all(instances.map(({ thumbnail }) => silentDelete(thumbnail)));

  await TemplateInstance.createQueryBuilder()
    .delete()
    .where({ parentId: template.id })
    .execute();

  return await template.remove();
}

export const doRenderEmailSubjectTemplate = async (
  attendee: Attendee,
  webinar: Webinar,
  notification: Notification,
  extraContext?: any
) => {
  const context = {
    event: serializeEvent(attendee.event),
    attendee: serializeAttendee(attendee),
    webinar: serializeWebinar(webinar),
    team: serializeTeam(webinar.team),
    ...extraContext,
  };

  const render = handlebars.compile(notification.subject);

  return render(context);
};

export const doRenderNotificationTemplate = async (
  attendee: Attendee,
  webinar: Webinar,
  notification: Notification,
  extraContext?: any
) => {
  const context = {
    event: serializeEvent(attendee.event),
    attendee: serializeAttendee(attendee),
    webinar: serializeWebinar(webinar),
    team: serializeTeam(webinar.team),
    ...extraContext,
  };

  handlebars.registerHelper("toJSON", function (object) {
    return new handlebars.SafeString(
      JSON.stringify(JSON.stringify(object)).slice(1, -1)
    );
  });

  const render = handlebars.compile(notification.content);

  return render(context);
};

export const doRenderPageTemplate = async (
  webinarId: number,
  type: TemplateType,
  extra?: any
) => {
  console.log("extra", extra);
  const webinar = await Webinar.findOne(
    {
      id: webinarId,
      ...Webinar.en("status", WebinarStatus.ACTIVE),
    },
    { relations: ["team"] }
  );

  const instances = await TemplateInstance.find({
    webinar: { id: webinarId },
    ...TemplateInstance.en("type", type),
  });
  const template = instances[0];

  const events = await Event.find({
    where: {
      webinar: { id: webinarId },
      canceled: false,
      start: MoreThan(new Date()),
    },
    order: {
      start: "ASC",
    },
    // Grab at most the next 10 events
    take: 10,
  });

  const webinarHash = Buffer.from(
    JSON.stringify({ id: webinar.id, templateId: template.id })
  ).toString("base64");

  const context = {
    events: events.map(serializeEvent),
    webinar: serializeWebinar(webinar),
    team: serializeTeam(webinar.team),
    webinarHash,
    ...extra,
  };

  const content = JSON.parse(template.content);

  handlebars.registerHelper("toJSON", function (object) {
    return new handlebars.SafeString(
      JSON.stringify(JSON.stringify(object)).slice(1, -1)
    );
  });

  handlebars.registerHelper("formatDate", function (datetime) {
    const timezone = extra?.attendee?.timezone;
    return moment.tz(datetime, timezone).format("LLLL z");
  });

  const title = getPageTitle(template.type, webinar.name);

  // TODO: We should probably add some SEO here
  const render = handlebars.compile(
    `<html>
      <head>
        <title>${title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script> 
<script src="/viewer-assets/jquery.inputmask.min.js"></script>
        <style>${content.css}</style>
      </head>
      <body>
        ${content.html}

        ${getBodyExtra(webinar, type)}
      </body>
    </html>`
  );

  return render(context);
};

function getPageTitle(type: TemplateType, name: string) {
  if (type === TemplateType.REGISTRATION) {
    return "Register for " + name;
  }
  if (type === TemplateType.CONFIRMATION) {
    return "You've been registered for " + name;
  }
  if (type === TemplateType.THANK_YOU) {
    return "Thank you for watching " + name;
  }
  return unknownValue(type);
}
