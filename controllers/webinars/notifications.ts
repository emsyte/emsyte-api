import {
  APINotificationCreateData,
  APINotificationUpdateData,
  NotificationDeliveryMethod,
  NotificationType,
} from "@emsyte/common/notification";
import * as _ from "lodash";
import { Attendee } from "../../db/entities/Attendee";
import { Event, SentEventNotification } from "../../db/entities/Event";
import { Integration } from "../../db/entities/Integration";
import { Notification } from "../../db/entities/Notification";
import { Team } from "../../db/entities/Team";
import { UnreachableCaseError } from "../../utils/errors";
import { doSendManyNotifications } from "../notifications";
import { getWebinar } from "./base";

// Basic CRUD

export async function doCreateNotification(
  team: Team,
  webinarId: number,
  data: APINotificationCreateData
) {
  const webinar = await getWebinar(team, webinarId);
  const notification = await Notification.create({
    webinar,
    ...data,
  }).save();

  return await notification.update(data);
}

export async function doGetNotifications(team: Team, webinarId: number) {
  const webinar = await getWebinar(team, webinarId);
  return await Notification.find({ webinar });
}

export async function doUpdateNotification(
  team: Team,
  webinarId: number,
  notificationId: number,
  data: APINotificationUpdateData
) {
  const webinar = await getWebinar(team, webinarId);
  const notification = await Notification.findOne({
    webinar,
    id: notificationId,
  });

  return await notification.update(data);
}

export async function doDeleteNotification(
  team: Team,
  webinarId: number,
  notificationId: number
) {
  const webinar = await getWebinar(team, webinarId);
  return await Notification.delete({
    id: notificationId,
    webinar,
  });
}

// Sending notifications
// TODO: This file should be just basic CRUD

/**
 * Get all the notifications of a particular type that are ready to be processed
 */
export async function getNotifications(
  type: NotificationType,
  timeWhere: string
) {
  return (
    Notification.createQueryBuilder("notification")
      .innerJoinAndSelect("notification.webinar", "webinar")

      // Email integration
      .leftJoinAndSelect("webinar.emailGateway", "emailGateway")
      .leftJoinAndSelect("emailGateway.integration", "emailIntegration")

      // SMS integration
      .leftJoinAndSelect("webinar.smsGateway", "smsGateway")
      .leftJoinAndSelect("smsGateway.integration", "smsIntegration")

      .innerJoinAndSelect("webinar.team", "team")
      .leftJoinAndMapMany(
        "notification.events",
        Event,
        "event",
        'event."webinarId" = webinar.id'
      )
      .leftJoin(
        "event.sentEventNotifications",
        "sent_notifications",
        'sent_notifications."notificationId" = notification.id'
      )
      .where('sent_notifications."notificationId" IS NULL')
      .andWhere("NOT event.canceled")
      .andWhere("notification.type = :type", Notification.en("type", type))
      .andWhere(timeWhere, { now: new Date() })
      .getMany()
  );
}

/**
 * Return all pre-event notifications that are ready to be sent
 */
export async function getPreEventNotifications() {
  return await getNotifications(
    NotificationType.PRE_EVENT,
    "(event.start - make_interval(hours => notification.hoursOffset)) < :now"
  );
}

/**
 * Return all post-event notifications that are ready to be sent
 */
export async function getPostEventNotifications() {
  return await getNotifications(
    NotificationType.POST_EVENT,
    "event.end + make_interval(hours => notification.hoursOffset) < :now"
  );
}

/**
 * Return all last minute notifications that are ready to be sent
 */
export async function getLastMinuteEventNotifications() {
  return await getNotifications(
    NotificationType.LAST_MINUTE,
    "event.start - '15 minutes'::interval < :now"
  );
}

/**
 * Given a list of events, get the integrations of a type that should be used to send notifications on those events.
 *
 * @param events List of events to get the integrations for
 */
export async function getIntegrationsFromTeams(teamIds: number[]) {
  if (teamIds.length === 0) {
    return [];
  }

  const integrations = await Integration.createQueryBuilder("integration")
    .where("active = true")
    .andWhere('"teamId" IN (:...teamIds)', { teamIds })
    .getMany();
  return integrations;
}

async function getAllNotifications() {
  const all = await Promise.all([
    getPreEventNotifications(),
    getPostEventNotifications(),
    getLastMinuteEventNotifications(),
  ]);
  return _.flatten(all);
}

function uniqueBy<T, K>(collection: T[], getKey: (item: T) => K): T[] {
  const map = new Map<K, T>();
  for (const item of collection) {
    map.set(getKey(item), item);
  }
  return [...map.values()];
}

function getGateway(notification: Notification) {
  if (notification.deliveryMethod === NotificationDeliveryMethod.SMS) {
    return notification.webinar.smsGateway;
  } else if (notification.deliveryMethod === NotificationDeliveryMethod.EMAIL) {
    return notification.webinar.emailGateway;
  }

  throw new UnreachableCaseError(notification.deliveryMethod);
}

/**
 * Send all the latest notifications
 */
export async function doSendLatestNotifications() {
  const notifications = await getAllNotifications();
  if (!notifications.length) {
    return;
  }

  const events = uniqueBy(
    _.flatten(
      notifications.map((notification) =>
        notification.events.map((event) => {
          // Add the webinar to the event
          event.webinar = notification.webinar;
          return event;
        })
      )
    ),
    (event) => event.id
  );

  const attendees = await Attendee.createQueryBuilder("a")
    .where('a."eventId" IN (:...eventIds)', {
      eventIds: events.map((e) => e.id),
    })
    .getMany();

  // Add the event to the attendee, and visa versa
  for (const attendee of attendees) {
    const event = events.find((event) => event.id === attendee.eventId);
    attendee.event = event;
    event.attendees = event.attendees ?? [];
    event.attendees.push(attendee);
  }

  for (const notification of notifications) {
    // Find the integration to use for this notification
    const integrationGateway = getGateway(notification);

    if (!integrationGateway) {
      continue;
    }

    const notificationAttendees = _.flatten(
      notification.events.map((event) => event.attendees ?? [])
    );

    await doSendManyNotifications(
      notification,
      integrationGateway,
      notificationAttendees
    );

    const notificationEvents = notification.events.map((event) => ({
      event,
      notification,
    }));

    await SentEventNotification.insert(notificationEvents);
  }
}
