import { IntegrationProvider } from "@emsyte/common/integration";
import { APINotificationGatewaySetData } from "@emsyte/common/webinar";
import { Integration } from "../../db/entities/Integration";
import { NotificationGateway } from "../../db/entities/NotificationsGateway";
import { Team } from "../../db/entities/Team";
import { getIntegrationFields } from "../../integrations/actions";
import RouterError from "../../utils/RouterError";
import { getWebinar } from "./base";

export const doSetNotificationGateway = async (
  team: Team,
  webinarId: number,
  type: "smsGateway" | "emailGateway",
  gateway: APINotificationGatewaySetData
) => {
  const webinar = await getWebinar(team, webinarId, {
    relations: ["emailGateway", "smsGateway"],
  });

  const integration = await Integration.findOne({
    team,
    ...Integration.en("provider", gateway.provider),
  });

  if (!integration) {
    throw RouterError.basic(`Provider ${gateway.provider} not configured`, 422);
  }

  if (webinar[type]) {
    await webinar[type].remove();
  }

  const notificationGateway = await NotificationGateway.create({
    integration,
    settings: gateway.settings,
  }).save();

  webinar[type] = notificationGateway;
  await webinar.save();

  return webinar;
};

export const doUpdateNotificationGateway = async (
  team: Team,
  webinarId: number,
  type: "smsGateway" | "emailGateway",
  gateway: APINotificationGatewaySetData
) => {
  const webinar = await getWebinar(team, webinarId, {
    relations: ["emailGateway", "smsGateway"],
  });

  await webinar[type].update({
    settings: gateway.settings,
  });
  return webinar;
};

export const doRemoveNotificationGateway = async (
  team: Team,
  webinarId: number,
  type: "smsGateway" | "emailGateway"
) => {
  const webinar = await getWebinar(team, webinarId, {
    relations: ["emailGateway", "smsGateway"],
  });

  await webinar[type].remove();

  webinar[type] = null;

  return webinar;
};

export const doGetNotificationGatewayFields = async (
  team: Team,
  provider: IntegrationProvider
) => {
  const integration = await Integration.findOne({
    team,
    ...Integration.en("provider", provider),
  });

  if (!integration) {
    throw RouterError.basic(`Provider ${provider} not configured`, 422);
  }

  return await getIntegrationFields(integration);
};
