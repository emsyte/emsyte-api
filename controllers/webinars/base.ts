import {
  APIWebinarCreateData,
  APIWebinarUpdateData,
  WebinarSettingsObject,
  WebinarStatus,
} from "@emsyte/common/webinar";
import { FindOneOptions, In } from "typeorm";
import { Event } from "../../db/entities/Event";
import { Team } from "../../db/entities/Team";
import { Webinar } from "../../db/entities/Webinar";
import RouterError from "../../utils/RouterError";
import { MoreThan } from "typeorm";
import moment = require("moment");

const { APP_DOMAIN } = process.env;

export async function getWebinar(
  team: Team,
  webinarId: number,
  options?: FindOneOptions<Webinar>
) {
  const webinar = await Webinar.findOne(
    {
      id: webinarId,
      team,
    },
    options
  );

  if (!webinar) {
    throw new RouterError("Webinar not found", 404, "Can't find webinar");
  }

  return webinar;
}

export const doGetWebinar = async (team: Team, id: number) => {
  const webinar = await getWebinar(team, id, {
    relations: ["emailGateway", "smsGateway"],
  });

  return webinar;
};

export const doGetAllWebinars = async (team: Team) => {
  const webinars = await Webinar.find({
    where: {
      team,
    },
    order: {
      createdDate: "DESC",
    },
  });

  return webinars;
};

function getDefaultSettings(webinar: Webinar): WebinarSettingsObject {
  const buff = Buffer.from(JSON.stringify({ id: webinar.id }));

  return {
    postRegistrationUrl: `${APP_DOMAIN}/webinar/confirmation/${buff.toString(
      "base64"
    )}`,
    postViewUrl: `${APP_DOMAIN}/webinar/thankyou/${buff.toString("base64")}`,

    liveRoomScript: "",
    registrationFormScript: "",
    registrationPageScript: "",
    replayRoomScript: "",
    thankyouPageScript: "",
  };
}

export const doCreateWebinar = async (
  team: Team,
  data: APIWebinarCreateData
) => {
  const webinar = await Webinar.create({
    ...data,
    team,
  }).save();

  webinar.settings = getDefaultSettings(webinar);
  await webinar.save();

  return webinar;
};

export const doActivateWebinar = async (team: Team, id: number) => {
  const webinar = await getWebinar(team, id);

  if (webinar.status !== WebinarStatus.DRAFT) {
    throw new RouterError("Invalid webinar status");
  }

  return await webinar.update({
    status: WebinarStatus.ACTIVE,
  });
};

export const doArchiveWebinar = async (team: Team, id: number) => {
  const webinar = await getWebinar(team, id);

  if (webinar.status !== WebinarStatus.ACTIVE) {
    throw new RouterError("Invalid webinar status");
  }

  const events = await Event.createQueryBuilder("event")
    .leftJoin("event.attendees", "attendees")
    .addSelect("COUNT(attendees)", "attendeeCount")
    .groupBy("event.id")
    .where({
      start: MoreThan(moment().toDate()),
      webinarId: webinar.id,
    })
    .getRawMany();

  const ids = [];
  for (const event of events) {
    if (+event.attendeeCount > 0) {
      throw new RouterError("Can't schedule", 400);
    }
    ids.push(event.event_id);
  }

  await Event.createQueryBuilder()
    .update()
    .set({
      canceled: true,
    })
    .where({
      id: In(ids),
    })
    .execute();

  return await webinar.update({
    status: WebinarStatus.ARCHIVE,
  });
};

export const doUpdateWebinar = async (
  team: Team,
  webinarId: number,
  data: APIWebinarUpdateData
) => {
  const webinar = await getWebinar(team, webinarId);
  return await webinar.update(data);
};

export const doDeleteWebinar = async (team: Team, id: number) => {
  const webinar = await getWebinar(team, id, {
    relations: ["emailGateway", "smsGateway"],
  });

  if (webinar.status !== WebinarStatus.DRAFT) {
    throw RouterError.basic("Can only delete draft webinars", 400);
  }

  if (webinar.emailGateway) {
    await webinar.emailGateway.remove();
  }

  if (webinar.smsGateway) {
    await webinar.smsGateway.remove();
  }

  return await webinar.remove();
};
