import { getPreEventNotifications } from "./notifications";
import MockDate from "mockdate";
import * as moment from "moment";

const notificationBase = {
  content: "",
  subject: "",
  hoursOffset: 0,
  createdDate: expect.any(Date),
  updatedDate: expect.any(Date),
  deliveryMethod: "email",
};

const eventBase = {
  createdDate: expect.any(Date),
  updatedDate: expect.any(Date),
  webinarId: expect.any(Number),
  videoUrl: expect.any(String),
  started: false,
  recurrenceId: null,
};

const event8 = {
  id: 8,
  start: new Date("2019-09-10T17:30:00.000Z"),
  end: new Date("2019-09-10T18:00:00.000Z"),
  canceled: false,
  ...eventBase,
};
const event9 = {
  id: 9,
  start: new Date("2019-09-17T17:30:00.000Z"),
  end: new Date("2019-09-17T18:00:00.000Z"),
  canceled: false,
  ...eventBase,
};

const notification1 = {
  ...notificationBase,
  id: 1,
  type: "pre_event",
  hoursOffset: 2,
};

const notification2 = {
  ...notificationBase,
  id: 2,
  type: "pre_event",
  hoursOffset: 1,
};

const notification3 = {
  ...notificationBase,
  id: 3,
  type: "last_minute",
  deliveryMethod: "sms",
};

const notification4 = {
  ...notificationBase,
  id: 4,
  type: "post_event",
  hoursOffset: 1,
};

const notification5 = {
  ...notificationBase,
  id: 5,
  type: "post_event",
  hoursOffset: 2,
};
export const a = {
  notification1,
  notification2,
  notification3,
  notification4,
  notification5,
};

describe("Notification Controller", () => {
  const EVENT_TIME = "2019-09-17T17:30:00.000Z";

  test("Pre Event (an hour before)", async () => {
    MockDate.set(moment(EVENT_TIME).subtract(1.5, "hours").toDate());
    const notifications = await getPreEventNotifications();
    expect(notifications).toEqual([
      {
        ...notification2,
        webinar: expect.any(Object),
        events: [event8],
      },
      {
        ...notification1,
        webinar: expect.any(Object),
        events: [event9],
      },
    ]);
  });

  test("Pre Event (at event start)", async () => {
    MockDate.set(moment(EVENT_TIME).toDate());

    const notifications = await getPreEventNotifications();
    expect(notifications).toEqual([
      {
        ...notification2,
        webinar: expect.any(Object),
        events: [event8, event9],
      },
      {
        ...notification1,
        webinar: expect.any(Object),
        events: [event9],
      },
    ]);
  });
});
