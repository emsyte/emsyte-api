import { APIWebinarOfferCreateData } from "@emsyte/common/webinar";
import { Offer } from "../../db/entities/Offer";
import { Webinar } from "../../db/entities/Webinar";

// Offers

export const doCreateWebinarOffer = async (
  webinarId: number,
  offer: APIWebinarOfferCreateData
) => {
  return await Offer.create({
    ...offer,
    webinar: { id: webinarId },
  }).save();
};

export const doUpdateWebinarOffer = async (
  id: number,
  offer: APIWebinarOfferCreateData
) => {
  const existing = await Offer.findOne(id);

  await existing.update({
    ...offer,
  });
  return existing;
};

export const doGetAllWebinarOffers = async (id: number) => {
  const webinar = await Webinar.findOne(id);
  let offers = await Offer.find({ webinar });
  return offers;
};

export const doGetWebinarOffer = async (webinarId: number, offerId: number) => {
  const offer = await Offer.findOne({ where: { id: offerId, webinarId } });
  return offer;
};
