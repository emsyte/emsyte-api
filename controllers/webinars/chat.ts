import {
  APIWebinarChatCreateData,
  APIWebinarChatUpdateData,
} from "@emsyte/common/webinar";
import { ChatMessage } from "../../db/entities/ChatMessage";
import { Team } from "../../db/entities/Team";
import RouterError from "../../utils/RouterError";
import { getWebinar } from "./base";

async function getChatMessage(team: Team, webinarId: number, chatId: number) {
  const chatMessage = await ChatMessage.findOne(
    { id: chatId, webinar: { id: webinarId } },
    { relations: ["webinar"] }
  );

  if (!(chatMessage && chatMessage.webinar.teamId === team.id)) {
    throw new RouterError(
      "Chat message not found",
      404,
      "Chat message not found"
    );
  }

  return chatMessage;
}

export const doCreateWebinarChatMessage = async (
  team: Team,
  webinarId: number,
  chatMessage: APIWebinarChatCreateData
) => {
  const webinar = await getWebinar(team, webinarId);

  return await ChatMessage.create({
    ...chatMessage,
    webinar,
  }).save();
};

export const doUpdateWebinarChatMessage = async (
  team: Team,
  webinarId: number,
  id: number,
  data: APIWebinarChatUpdateData
) => {
  const chatMessage = await getChatMessage(team, webinarId, id);
  return await chatMessage.update(data);
};

export const doUploadWebinarChatMessage = async (
  team: Team,
  webinarId: number,
  records: APIWebinarChatCreateData[]
) => {
  const webinar = await getWebinar(team, webinarId);
  await ChatMessage.delete({
    webinar,
  });

  return await ChatMessage.insert(
    records.map((record) => ({ ...record, webinar }))
  );
};

export const doGetAllWebinarChatMessages = async (
  team: Team,
  webinarId: number
) => {
  const webinar = await getWebinar(team, webinarId);

  return await ChatMessage.find({
    where: { webinar },
    order: {
      offset_timestamp: "ASC",
    },
  });
};

export const doGetChatMessage = async (
  team: Team,
  webinarId: number,
  chatId: number
) => {
  return await getChatMessage(team, webinarId, chatId);
};

export const doDeleteChatMessage = async (
  team: Team,
  webinarId: number,
  chatId: number
) => {
  const chatMessage = await getChatMessage(team, webinarId, chatId);
  return await chatMessage.remove();
};
