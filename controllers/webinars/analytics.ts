import * as moment from "moment";
import { Between, In } from "typeorm";
import { Attendee } from "../../db/entities/Attendee";
import { OfferEvent, OfferEventType } from "../../db/entities/OfferEvent";
import { PageView } from "../../db/entities/PageView";
import { PaidRegistration } from "../../db/entities/PaidRegistration";
import { EMOTIONS, Reaction, ReactionStatus } from "../../db/entities/Reaction";
import { Team } from "../../db/entities/Team";
import { ViewerEvent } from "../../db/entities/ViewerEvent";
import { getWebinar } from "./base";

// 1 frame per second
const SAMPLE_RATE = 1;

async function getWebinarReactions(attendees: Attendee[]) {
  if (attendees.length === 0) {
    return [];
  }

  return await Reaction.createQueryBuilder("reaction")
    .where({
      ...Reaction.en("status", ReactionStatus.READY),
      attendee: In(attendees.map((a) => a.id)),
    })
    .getMany();
}

async function getWebinarAttendees(webinarId: number, range: Range) {
  return await Attendee.createQueryBuilder("attendee")
    .innerJoin("attendee.event", "event")
    .where("event.start BETWEEN :from AND :to", {
      from: range.from.toDate(),
      to: range.to.toDate(),
    })
    .andWhere('event."webinarId" = :webinarId', { webinarId })
    .getMany();
}

function simplify(data: number[], size: number = 60): number[] {
  const result: number[] = [];

  for (let i = 0; i < data.length / size; i++) {
    const chunk = data.slice(i * size, i * size + size);
    const total = chunk.reduce((sum, part) => sum + part, 0);
    result.push(total / size);
  }

  return result;
}

function simplifyKeys(data: Record<string, number[]>, size: number = 60) {
  return Object.entries(data).reduce<Record<string, number[]>>(
    (result, [key, values]) => {
      result[key] = simplify(values, size);
      return result;
    },
    {}
  );
}

async function getTimeSeriesData(reactions: Reaction[], duration: number) {
  const seriesLength = Math.floor(duration * SAMPLE_RATE);

  const timeSeriesData = {
    attention: new Array(seriesLength).fill(0),
    neutral: new Array(seriesLength).fill(0),
    happiness: new Array(seriesLength).fill(0),
    surprise: new Array(seriesLength).fill(0),
    sadness: new Array(seriesLength).fill(0),
    anger: new Array(seriesLength).fill(0),
  };

  const average = {
    attention: 0,
    neutral: 0,
    happiness: 0,
    surprise: 0,
    sadness: 0,
    anger: 0,
  };

  const weight = 1 / reactions.length;

  for (const reaction of reactions) {
    const len = Math.min(reaction.report.attention.length, seriesLength);

    for (let i = 0; i < len; i++) {
      timeSeriesData.attention[i] =
        (timeSeriesData.attention[i] ?? 0) +
        reaction.report.attention[i] * weight;

      average.attention +=
        reaction.report.attention[i] *
        weight *
        (1 / reaction.report.attention.length);

      for (const emotion of EMOTIONS) {
        timeSeriesData[emotion][i] =
          (timeSeriesData[emotion][i] ?? 0) +
          reaction.report.emotion[emotion][i] * weight;

        average[emotion] +=
          reaction.report.emotion[emotion][i] *
          weight *
          (1 / reaction.report.emotion[emotion].length);
      }
    }
  }

  return {
    series: simplifyKeys(timeSeriesData, timeSeriesData.attention.length / 50),
    average,
  };
}

async function getOfferClicks(attendees: Attendee[]) {
  if (attendees.length === 0) {
    return [];
  }
  return await OfferEvent.createQueryBuilder("offer")
    .where({
      attendee: In(attendees.map((a) => a.id)),
      ...OfferEvent.en("type", OfferEventType.CLICK),
    })
    .getMany();
}

export async function doGetWebinarAnalytics(
  team: Team,
  webinarId: number,
  range: Range
) {
  range.to.add(1, "day");

  const webinar = await getWebinar(team, webinarId);
  const attendees = await getWebinarAttendees(webinarId, range);
  const reactions = await getWebinarReactions(attendees);
  const offerClicks = await getOfferClicks(attendees);
  const registrationVisits = await PageView.createQueryBuilder()
    .select("key")
    .distinctOn(["key"])
    .addSelect("COUNT(key)", "count")
    .where('"createdDate" BETWEEN :from AND :to', {
      from: range.from.toDate(),
      to: range.to.toDate(),
    })
    .andWhere('"webinarId" = :webinarId', {
      webinarId,
    })
    .groupBy("key")
    .getRawMany();

  const { series, average } = reactions.length
    ? await getTimeSeriesData(reactions, webinar.duration)
    : { series: null, average: null };

  const attendance = await getAttendance(attendees, webinar.duration);

  const attended = await ViewerEvent.createQueryBuilder("viewer_event")
    .select('COUNT(DISTINCT viewer_event."attendeeId")')
    .where({
      attendee: In(attendees.map((a) => a.id)),
    })
    .getRawOne();

  return {
    totals: {
      registered: attendees.length,
      attended: attended.count,
      totalRegistrationVisits: registrationVisits.reduce(
        (total, { count }) => total + parseInt(count),
        0
      ),
      uniqueRegistrationVisits: registrationVisits.length,
      offerClicks: offerClicks.length,
      reactionCount: reactions.length,
    },
    series,
    average,
    attendance,
  };
}

async function getAttendance(attendees: Attendee[], duration: number) {
  const BUCKET_SIZE = 2 * 60; // Every 2 minutes

  // Select attendees per time-frame bucket
  const buckets = await ViewerEvent.createQueryBuilder("viewer_event")
    .where({
      attendee: In(attendees.map((a) => a.id)),
    })
    .select("COUNT(*) AS attendees, time_offset / :window AS bucket")
    .setParameter("window", BUCKET_SIZE)
    .groupBy("bucket")
    .orderBy("bucket")
    .getRawMany<{
      attendees: number;
      bucket: number;
    }>();

  const seriesLength = Math.floor(duration / BUCKET_SIZE);

  const data = new Array(seriesLength).fill(0);

  for (const bucket of buckets) {
    if (bucket.bucket <= seriesLength) {
      data[bucket.bucket] = +bucket.attendees;
    }
  }
  return data;
}

export async function doGetEventAnalytics(eventId: number) {
  const registered = await Attendee.find({
    event: { id: eventId },
  });

  const attended: { attendeeId: number; watch_time: number }[] =
    registered.length === 0
      ? []
      : await ViewerEvent.createQueryBuilder("viewer_event")
          .select(
            'viewer_event."attendeeId", MAX(viewer_event.time_offset) as watch_time'
          )
          .where({
            attendee: In(registered.map((attendee) => attendee.id)),
          })
          .groupBy('viewer_event."attendeeId"')
          .getRawMany();

  const reactions = await getWebinarReactions(registered);

  const averageWatchTime = attended.length
    ? attended.reduce((total, current) => total + current.watch_time, 0) /
      attended.length
    : 0;

  return {
    attendance: {
      registered: registered.length,
      attendees: attended.length,
      avg_watch_time: averageWatchTime,
    },
    attendees: registered.map((attendee) => ({
      id: attendee.id,
      name: `${attendee.firstName} ${attendee.lastName}`,
      attended: attendee.attended,
    })),
    reactionCount: reactions.length,
  };
}

interface TimeSeriesDataFrame {
  day: Date;
  total: number;
}

interface Range {
  from: moment.Moment;
  to: moment.Moment;
}

export function processDashboardStat(
  data: TimeSeriesDataFrame[],
  range: Range
): [number, number[]] {
  const days = range.to.diff(range.from, "days");

  const series = Array(days).fill(0);

  let total = 0;

  for (const item of data) {
    const index = moment(item.day).diff(range.from, "days");

    if (index >= 0 && index < days) {
      total += +item.total;

      series[index] += +item.total;
    }
  }
  return [total, series];
}

async function getRevenue(team: Team, range: Range) {
  const registrationRevenueByDay = await PaidRegistration.createQueryBuilder(
    "pr"
  )
    .where({
      createdDate: Between(range.from.toDate(), range.to.toDate()),
    })
    .select(
      `date_trunc('day', pr."createdDate") as day, SUM(pr."registrationFee") as total`
    )
    .innerJoin("pr.webinar", "webinar")
    .andWhere('webinar."teamId" = :teamId', { teamId: team.id })
    .groupBy("day")
    .orderBy("day", "ASC")
    .getRawMany();

  const offerRevenueByDay = await OfferEvent.createQueryBuilder("offer_event")
    .where({
      createdDate: Between(range.from.toDate(), range.to.toDate()),
    })
    .select(
      `date_trunc('day', offer_event."createdDate") as day, SUM(offer_event."price") as total`
    )
    .innerJoin("offer_event.offer", "offer")
    .innerJoin("offer.webinar", "webinar")
    .andWhere('webinar."teamId" = :teamId', { teamId: team.id })
    .andWhere(
      "offer_event.type = :type",
      OfferEvent.en("type", OfferEventType.PURCHASE)
    )
    .groupBy("day")
    .orderBy("day", "ASC")
    .getRawMany();

  return processDashboardStat(
    [...offerRevenueByDay, ...registrationRevenueByDay],
    range
  );
}

export async function doGetDashboardAnalytics(team: Team, range: Range) {
  range.to.add(1, "day");

  const registrationVisitsPerDayRaw = await PageView.createQueryBuilder(
    "page_view"
  )
    .innerJoin("page_view.webinar", "webinar")
    .andWhere('webinar."teamId" = :teamId', { teamId: team.id })
    .where({
      createdDate: Between(range.from.toDate(), range.to.toDate()),
    })
    .select(
      `date_trunc('day', page_view."createdDate") as day, COUNT(page_view.id) as total`
    )
    .addGroupBy("day")
    .orderBy("day", "ASC")
    .getRawMany();

  const registrantsByDayRaw = await Attendee.createQueryBuilder("attendee")
    .where({
      createdDate: Between(range.from.toDate(), range.to.toDate()),
    })
    .select(
      `date_trunc('day', attendee."createdDate") as day, COUNT(attendee.id) as total`
    )
    .innerJoin("attendee.event", "event")
    .innerJoin("event.webinar", "webinar")
    .andWhere('webinar."teamId" = :teamId', { teamId: team.id })
    .groupBy("day")
    .orderBy("day", "ASC")
    .getRawMany();

  const attendeesPerDayRaw = await ViewerEvent.createQueryBuilder(
    "viewer_event"
  )
    .innerJoin("viewer_event.attendee", "attendee")
    .innerJoin("attendee.event", "event")
    .innerJoin("event.webinar", "webinar")
    .where({
      createdDate: Between(range.from.toDate(), range.to.toDate()),
    })
    .andWhere('webinar."teamId" = :teamId', { teamId: team.id })
    .select(
      `date_trunc('day', viewer_event."createdDate") as day, COUNT(DISTINCT viewer_event."attendeeId") as total`
    )
    .groupBy("day")
    .orderBy("day", "ASC")
    .getRawMany();

  const offerClicksByDayRaw = await OfferEvent.createQueryBuilder("offer_event")
    .where({
      createdDate: Between(range.from.toDate(), range.to.toDate()),
    })
    .select(
      `date_trunc('day', offer_event."createdDate") as day, COUNT(offer_event.id) as total`
    )
    .innerJoin("offer_event.offer", "offer")
    .innerJoin("offer.webinar", "webinar")
    .andWhere('webinar."teamId" = :teamId', { teamId: team.id })
    .andWhere(
      "offer_event.type = :type",
      OfferEvent.en("type", OfferEventType.CLICK)
    )
    .groupBy("day")
    .orderBy("day", "ASC")
    .getRawMany();

  const [totalRevenue, revenueByDay] = await getRevenue(team, range);
  const [
    totalRegistrationVisits,
    registrationVisitsByDay,
  ] = processDashboardStat(registrationVisitsPerDayRaw, range);
  const [totalAttendees, attendeesByDay] = processDashboardStat(
    attendeesPerDayRaw,
    range
  );
  const [totalRegistrants, registrantsByDay] = processDashboardStat(
    registrantsByDayRaw,
    range
  );
  const [totalOfferClicks, offerClicksByDay] = processDashboardStat(
    offerClicksByDayRaw,
    range
  );

  return {
    total: {
      revenue: totalRevenue,
      registered: totalRegistrants,
      offerClicks: totalOfferClicks,
      registrationVisits: totalRegistrationVisits,
      attendees: totalAttendees,
    },
    series: {
      revenue: revenueByDay,
      registered: registrantsByDay,
      offerClicks: offerClicksByDay,
      registrationVisits: registrationVisitsByDay,
      attendees: attendeesByDay,
    },
  };
}
