import { APIWebinarAttendeeCreateData } from "@emsyte/common/webinar";
import { In } from "typeorm";
import { Attendee } from "../../db/entities/Attendee";
import { Event } from "../../db/entities/Event";
import { PaidRegistration } from "../../db/entities/PaidRegistration";
import { Webinar } from "../../db/entities/Webinar";
import { createAttendeeReplayToken, createAttendeeToken } from "../../lib/auth";
import RouterError from "../../utils/RouterError";
import { doSendAttendeeWelcomeEmail } from "../notifications";
import { URL } from "url";

const APP_DOMAIN =
  process.env.NODE_ENV === "test" ? "localhost:3000" : process.env.APP_DOMAIN;

function getPostRegistrationUrl(webinar: Webinar, attendee: Attendee) {
  const url = new URL(webinar.settings.postRegistrationUrl);
  url.searchParams.append("id", getViewId(attendee));
  url.searchParams.append("atoken", createAttendeeToken(attendee));
  return url.toString();
}

export function getPostUrl(webinar: Webinar, attendee: Attendee) {
  const url = new URL(webinar.settings.postViewUrl);
  url.searchParams.append("id", getViewId(attendee));
  url.searchParams.append("atoken", createAttendeeReplayToken(attendee));
  return url.toString();
}

// Get all Attendees for Event
export const doGetAllAttendeesForEvent = async (userId: number) => {
  try {
    // TODO: This query can be optimized
    const webinars = await Webinar.find({ where: { userId } });
    const webinarIds = webinars.map((x) => x.id);

    const events = await Event.find({ where: { webinarId: In(webinarIds) } });
    return { data: events };
  } catch (error) {
    console.log(error);
    throw new RouterError("Couldn't grab webinar", 400, "Wrong Id.");
  }
};

// Register Attendee
export const doCreateAttendeeForEvent = async (
  webinarId: number,
  eventId: number,
  data: APIWebinarAttendeeCreateData
) => {
  const event = await Event.findOne({
    where: {
      id: eventId,
      webinar: { id: webinarId },
    },
    relations: [
      "webinar",
      "webinar.emailGateway",
      "webinar.emailGateway.integration",
      "webinar.team",
    ],
  });

  if (!event) {
    throw new RouterError("Event not found", 404, "Event not found");
  }

  const exists = await Attendee.findOne({
    where: { email: data.email, event },
  });

  if (exists) {
    throw new RouterError(
      "Attendee Exists",
      422,
      "There is already an attendee for this e-mail."
    );
  }

  const attendee = await Attendee.create({
    event,
    ...data,
    active: !event.webinar.isPaid,
  }).save();

  if (event.webinar.isPaid) {
    return {
      message: "Continue to checkout",
      token: createAttendeeToken(attendee),
      redirectTo: event.webinar.checkoutLink,
    };
  }

  await doSendAttendeeWelcomeEmail(
    attendee,
    attendee.event.webinar.emailGateway
  );

  return {
    message: `You've been registered for the event, please use included key to access the event`,
    redirectTo: getPostRegistrationUrl(event.webinar, attendee),
  };
};

export const doActivateAttendee = async (attendeeId: number) => {
  const attendee = await Attendee.findOne(attendeeId, {
    relations: ["event"],
  });
  attendee.event.webinar = await Webinar.findOne(attendee.event.webinarId, {
    relations: ["emailGateway"],
  });

  await attendee.update({ active: true });

  // Track the payment event
  await PaidRegistration.create({
    attendee,
    event: attendee.event,
    webinar: attendee.event.webinar,
    registrationFee: attendee.event.webinar.registrationFee,
  }).save();

  await doSendAttendeeWelcomeEmail(
    attendee,
    attendee.event.webinar.emailGateway
  );

  return {
    message: `You've been registered for the event, please use included key to access the event`,
    redirectTo: getPostRegistrationUrl(attendee.event.webinar, attendee),
  };
};

export function getViewId(attendee: Attendee) {
  const data = { id: attendee.event.webinar.id, eventId: attendee.event.id };
  const buff = new Buffer(JSON.stringify(data));
  return buff.toString("base64");
}

export function getViewLink(attendee: Attendee) {
  const token = createAttendeeToken(attendee);
  const id = getViewId(attendee);

  return getViewLinkFromToken(id, token);
}

export function getReplayLink(attendee: Attendee) {
  const token = createAttendeeReplayToken(attendee);
  const id = getViewId(attendee);

  return getReplayLinkFromToken(id, token);
}

export function getViewLinkFromToken(id: string, token: string) {
  return `${APP_DOMAIN}/webinar/watch/${id}?token=${token}`;
}

// TODO: Make sure this matches the replay page
export function getReplayLinkFromToken(id: string, token: string) {
  return `${APP_DOMAIN}/webinar/replay/${id}?token=${token}`;
}
