import moment = require("moment");
import { processDashboardStat } from "./analytics";

describe("Analytics", () => {
  test("processDashboardStat()", () => {
    const [total, series] = processDashboardStat(
      [
        { day: moment("2020-12-15").toDate(), total: 10 },
        { day: moment("2020-12-16").toDate(), total: 5 },
        { day: moment("2020-12-15").toDate(), total: 3 },
        { day: moment("2020-12-20").toDate(), total: 7 },
        { day: moment("2020-12-12").toDate(), total: 3 },
      ],
      {
        from: moment("2020-12-13"),
        to: moment("2020-12-20"),
      }
    );

    expect(total).toEqual(18);
    expect(series).toEqual([0, 0, 13, 5, 0, 0, 0]);
  });
});
