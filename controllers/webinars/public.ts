import { WebinarStatus } from "@emsyte/common/webinar";
import { MoreThan } from "typeorm";
import { Event } from "../../db/entities/Event";
import { Webinar } from "../../db/entities/Webinar";
import RouterError from "../../utils/RouterError";

export const doGetPublicWebinar = async (id: number) => {
  const webinar = await Webinar.findOne({
    where: { id, ...Webinar.en("status", WebinarStatus.ACTIVE) },
  });

  if (!webinar) {
    throw new RouterError("Webinar not found", 404, "Webinar not found");
  }

  return webinar;
};

export const doGetAllPublicWebinarEvents = async (webinarId: number) => {
  const webinar = await doGetPublicWebinar(webinarId);
  const events = await Event.find({
    where: {
      webinar,
      canceled: false,
      start: MoreThan(new Date()),
    },
    order: {
      start: "ASC",
    },
  });

  return events;
};

export const doGetPublicWebinarEvent = async (
  webinarId: number,
  eventId: number
) => {
  const webinar = await doGetPublicWebinar(webinarId);
  const event = await Event.findOne({
    where: { webinar, id: eventId },
  });

  if (!event) {
    throw new RouterError(
      `Event ${eventId} isn't available for webinar ${webinarId}`,
      404,
      "Event doesn't exist"
    );
  }

  return event;
};
