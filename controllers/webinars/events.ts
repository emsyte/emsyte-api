import {
  APIWebinarAddEventVideoUrl,
  APIWebinarAddRecurrenceData,
  WebinarType,
} from "@emsyte/common/webinar";
import * as moment from "moment";
import { Between, In } from "typeorm";
import { Attendee } from "../../db/entities/Attendee";
import { Event } from "../../db/entities/Event";
import { Team } from "../../db/entities/Team";
import { Recurrence } from "../../db/entities/Webinar";
import RouterError from "../../utils/RouterError";
import { getWebinar } from "./base";

interface Range {
  from: moment.Moment;
  to: moment.Moment;
}

function _validateDates(start: moment.Moment, end: moment.Moment) {
  if (!start || !end) {
    throw RouterError.basic("Start or end date not set", 400);
  }

  if (end.add({ day: 1 }).isBefore(moment())) {
    throw RouterError.basic("End date can't be in the past", 422);
  }

  if (start.isAfter(end)) {
    throw RouterError.basic("Start date can't be after the end date", 422);
  }
}

export const doCreateRecurrence = async (
  team: Team,
  webinarId: number,
  data: APIWebinarAddRecurrenceData
) => {
  _validateDates(
    moment.tz(data.start, data.timezone),
    moment.tz(data.end, data.timezone)
  );

  const webinar = await getWebinar(team, webinarId);
  const recurrence = await Recurrence.create({
    ...data,
    start: new Date(data.start),
    end: moment.tz(data.end, data.timezone).toDate(),
    webinar,
  }).save();

  const videoUrl =
    webinar.type === WebinarType.RECORDED ? webinar.videoUrl : "";

  const events = recurrence
    .getRRule()
    .all()
    .map((date) =>
      Event.create({
        start: date,
        end: moment(date).add(webinar.duration, "seconds").toDate(),
        recurrenceId: recurrence.id,
        webinarId: webinar.id,
        videoUrl,
      })
    );

  await Event.createQueryBuilder().insert().values(events).execute();

  return recurrence;
};

export const doDeleteRecurrence = async (
  team: Team,
  webinarId: number,
  recurrenceId: number
) => {
  const webinar = await getWebinar(team, webinarId);
  const recurrence = await Recurrence.findOne({
    webinarId: webinar.id,
    id: recurrenceId,
  });

  const { attendee_count } = await Attendee.createQueryBuilder("attendee")
    .select("COUNT(attendee.*)", "attendee_count")
    .leftJoin("attendee.event", "event")
    .leftJoin("event.recurrence", "recurrence")
    .where("recurrence.id = :recurrenceId", { recurrenceId: recurrence.id })
    .getRawOne();

  if (+attendee_count > 0) {
    throw RouterError.basic(
      "Can't remove recurrence if any attendees have registered for an event on this recurrence",
      409
    );
  }

  await recurrence.remove();

  await Event.delete({
    recurrenceId,
  });

  return recurrence;
};

export const doGetRecurrences = async (team: Team, webinarId: number) => {
  const webinar = await getWebinar(team, webinarId, {
    relations: ["recurrences"],
  });

  return webinar.recurrences;
};

export const doGetAllEvents = async (
  team: Team,
  pagination?: Range | number,
  order?: "ASC" | "DESC"
) => {
  let query = Event.createQueryBuilder("event")
    .innerJoinAndSelect("event.webinar", "webinar")
    .leftJoin("event.attendees", "attendees")
    .leftJoin(
      "attendees.viewer_events",
      "viewer_events",
      "event.start + (viewer_events.time_offset * interval '1 second') + interval '5 minutes' > :now",
      {
        now: new Date(),
      }
    )
    .addSelect("COUNT(viewer_events)", "viewer_events")
    .addSelect("COUNT(distinct attendees.id)", "registered")
    .groupBy("event.id, webinar.id")
    .orderBy("event.start", order ?? "DESC");

  if (typeof pagination === "number") {
    query = query.limit(pagination);
  } else if (pagination) {
    query = query.where({
      start: Between(pagination.from.toDate(), pagination.to.toDate()),
    });
  }

  query.andWhere('webinar."teamId" = :teamId', { teamId: team.id });

  const results = await query.getRawAndEntities();
  const data = results.entities
    .map((entity, i) =>
      Object.assign(entity, {
        registered: parseInt(results.raw[i].registered),
        attending: parseInt(results.raw[i].viewer_events),
      })
    )
    .reverse();
  console.log(data);
  return data;
};

export const doGetSchedule = async (
  team: Team,
  pagination?: Range | number,
  order?: "ASC" | "DESC"
) => {
  console.log("here");
  let query = Event.createQueryBuilder("event")
    .innerJoinAndSelect("event.webinar", "webinar")
    .leftJoin("event.attendees", "attendees")
    .leftJoin("attendees.viewer_events", "viewer_events")
    .addSelect("COUNT(attendees)", "registered")
    .addSelect("COUNT(viewer_event)", "viewer_events")
    .groupBy("attendees.id, event.id, webinar.id")
    .orderBy("event.start", order ?? "DESC")
    .where('webinar."teamId" = :teamId', { teamId: team.id });

  if (typeof pagination === "number") {
    query = query.limit(pagination);
  } else if (pagination) {
    query = query.where({
      start: Between(pagination.from.toDate(), pagination.to.toDate()),
    });
  }

  const results = await query.getRawAndEntities();

  const recurrences = await Recurrence.find({
    id: In(results.entities.map((event) => event.recurrenceId)),
  });

  return {
    events: results.entities
      .map((entity, i) =>
        Object.assign(entity, {
          registered: parseInt(results.raw[i].registered),
        })
      )
      .reverse(),
    recurrences,
  };
};

export const doGetAllWebinarEvents = async (webinarId: number) => {
  try {
    const events = await Event.find({
      where: { webinar: { id: webinarId } },
      order: {
        start: "ASC",
      },
    });
    return events;
  } catch (error) {
    console.log(error);
    throw new RouterError("Couldn't grab webinar", 400, "Wrong Id.");
  }
};

export const doGetWebinarEvent = async (
  team: Team,
  webinarId: number,
  eventId: number
) => {
  const webinar = await getWebinar(team, webinarId);

  try {
    const event = await Event.findOne({
      where: { id: eventId, webinarId: webinar.id },
    });

    event.webinar = webinar;

    return event;
  } catch (error) {
    console.log(error);
    throw new RouterError("Couldn't grab webinar", 400, "Wrong Id.");
  }
};

export const doSetEventUrl = async (
  team: Team,
  webinarId: number,
  eventId: number,
  data: APIWebinarAddEventVideoUrl
) => {
  const webinar = await getWebinar(team, webinarId);

  if (webinar.type !== WebinarType.LIVE) {
    throw new RouterError(
      "Can't change event video URL on recorded webinar",
      409
    );
  }

  const event = await Event.findOne({
    where: { id: eventId, webinarId: webinar.id },
  });

  const result = await event.update({
    videoUrl: data.videoUrl,
  });
  result.webinar = webinar;

  return result;
};
