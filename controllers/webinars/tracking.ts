import { Attendee } from "../../db/entities/Attendee";
import { Offer } from "../../db/entities/Offer";
import * as jwt from "jsonwebtoken";
import { IsNumber } from "class-validator";
import { runValidation } from "../../utils/validator";
import { OfferEvent, OfferEventType } from "../../db/entities/OfferEvent";

interface TrackingData {
  attendeeId: number;
  offerId: number;
}

class ValidateTrackingData implements TrackingData {
  @IsNumber()
  attendeeId: number;

  @IsNumber()
  offerId: number;
}

const APP_SECRET = process.env.APP_SECRET || "not_a_secret";

export function createTrackingCookie(attendee: Attendee, offer: Offer) {
  const data: TrackingData = {
    attendeeId: attendee.id,
    offerId: offer.id,
  };

  return jwt.sign(data, APP_SECRET);
}

async function parseTrackingCookie(cookie: string) {
  try {
    const data = jwt.verify(cookie, APP_SECRET);
    return await runValidation(ValidateTrackingData, data);
  } catch {
    return null;
  }
}

export async function trackOfferPurchase(cookie: string) {
  const data = await parseTrackingCookie(cookie);

  if (!data) {
    return;
  }

  const offer = await Offer.findOne(data.offerId);

  return await OfferEvent.create({
    type: OfferEventType.PURCHASE,
    attendee: { id: data.attendeeId },
    offer,
    price: offer.price,
  }).save();
}
