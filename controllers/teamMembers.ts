import { Team, TeamToUser } from "../db/entities/Team";
import RouterError from "../utils/RouterError";
import {
  APIInviteTeamMemberData,
  TeamUserStatus,
  APIEditTeamMemberData,
} from "@emsyte/common/team";
import { User } from "../db/entities/User";
import { UserStatus } from "@emsyte/common/user";
import { doSendTeamInvite } from "./email";
import { randomBytes } from "crypto";

async function getUser(email: string) {
  const user = await User.findOne({ email });
  if (user) {
    return { user, password: null };
  }

  const newUser = User.create({
    email,
    status: UserStatus.ACTIVE,
    firstName: "",
    lastName: "",
  });
  const password = randomBytes(8).toString("hex");
  await newUser.setPassword(password);
  await newUser.save();

  return { user: newUser, password };
}

export async function doGetTeamMembers(team: Team) {
  return await TeamToUser.find({
    where: { team },
    relations: ["user"],
  });
}

export async function doInviteTeamMember(
  team: Team,
  userId: number,
  data: APIInviteTeamMemberData
) {
  const inviter = await User.findOne(userId);

  const { user, password } = await getUser(data.email);

  const teamToUser = await TeamToUser.create({
    team,
    user,
    role: data.role,
    // TODO: This should be INVITED once we allow users to accept invites
    status: TeamUserStatus.ACTIVATED,
  }).save();

  await doSendTeamInvite({
    temporaryPassword: password ?? "",
    user,
    teamName: team.name,
    inviter,
    newAccount: password !== null,
  });

  return teamToUser;
}

export async function doRemoveTeamMember(team: Team, userId: number) {
  try {
    const teamToUser = await TeamToUser.findOne({
      where: { user: { id: userId }, team },
      relations: ["user"],
    });
    return await teamToUser.remove();
  } catch {
    throw new RouterError(
      "Failed to remove team member",
      500,
      "Failed to remove team member"
    );
  }
}

export async function doEditTeamMember(
  team: Team,
  userId: number,
  data: APIEditTeamMemberData
) {
  try {
    const teamToUser = await TeamToUser.findOne({
      where: { user: { id: userId }, team },
      relations: ["user"],
    });
    return await teamToUser.update(data);
  } catch {
    throw new RouterError(
      "Failed to remove team member",
      500,
      "Failed to remove team member"
    );
  }
}
