import { TeamPlan } from "@emsyte/common/team";
import { Team } from "../db/entities/Team";
import { User } from "../db/entities/User";
import {
  EmailName,
  sendEmailLocal,
  sendEmailSendGridTemplate,
  TemplateEmail,
} from "../lib/senders";
import { serializeUser } from "../lib/serializers/userSerializer";
import { unknownValue } from "../utils/conditions";

const {
  EMAIL_SERVICE_KEY,
  EMAIL_SERVICE_ADDRESS,
  EMAIL_SERVICE_TITLE,
  APP_DOMAIN,
} = process.env;

const TEMPLATE_IDS = {
  welcome: "d-3e33fc9fec014fe3a649cc24a3a76631",
  deactivate: "d-692783a0c7384f03a7122416d98918e3",
  invite: "d-64ead0d52cea4e7695d9894f0782642d",
  inviteNewAccount: "d-919a3f1ea3b547b7b0d402705eceeb82",
  passwordReset: "d-31e2956212414877a51a262b1bf5ad4c",
};

async function sendSystemEmail(to: EmailName, templateId: string, data: any) {
  const email: TemplateEmail = {
    to,
    from: {
      name: EMAIL_SERVICE_TITLE,
      email: EMAIL_SERVICE_ADDRESS,
    },
    templateId,
    data,
  };

  if (process.env.NODE_ENV !== "production") {
    return await sendEmailLocal(email);
  }
  await sendEmailSendGridTemplate(email, EMAIL_SERVICE_KEY);
}

async function sendUserEmail(user: User, templateId: string, data: any) {
  return await sendSystemEmail(
    {
      name: `${user.firstName} ${user.lastName}`,
      email: user.email,
    },
    templateId,
    data
  );
}

function getPlanName(plan: TeamPlan) {
  switch (plan) {
    case TeamPlan.FREE:
      return "Free";
    case TeamPlan.BASIC:
      return "Basic";
    case TeamPlan.PREMIUM:
      return "Premium";
    default:
      return unknownValue(plan);
  }
}

// Registering an account on a site
export const doSendWelcomeEmail = async (user: User, team: Team) => {
  const data = {
    ...serializeUser(user),
    teamName: team.name,
    planName: getPlanName(team.plan),
  };

  await sendUserEmail(user, TEMPLATE_IDS.welcome, data);

  console.log("Sent welcome email to " + user.email);

  return { message: `Welcome Email Sent` };
};
// Deactivate Account
export const doSendDeactivateEmail = async (user: User) => {
  const data = serializeUser(user);

  await sendUserEmail(user, TEMPLATE_IDS.deactivate, data);

  return { message: `Deactivation Email Sent` };
};
// Team Member Invite
export async function doSendTeamInvite({
  teamName,
  temporaryPassword,
  newAccount,
  user,
  inviter,
}: {
  teamName: string;
  temporaryPassword: string;
  newAccount: boolean;
  user: User;
  inviter: User;
}) {
  const templateId = newAccount
    ? TEMPLATE_IDS.inviteNewAccount
    : TEMPLATE_IDS.invite;

  const data = {
    user: serializeUser(user),
    inviter: serializeUser(inviter),
    teamName,
    temporaryPassword,
    app_profile_login: `${APP_DOMAIN}/login`,
  };

  await sendSystemEmail(
    {
      name: "",
      email: user.email,
    },
    templateId,
    data
  );

  return { message: `Invite Sent` };
}

export async function doSendForgotPassword(user: User, token: string) {
  const resetLink = `${APP_DOMAIN}/reset-password?token=${token}`;
  const data = {
    ...serializeUser(user),
    resetLink,
  };
  await sendUserEmail(user, TEMPLATE_IDS.passwordReset, data);
}

// TODO
// // Confirming Purchase
// export const doSendInvoice = async ({
//   email,
//   firstName,
//   lastName,
//   product,
//   description,
//   price,
// }) => {
//   let template = mailer.getTemplate("invoice");

//   if (template instanceof MailgunTemplate) {
//     await mailer
//       .sendFromTemplate(email, template, {
//         firstName,
//         lastName,
//         email,
//         product,
//         description,
//         price,
//       })
//       .catch((error) => {
//         console.error(error);
//         throw new RouterError(`Error: ${error}`, 500);
//       });

//     return { message: `Invoice Sent` };
//   }
// };
