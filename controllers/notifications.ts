import { NotificationType } from "@emsyte/common/notification";
import * as moment from "moment-timezone";
import { Attendee } from "../db/entities/Attendee";
import { Notification } from "../db/entities/Notification";
import { NotificationGateway } from "../db/entities/NotificationsGateway";
import {
  getGatewayIntegration,
  updateIntegrationSettings,
} from "../integrations/actions";
import {
  AutoResponderProvider,
  EmailProvider,
  SMSProvider,
} from "../integrations/base";
import { FieldMapping } from "../integrations/types";
import {
  doRenderEmailSubjectTemplate,
  doRenderNotificationTemplate,
} from "./template";
import { getReplayLink, getViewLink } from "./webinars/attendees";

async function sendWelcomeEmail(
  provider: EmailProvider<any>,
  attendee: Attendee
) {
  const webinar = attendee.event.webinar;

  console.log(`Sending welcome email to ${attendee.email} for ${webinar.id}`);

  const notification = await Notification.findOne({
    webinar,
    ...Notification.en("type", NotificationType.WELCOME),
  });

  const { subject, content } = await renderEmail(notification, attendee);

  return await provider.sendEmail({
    to: {
      email: attendee.email,
      name: attendee.firstName + " " + attendee.lastName,
    },
    subject,
    content,
  });
}
async function addContact(
  provider: AutoResponderProvider<any>,
  attendee: Attendee
) {
  const when = moment(attendee.event.start).tz(attendee.timezone);

  const fieldValues: FieldMapping = {
    webinar_name: attendee.event.webinar.name,
    live_link: getViewLink(attendee),
    replay_link: getReplayLink(attendee),
    date: when.format("dddd, MMMM Do YYYY"),
    time: when.format("h:mm a"),
    timezone: attendee.timezone,
  };

  const settings = provider.getGatewaySettings();

  // Only send field values that are configured
  for (const key of Object.keys(fieldValues)) {
    if (!settings.fields[key]) {
      delete fieldValues[key];
    }
  }

  return await provider.addContact(attendee, fieldValues);
}
function getEmailContent(
  notificationType: NotificationType,
  attendee: Attendee
) {
  if (notificationType === NotificationType.POST_EVENT) {
    return {};
  }
  return {
    viewLink: getViewLink(attendee),
    replayLink: getReplayLink(attendee),
  };
}

async function renderEmail(notification: Notification, attendee: Attendee) {
  const extra = getEmailContent(notification.type, attendee);

  const content = await doRenderNotificationTemplate(
    attendee,
    attendee.event.webinar,
    notification,
    extra
  );

  const subject = await doRenderEmailSubjectTemplate(
    attendee,
    attendee.event.webinar,
    notification
  );
  return { content, subject };
}

async function sendNotification(
  provider: SMSProvider<any> | EmailProvider<any>,
  notification: Notification,
  attendee: Attendee
) {
  if (provider instanceof EmailProvider) {
    const { subject, content } = await renderEmail(notification, attendee);

    console.log(
      `Sending email to ${attendee.email} for webinar ${attendee.event.webinar.id} and notification ${notification.id}`
    );

    return await provider.sendEmail({
      to: {
        email: attendee.email,
        name: attendee.firstName + " " + attendee.lastName,
      },
      subject,
      content,
    });
  }

  if (!attendee.phone) {
    return;
  }

  const content = await doRenderNotificationTemplate(
    attendee,
    attendee.event.webinar,
    notification
  );

  console.log(
    `Sending reminder to ${attendee.phone} for webinar ${attendee.event.webinar.id}`
  );

  return await provider.sendSMS({
    content,
    phone: attendee.phone,
  });
}

export async function doSendAttendeeWelcomeEmail(
  attendee: Attendee,
  gateway: NotificationGateway
) {
  if (!gateway) {
    return;
  }

  const provider = getGatewayIntegration(gateway);

  try {
    if (provider instanceof EmailProvider) {
      return await sendWelcomeEmail(provider, attendee);
    } else if (provider instanceof AutoResponderProvider) {
      return await addContact(provider, attendee);
    }
    // TODO: SMS provider, what to do?
  } finally {
    await updateIntegrationSettings(gateway.integration, provider);
  }
}

export async function doSendManyNotifications(
  notification: Notification,
  notificationGateway: NotificationGateway,
  attendees: Attendee[]
) {
  const provider = getGatewayIntegration(notificationGateway);

  if (provider instanceof AutoResponderProvider) {
    return;
  }

  try {
    return await Promise.all(
      attendees.map(async (attendee) => {
        try {
          await sendNotification(provider, notification, attendee);
        } catch (error) {
          console.log(
            `Failed to send notification ${notification.id} to attendee ${attendee.id}`,
            error
          );
        }
      })
    );
  } finally {
    await updateIntegrationSettings(notificationGateway.integration, provider);
  }
}
