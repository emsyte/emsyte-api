import { TeamUserRole } from "@emsyte/common/team";
import {
  APIAuthRegisterData,
  APIChangePasswordData,
  APIUserForgotPasswordConfirmData,
  APIUserForgotPasswordData,
  APIUserUpdateData,
  UserStatus,
} from "@emsyte/common/user";
import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import { Team, TeamToUser } from "../db/entities/Team";
import { User } from "../db/entities/User";
import {
  createRefreshToken,
  createUserToken,
  verifyRefreshToken,
} from "../lib/auth";
import RouterError from "../utils/RouterError";
import { doSendForgotPassword, doSendWelcomeEmail } from "./email";
import { doDeactivateSubscription } from "./stripe";
import { doRegisterTeam } from "./teams";

const APP_SECRET = process.env.APP_SECRET || "not_a_secret";

async function getDefaultTeam(user: User) {
  const defaultTeam = await Team.createQueryBuilder("team")
    .select("team.slug")
    .innerJoin("team.teamToUsers", "teamToUser")
    .where('"teamToUser"."userId" = :id', { id: user.id })
    .getOne();

  return defaultTeam.slug;
}

function userResponse(user: User) {
  const token = createUserToken(user);
  const refreshToken = createRefreshToken(user);
  return { token, refreshToken };
}

export const register = async (data: APIAuthRegisterData) => {
  const { email, firstName, lastName } = data;
  const userExists = await User.findOne({ email });

  if (userExists) {
    throw new RouterError(
      "userExists",
      422,
      "There is already an user for this e-mail."
    );
  }
  const user = User.create({
    firstName,
    lastName,
    email,
  });

  await user.setPassword(data.password);
  await user.save();

  // TODO: Do this all in one transaction
  try {
    const team = await doRegisterTeam(user.id, {
      name: data.teamName,
      price: data.price,
      source: data.source,
      coupon: data.coupon,
    });
    await doSendWelcomeEmail(user, team);

    return {
      ...userResponse(user),
      defaultTeam: team.slug,
    };
  } catch (error) {
    // Cleanup any data so the user can submit again
    const team = await Team.createQueryBuilder("team")
      .innerJoin("team.teamToUsers", "teamToUser")
      .where('"teamToUser"."userId" = :userId', { userId: user.id })
      .getOne();

    if (team) {
      await team.remove();
    }
    const teamToUser = await TeamToUser.createQueryBuilder("teamToUser")
      .innerJoin("teamToUser.user", "user")
      .where("user.id = :userId", { userId: user.id })
      .getOne();

    if (teamToUser) {
      await teamToUser.remove();
    }

    await user.remove();

    throw error;
  }
};

export const login = async (email: string, password: string) => {
  const user = await User.findOne({ email: email });

  if (!user) {
    throw new RouterError("userNotFound", 404, "Incorrect Email or Password.");
  }
  const passwordMatch = await bcrypt.compare(password, user.password);
  if (!passwordMatch) {
    throw new RouterError(
      "userWrongPassword",
      401,
      "Incorrect Email or Password."
    );
  } else {
    return {
      ...userResponse(user),
      defaultTeam: await getDefaultTeam(user),
    };
  }
};

export const refresh = async (_token, _refreshToken) => {
  try {
    const { id: userId } = verifyRefreshToken(_refreshToken);
    const user = await User.findOne(userId);

    return userResponse(user);
  } catch (err) {
    throw new RouterError(err, 401, "Failure to refresh token, please login.");
  }
};

export const doGetUser = async (id: number) => {
  const user = await User.findOne(id);
  return user;
};

export const doUpdateUser = async (id: number, data: APIUserUpdateData) => {
  const user = await User.findOne(id);
  await user.update(data);
  return userResponse(user);
};

export const doChangePassword = async (
  id: number,
  data: APIChangePasswordData
) => {
  const user = await User.findOne(id);
  const passwordMatch = await bcrypt.compare(data.password, user.password);
  if (!passwordMatch) {
    throw new RouterError(
      "Password does not match",
      409,
      "Password does not match"
    );
  }

  await user.setPassword(data.newPassword);
  await user.save();
  return { message: "Password changed successfully" };
};

export const doForgotPassword = async (data: APIUserForgotPasswordData) => {
  const token = jwt.sign({ ...data }, APP_SECRET, { expiresIn: "1h" });
  const user = await User.findOne({ email: data.email });

  if (user) {
    await doSendForgotPassword(user, token);
  }
  return { message: "Password reset email sent" };
};

export const doForgotPasswordConfirm = async (
  data: APIUserForgotPasswordConfirmData
) => {
  const { email } = jwt.verify(
    data.token,
    APP_SECRET
  ) as APIUserForgotPasswordData;

  const user = await User.findOne({ email });

  await user.setPassword(data.password);
  await user.save();

  return { message: "Password updated successfully" };
};

export const doDeactivateAccount = async (userId: number) => {
  const user = await User.findOne(userId);
  await user.update({
    status: UserStatus.INACTIVE,
  });
  const userTeams = await TeamToUser.createQueryBuilder("teamToUser")
    .innerJoinAndSelect("teamToUser.team", "team")
    .where({
      user,
      ...TeamToUser.en("role", TeamUserRole.OWNER),
    })
    .getMany();

  for (const userTeam of userTeams) {
    await doDeactivateSubscription(userTeam.team);
  }

  console.log(`Deactivating ${user.id}| ${user.firstName} ${user.lastName}`);
  return { message: "Account deactivated successfully" };
};
