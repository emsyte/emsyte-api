import {
  AuthAttendeeObject,
  AuthType,
  AuthUserObject,
} from "@emsyte/common/auth";
import { TeamUserRole } from "@emsyte/common/team";
import {
  ChatEvent,
  MessageEvent,
  MessageEventType,
  JoinedEvent,
  MessageType,
  RemoveEvent,
  LabelType,
  ErrorEvent,
  LeftEvent,
  HistoryEvent,
  AttendeesListEvent,
  BarsChannelEvent,
} from "@emsyte/common/message";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import moment = require("moment");
import * as sanitize from "sanitize-html";
import "source-map-support/register";
import { Connection } from "typeorm";
import initializeDatabase from "../db/connection";
import { Attendee } from "../db/entities/Attendee";
import { Event } from "../db/entities/Event";
import { TeamToUser } from "../db/entities/Team";
import { Webinar } from "../db/entities/Webinar";
import { verifyToken } from "../lib/auth";
import { client as db } from "../utils/db";
import { runValidation } from "../utils/validator";
import WebsocketClient from "../utils/websocket-client";
import {
  UsersChannelBodyValidator,
  RemoveMessageBodyValidator,
  SendMessageBodyValidator,
  SubscribeBodyValidator,
  HistoryChannelBodyValidator,
} from "../validators/websocket";

// ts-ignore
const BARS_PER_WEBINARS = 256;
const wsClient = new WebsocketClient();
const success = { statusCode: 200 };

const HISTORY_BATCH_SIZE = 50;

export const connect = async (event, _context) => {
  await wsClient._setupClient(event);
  return success;
};

function getChannelId(eventId: number) {
  return `event-${eventId}`;
}

export const disconnect = async (event, context) => {
  const subscriptions = await db.fetchConnectionSubscriptions(event);
  const unsubscribes = subscriptions.map(async (subscription) => {
    const channelId = db.parseEntityId(subscription[db.Channel.Primary.Key]);
    return leaveChannel(
      {
        ...event,
        body: JSON.stringify({
          action: "leaveChannel",
          channelId,
        }),
      },
      context
    );
  });
  await Promise.all(unsubscribes);
  return success;
};

export const defaultMessage = async (event, _context) => {
  const errorEvent: ErrorEvent = {
    event: MessageEventType.ERROR,
    content: "invalid action type",
  };

  await wsClient.send(event, errorEvent);

  return success;
};

interface MessageDetails {
  channelId: string;
  content: string;
  sendTo: string;
  question: boolean;
  replyTo: string;
  token?: string;
}

interface UserDetails {
  name: string;
  label: string;
  userId: string;
}

interface MessageInDb {
  UserId: string;
  Name: string;
  Label: string;
  Content: string;
  Question: boolean;
  ReplyTo: string;
  Token: string;
}

interface UserInDb {
  UserId: string;
  Label: LabelType;
  Name: string;
}

export async function sendSingleMessage(
  messageDetails: MessageDetails,
  user: UserDetails
) {
  let roomId = messageDetails.sendTo ?? "general";

  let replyToMessage;
  if (messageDetails.replyTo) {
    const message = await db.Client.get({
      TableName: db.MessagesTable,
      Key: {
        [db.Channel.Messages.Key]: `${messageDetails.channelId}-${roomId}`,
        [db.Channel.Messages.Range]: messageDetails.replyTo,
      },
    }).promise();

    if (message.Item) {
      replyToMessage = {
        messageId: messageDetails.replyTo,
        name: message.Item.Name,
        label: message.Item.Label,
        content: message.Item.Content,
      };
    }
  }

  const pk = `${messageDetails.channelId}-${roomId}`;
  const content = sanitize(messageDetails.content, {
    allowedTags: ["ul", "ol", "b", "i", "em", "strike", "pre", "strong", "li"],
    allowedAttributes: {},
  });

  const randomEnding = (
    "0000" + Math.floor(Math.random() * 65536).toString(16)
  ).slice(-4);
  const messageId = `${Date.now().toString()}-${randomEnding}`;
  const name = user.name;
  const label = user.label;
  const userId = user.userId;

  const Item: MessageInDb = {
    [db.Message.Primary.Key]: pk,
    [db.Message.Primary.Range]: messageId,
    [db.Primary.TimeToLive]: moment().add(30, "days").valueOf(),
    UserId: userId,
    Name: name,
    Label: label,
    Content: content,
    Question: messageDetails.question,
    ReplyTo: JSON.stringify(replyToMessage),
    Token: messageDetails.token ?? "",
  };
  await db.Client.put({
    TableName: db.MessagesTable,
    Item,
  }).promise();
}

export const sendMessage = async (event, _context) => {
  const body = await runValidation(
    SendMessageBodyValidator,
    JSON.parse(event.body)
  );

  console.log("sendMessage", body);

  const connectionId = db.parseEntityId(event);
  const sender = await getUser(body.channelId, connectionId);
  const roomId = body.sendTo ?? "general";

  // Ensures user can't send a message to anyone else
  // Other than general chat and his own
  if (
    roomId !== "general" &&
    roomId !== sender.UserId &&
    sender.Label === AuthType.ATTENDEE
  ) {
    return success;
  }

  await sendSingleMessage(body, {
    name: sender.Name,
    label: sender.Label,
    userId: sender.UserId,
  });

  return success;
};

async function getUser(channelId, connectionId) {
  const user = await db.Client.get({
    TableName: db.ConnectionsTable,
    Key: {
      [db.Channel.Connections.Key]: channelId,
      [db.Channel.Connections.Range]: connectionId,
    },
  }).promise();

  if (!user) {
    throw new Error("Invalid channel or connection");
  }

  if (!user.Item) {
    const event: ErrorEvent = {
      event: MessageEventType.ERROR,
      channelId,
      content: "Not authenticated to this channel",
    };

    wsClient.send(connectionId, event);
    return null;
  }

  return user.Item;
}

export const removeMessage = async (event, _context) => {
  const body = await runValidation(
    RemoveMessageBodyValidator,
    JSON.parse(event.body)
  );

  const { messageId, channelId } = body;
  const connectionId = db.parseEntityId(event);

  const user = await db.Client.get({
    TableName: db.ConnectionsTable,
    Key: {
      [db.Channel.Connections.Key]: channelId,
      [db.Channel.Connections.Range]: connectionId,
    },
  }).promise();

  if (
    user.Item.Label != AuthType.ATTENDEE ||
    connectionId === user.Item[db.Connection.Primary.Range]
  ) {
    await db.Client.update({
      TableName: db.MessagesTable,
      Key: {
        [db.Channel.Messages.Key]: `${body.channelId}-general`,
        [db.Channel.Messages.Range]: messageId,
      },
      UpdateExpression: "set Removed = :r",
      ConditionExpression: "UserId = :u",
      ExpressionAttributeValues: {
        ":r": user.Item.Label,
        ":u": user.Item.UserId,
      },
    }).promise();
  } else {
    const errorEvent: ErrorEvent = {
      event: MessageEventType.ERROR,
      content: "You're not authorized",
    };
    await wsClient.send(event, errorEvent);
  }

  return success;
};

export const broadcast = async (event, _context) => {
  for (const record of event.Records) {
    const messageId = record.dynamodb.Keys[db.Message.Primary.Range].S;
    const pk = record.dynamodb.Keys[db.Message.Primary.Key].S;

    if (pk === "APPLICATION") {
      continue;
    }

    const [, eventId, ...roomParts] = pk.split("-");
    const roomId = roomParts.join("-");
    const channelId = getChannelId(+eventId);

    if (record.eventName === "INSERT") {
      const newRecord = record.dynamodb.NewImage;
      // New message
      const message: MessageType = {
        messageId,
        userId: newRecord.UserId.S,
        label: newRecord.Label.S,
        name: newRecord.Name.S,
        content: newRecord.Content.S,
        removed: newRecord.Removed?.S,
        question: newRecord.Question?.BOOL,
        replyTo: newRecord.ReplyTo?.S && JSON.parse(newRecord.ReplyTo.S),
      };

      const userId = newRecord.UserId?.S;

      await broadcastChannel<MessageEvent>(
        channelId,
        (user) => {
          const ownMessage = user.UserId === userId;
          return {
            event: MessageEventType.MESSAGE,
            message: {
              ...message,
              ownMessage,
            },
            token: ownMessage ? newRecord.Token.S : undefined,
            roomId,
          };
        },
        (u) =>
          roomId === "general" ||
          u.Label !== AuthType.ATTENDEE ||
          u.UserId === userId ||
          roomId === u.UserId
      );
    } else if (record.eventName === "MODIFY") {
      // Removed message
      await broadcastChannel<RemoveEvent>(channelId, () => ({
        event: MessageEventType.REMOVE,
        messageId,
        removedBy: record.dynamodb.NewImage.Removed?.S,
      }));
    }
  }

  return success;
};

// Conditionally broadcasts to everyone on the channel (only to admins, only to attendees, etc)
async function broadcastChannel<T extends ChatEvent>(
  channelId: string,
  payload: (user: DocumentClient.AttributeMap) => T,
  condition: (user: DocumentClient.AttributeMap) => boolean = () => true
) {
  const subscribers = await db.fetchChannelSubscriptions(channelId);
  const promises = subscribers.map(async (subscriber) => {
    if (condition(subscriber)) {
      return wsClient.send(
        subscriber[db.Channel.Connections.Range],
        payload(subscriber)
      );
    } else {
      return Promise.resolve();
    }
  });
  return Promise.all(promises);
}

async function sendHistory(
  connectionId: string,
  userId: string,
  Label: string,
  channelId: string,
  roomId: string,
  from: string | undefined | null,
  to: string | undefined | null,
  isAttendee: boolean
) {
  const messages = await getMessagesFromChannel(
    channelId,
    roomId,
    from,
    to,
    isAttendee
  );

  const message = await getFirstMessageFromChannel(
    channelId,
    roomId,
    isAttendee
  );

  const history = messages.map<MessageType>((message) => ({
    userId: roomId,
    messageId: message[db.Channel.Connections.Range],
    ownMessage: message.UserId === userId && message.Label === Label,
    content: message.Content,
    question: message.Question,
    removed: message.Removed,
    label: message.Label,
    name: message.Name,
    replyTo: message.ReplyTo && JSON.parse(message.ReplyTo),
  }));

  const historyEvent: HistoryEvent = {
    event: MessageEventType.HISTORY,
    messages: history,
    isAtBeginning:
      (messages.length > 0 &&
        messages[messages.length - 1]?.sk === message?.sk) ||
      (!from && to === message?.sk),
    isAtEnd: false,
    roomId,
  };

  return wsClient.send(connectionId, historyEvent);
}

export const historyChannel = async (event, _context) => {
  const body = await runValidation(
    HistoryChannelBodyValidator,
    JSON.parse(event.body)
  );
  const { channelId, roomId, from, to } = body;
  const connectionId = db.parseEntityId(event);

  try {
    const user = await getUser(channelId, connectionId);

    if (!user) {
      return;
    }

    const isAttendee = user.Label == AuthType.ATTENDEE;
    const isOwnRoom = user.UserId === roomId;
    const isGeneralRoom = roomId === "general";

    if (isAttendee && !(isOwnRoom || isGeneralRoom)) {
      throw new Error("Unauthorized");
    }

    await sendHistory(
      connectionId,
      user.UserId,
      user.Label,
      channelId,
      roomId,
      from,
      to,
      isAttendee
    );
  } catch (err) {
    console.error(err);
  }

  return success;
};

async function getFirstMessageFromChannel(
  channelId: string,
  roomId: string,
  isAttendee: boolean
) {
  const messages = await db.Client.query({
    TableName: db.MessagesTable,
    Limit: 1,
    ScanIndexForward: true,
    KeyConditions: {
      [db.Channel.Connections.Key]: {
        ComparisonOperator: "EQ",
        AttributeValueList: [`${channelId}-${roomId}`],
      },
    },
    QueryFilter: isAttendee
      ? {
          Removed: {
            ComparisonOperator: "NULL",
          },
        }
      : undefined,
  }).promise();
  return messages.Items.length ? messages.Items[0] : null;
}

async function getMessagesFromChannel(
  channelId: string,
  roomId: string,
  from: string,
  to: string,
  isAttendee: boolean
) {
  let sortKeyConditions;
  console.log(to, from);
  if (from && to) {
    sortKeyConditions = {
      ComparisonOperator: "BETWEEN",
      AttributeValueList: [from, to],
    };
  } else if (to) {
    sortKeyConditions = {
      ComparisonOperator: "LT",
      AttributeValueList: [to],
    };
  } else {
    sortKeyConditions = {
      ComparisonOperator: "GT",
      AttributeValueList: [from],
    };
  }

  const response = await db.Client.query({
    TableName: db.MessagesTable,
    Limit: HISTORY_BATCH_SIZE,
    ScanIndexForward: false,
    KeyConditions: {
      [db.Channel.Connections.Key]: {
        ComparisonOperator: "EQ",
        AttributeValueList: [`${channelId}-${roomId}`],
      },
      [db.Channel.Connections.Range]: sortKeyConditions,
    },
    QueryFilter: isAttendee
      ? {
          Removed: {
            ComparisonOperator: "NULL",
          },
        }
      : undefined,
  }).promise();

  return response.Items;
}

async function getAuthorizedUserLabel(user: AuthUserObject, eventId: number) {
  const webinar = await Webinar.createQueryBuilder("webinar")
    .innerJoin("webinar.events", "event")
    .where("event.id = :eventId", { eventId })
    .getOne();

  const userTeam = await TeamToUser.findOne({
    user: { id: user.id },
  });

  if (userTeam.teamId !== webinar.teamId) {
    throw new Error("Unauthorized");
  }

  if (userTeam.role === TeamUserRole.OWNER) {
    return LabelType.PRESENTER;
  }
  return LabelType.SUPPORT;
}
function getEventId(channelId: string) {
  // Channel ID is in the format event-<event id>

  return +channelId.split("-", 2)[1];
}

async function getAuthorizedAttendeeLabel(
  authAttendee: AuthAttendeeObject,
  channelId: string
) {
  const eventId = getEventId(channelId);
  const attendee = await Attendee.findOne(authAttendee.id);
  if (attendee.eventId !== eventId) {
    throw new Error("Unauthorized");
  }

  return LabelType.ATTENDEE;
}

async function getAuthorizedUserOrAttendeeLabel(
  userOrAttendee: AuthUserObject | AuthAttendeeObject,
  channelId: string
) {
  const eventId = getEventId(channelId);
  if (userOrAttendee.type === AuthType.USER) {
    return getAuthorizedUserLabel(userOrAttendee, eventId);
  }
  return getAuthorizedAttendeeLabel(userOrAttendee, channelId);
}

function validateToken(token: string) {
  try {
    return verifyToken(token);
  } catch (error) {
    console.log(error);
    throw error;
  }
}

let connectionPromise: Promise<Connection>;

export const joinChannel = async (event, context) => {
  if (!connectionPromise) {
    connectionPromise = initializeDatabase({ name: "default" });
  }

  await connectionPromise;

  try {
    return await doSubscribeChannel(event, context);
  } catch (error) {
    console.log("Unable to subscribe to channel");
    console.error(error);

    const errorEvent: ErrorEvent = {
      event: MessageEventType.ERROR,
      content: "You're not authorized",
    };

    await wsClient.send(event, errorEvent);
    throw error;
  }
};

async function doSetAttended(attendee: AuthAttendeeObject) {
  try {
    await Attendee.update(attendee.id, { attended: true });
  } catch (err) {
    console.log(err);
  }
}

function getUserId(user: AuthUserObject | AuthAttendeeObject) {
  return `${user.type}-${user.id}`;
}

async function doSubscribeChannel(event, context) {
  const body = await runValidation(
    SubscribeBodyValidator,
    JSON.parse(event.body)
  );
  const { channelId, token } = body;

  const user = validateToken(token);
  const label = await getAuthorizedUserOrAttendeeLabel(user, channelId);
  const { firstName, lastName } = user;

  const connectionId = db.parseEntityId(event);

  const userId = getUserId(user);

  console.log(
    `New user: ${user.type}/${userId}; Connection Id: ${connectionId}`
  );

  const record: UserInDb = {
    [db.Channel.Connections.Key]: channelId,
    [db.Channel.Connections.Range]: connectionId,
    [db.Primary.TimeToLive]: moment().add(12, "hours").valueOf(),

    UserId: userId,
    Label: label,
    Name: `${firstName} ${lastName}`,
  };

  await db.Client.put({
    TableName: db.ConnectionsTable,
    Item: record,
  }).promise();

  if (label === LabelType.ATTENDEE) {
    await doSetAttended(user as AuthAttendeeObject);
    await broadcastChannel<JoinedEvent>(
      channelId,
      () => ({
        event: MessageEventType.JOINED,
        user: {
          label: record.Label,
          name: record.Name,
          userId: record.UserId,
        },
      }),
      (u) => u.Label != AuthType.ATTENDEE
    );
  } else {
    await usersChannel(event, context);
  }

  if (body.sendHistories) {
    await Promise.all(
      body.sendHistories.map((room) =>
        sendHistory(
          connectionId,
          getUserId(user),
          label,
          channelId,
          room.roomId,
          room.lastKey,
          null,
          label === LabelType.ATTENDEE
        )
      )
    );
  }

  return success;
}

export const leaveChannel = async (event, _context) => {
  const pk = JSON.parse(event.body).channelId;
  const sk = db.parseEntityId(event);

  const leaver = await getUser(pk, sk);

  await db.Client.delete({
    TableName: db.ConnectionsTable,
    Key: {
      [db.Channel.Connections.Key]: pk,
      [db.Channel.Connections.Range]: sk,
    },
  }).promise();

  if (leaver?.UserId) {
    await broadcastChannel<LeftEvent>(
      pk,
      () => ({
        event: MessageEventType.LEFT,
        user: {
          userId: leaver.UserId,
          label: leaver.Label,
          name: leaver.Name,
        },
      }),
      (u) => u.Label != AuthType.ATTENDEE
    );
  }

  return success;
};

export const usersChannel = async (event, _context) => {
  const body = await runValidation(
    UsersChannelBodyValidator,
    JSON.parse(event.body)
  );

  const [, eventId] = body.channelId.split("-", 2);
  const connectedWebSockets = await db.fetchChannelSubscriptions(
    getChannelId(+eventId)
  );

  const attendeesEvent: AttendeesListEvent = {
    event: MessageEventType.ATTENDEES_LIST,
    attendees: connectedWebSockets
      .filter((conn) => conn.Label === "attendee")
      .map((attendee) => ({
        label: attendee.Label,
        name: attendee.Name,
        userId: attendee.UserId,
      })),
  };

  await wsClient.send(event, attendeesEvent);

  return success;
};

export const barsChannel = async (event, _context) => {
  const body = await runValidation(
    UsersChannelBodyValidator,
    JSON.parse(event.body)
  );

  if (!connectionPromise) {
    connectionPromise = initializeDatabase({ name: "default" });
  }

  await connectionPromise;

  const [, eventId] = body.channelId.split("-", 2);

  const eventEntity = await Event.findOne(eventId);
  if (!eventEntity?.id) {
    throw new Error("Event not found");
  } else if (new Date() < eventEntity.start) {
    throw new Error("Event has not started yet");
  }

  const messages = await db.Client.query({
    TableName: db.MessagesTable,
    ScanIndexForward: false,
    KeyConditions: {
      [db.Channel.Connections.Key]: {
        ComparisonOperator: "EQ",
        AttributeValueList: [`${body.channelId}-general`],
      },
      [db.Channel.Connections.Range]: {
        ComparisonOperator: "GE",
        AttributeValueList: [eventEntity.start.getTime().toString()],
      },
    },
  }).promise();

  const timestep =
    (eventEntity.end.getTime() - eventEntity.start.getTime()) /
    BARS_PER_WEBINARS;

  const currentBarsCount = Math.floor(
    Math.min(
      (new Date().getTime() - eventEntity.start.getTime()) / BARS_PER_WEBINARS,
      BARS_PER_WEBINARS // Just make sure we don't load too many bars, remove for live webinars
    )
  );

  const processedChats = messages.Items.map((message) => ({
    timestamp: new Date(+message[db.Channel.Messages.Range].split("-", 1)[0]),
    question: message.Question,
  }));

  const bars = Array(currentBarsCount)
    .fill(0)
    .map((_, i) => {
      const upper = Math.floor(eventEntity.start.getTime() + i * timestep);
      const lower = Math.floor(
        eventEntity.start.getTime() + (i - 1) * timestep
      );

      const messages = processedChats.filter(
        (chat) =>
          lower <= chat.timestamp.getTime() && chat.timestamp.getTime() < upper
      );

      return {
        question: messages.reduce((ac, v) => ac || v.question, false),
        count: messages.length,
      };
    });

  const barsEvent: BarsChannelEvent = {
    event: MessageEventType.BARS_CHANNEL,
    bars,
  };

  await wsClient.send(event, barsEvent);

  return success;
};
