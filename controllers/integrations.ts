import {
  APIIntegrationSetData,
  IntegrationObjectFull,
  INTEGRATIONS,
} from "@emsyte/common/integration";
import { Integration } from "../db/entities/Integration";
import { Team } from "../db/entities/Team";
import { validateProviderSettings } from "../integrations/actions";

export async function doGetIntegrationSettings(
  team: Team
): Promise<IntegrationObjectFull[]> {
  const integrations = await Integration.find({
    where: { team },
  });
  return INTEGRATIONS.map((integration) => {
    const existing = integrations.find(
      (i) => i.provider === integration.provider
    );

    return existing
      ? {
          ...integration,
          settings: existing.settings,
          active: true,
        }
      : {
          ...integration,
          settings: null,
          active: false,
        };
  });
}

export async function doSetIntegrationSetting(
  team: Team,
  integration: APIIntegrationSetData
): Promise<IntegrationObjectFull> {
  const { provider, settings } = integration;
  const newSettings = await validateProviderSettings(provider, settings);
  const definition = INTEGRATIONS.find((i) => i.provider === provider);

  try {
    const integration = await Integration.findOne({
      where: { team, ...Integration.en("provider", provider) },
    });

    integration.settings = newSettings;

    await integration.save();

    return {
      ...definition,
      settings: integration.settings,
      active: true,
    };
  } catch (error) {
    const integration = await Integration.create({
      team,
      provider,
      settings: newSettings,
    }).save();

    return {
      ...definition,
      settings: integration.settings,
      active: true,
    };
  }
}
