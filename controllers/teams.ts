import { Team, TeamToUser } from "../db/entities/Team";
import RouterError from "../utils/RouterError";
import {
  APIRegisterTeamData,
  TeamUserRole,
  TeamUserStatus,
  APIEditTeamData,
  TeamStatus,
  TeamPlan,
} from "@emsyte/common/team";
import { User } from "../db/entities/User";
import * as uuid from "uuid";
import { doCreateCustomer, doCreateSubscription } from "./stripe";
export async function doCheckTeamName(slug: string) {
  const teamCount = await Team.count({ slug });
  if (teamCount === 0) {
    return {
      message: "Team name available",
    };
  }

  throw new RouterError(
    "Team name already taken",
    409,
    "Team name already taken"
  );
}

export async function doRegisterTeam(
  userId: number,
  data: APIRegisterTeamData
) {
  const user = await User.findOne(userId);
  const team = await Team.create({
    ...data,

    // In the future, we may allow users to pick their own slugs, but for now, a UUID should suffice
    slug: uuid.v4(),

    // This is a placeholder, the plan will actually be set when the billing information is added
    plan: TeamPlan.FREE,

    // By default use the user email as the billing email.
    billingEmail: user.email,

    // Once billing is setup, the status will be ENABLED
    status: TeamStatus.DISABLED,
  }).save();

  await TeamToUser.create({
    team,
    user,
    role: TeamUserRole.OWNER,
    status: TeamUserStatus.ACTIVATED,
  }).save();

  await doCreateCustomer(team, data.source);
  await doCreateSubscription(team, data.price, data.coupon);

  return team;
}

export async function doDeleteTeam(team: Team) {
  try {
    return await team.remove();
  } catch (e) {
    console.error(e);

    throw new RouterError(
      "Failed to delete team",
      500,
      "Failed to delete team"
    );
  }
}

export async function doEditTeam(team: Team, data: APIEditTeamData) {
  await team.update(data);
  return team;
}
