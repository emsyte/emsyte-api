import * as AWS from "aws-sdk";
import { randomBytes } from "crypto";
import * as moment from "moment";
import { getManager, LessThan, MoreThan } from "typeorm";
import { Event } from "../../db/entities/Event";
import { TaskRunner } from "../../utils/TaskRunner";

const sfn = new AWS.StepFunctions();

async function runChatInsertionCheck() {
  if (process.env.NODE_ENV !== "production") {
    return;
  }

  const events = await Event.find({
    // It's ok to start early
    start: LessThan(moment().add(10, "minutes").toDate()),
    end: MoreThan(moment().subtract(10, "minutes").toDate()),

    started: false,
  });

  console.log(`Starting ${events.length} chat insertion step functions`);

  await Promise.all(
    events.map(
      async (event) =>
        await getManager().transaction(async (manager) => {
          await manager.getRepository(Event).update(event.id, {
            started: true,
          });
          const name = `event-${event.id}-${randomBytes(4).toString("hex")}`;

          console.log(`Starting: ${name}`);

          await sfn
            .startExecution({
              // TODO: Pull this from environment variable
              stateMachineArn: process.env.CHAT_INSERTION_STATE_MACHINE,
              input: JSON.stringify({ eventId: event.id }),
              name: `event-${event.id}-${randomBytes(4).toString("hex")}`,
            })
            .promise();
        })
    )
  );

  console.log("Finished checking chat insertions");
}

class ChatInsertionTask extends TaskRunner<{ action: string }, {}> {
  async run() {
    await runChatInsertionCheck();

    return {};
  }
}

export const handler = new ChatInsertionTask().getHandler();
