import { Reaction, ReactionStatus } from "../../db/entities/Reaction";
import { TaskRunner } from "../../utils/TaskRunner";
import { doCalculateReaction } from "./../reactions";

async function runAnalyticsCheck() {
  const reactions = await Reaction.createQueryBuilder("reaction")
    .innerJoin("reaction.attendee", "attendee")
    .innerJoin("attendee.event", "event")
    .where("event.end < now()")
    .andWhere("reaction.status = :status", {
      ...Reaction.en("status", ReactionStatus.STAGING),
    })
    .getMany();

  await Promise.all(reactions.map(doCalculateReaction));

  console.log("Finished checking reactions");
}

class AnalyticsCheckTask extends TaskRunner<{ action: string }, {}> {
  async run() {
    await runAnalyticsCheck();

    return {};
  }
}

export const handler = new AnalyticsCheckTask().getHandler();
