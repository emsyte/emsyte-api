import { TaskRunner } from "../../utils/TaskRunner";
import { doSendLatestNotifications } from "./../webinars/notifications";

async function runNotificationCheck() {
  await doSendLatestNotifications();
  console.log("Finished sending notifications");
}

class NotificationsCheckTask extends TaskRunner<{ action: string }, {}> {
  async run() {
    await runNotificationCheck();

    return {};
  }
}

export const handler = new NotificationsCheckTask().getHandler();
