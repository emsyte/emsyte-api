import { TeamPlan, TeamStatus } from "@emsyte/common/team";
import Stripe from "stripe";
import { getManager } from "typeorm";
import { Team } from "../db/entities/Team";
import RouterError from "../utils/RouterError";

const { STRIPE_SECRET_KEY } = process.env;

const stripe = new Stripe(STRIPE_SECRET_KEY, {
  apiVersion: "2020-08-27",
  typescript: true,
});

function getCustomerId(team: Team) {
  const { stripeCustomerId } = team;
  if (!stripeCustomerId || stripeCustomerId.length === 0) {
    throw new RouterError("No customer on team!", 409, "Setup payment method!");
  }
  return stripeCustomerId;
}

async function getOrCreateCustomerId(team: Team) {
  try {
    return getCustomerId(team);
  } catch {
    const customer = await stripe.customers.create({
      email: team.billingEmail,
      description: `Team ${team.name}`,
    });

    await team.update({
      stripeCustomerId: customer.id,
    });
  }
}

/*
 * Stripe Base Methods
 * --------------
 */

export async function doGetPlans() {
  const products = await stripe.products.list({ active: true });
  const prices = await stripe.prices.list({
    active: true,
  });

  return products.data.map((product) => ({
    ...product,
    prices: prices.data
      .filter((price) => price.product === product.id)
      .map((price) => ({
        ...price,
        amount_usd: price.unit_amount / 100,
      })),
  }));
}

export async function doValidatePromotionCode(promoCode: string) {
  const promotionCodes = await stripe.promotionCodes.list({
    code: promoCode,
    active: true,
  });

  if (promotionCodes.data.length === 0) {
    return { valid: false, message: "Invalid coupon" };
  }

  const code = promotionCodes.data[0];

  return {
    valid: true,
    // TODO: There are a lot of different ways depending on how the coupon code is constructed.
    message: `${code.coupon.percent_off}% off ${code.coupon.duration}`,
  };
}

/*
 * Customer Methods
 * --------------
 */
export async function doCreateCustomer(team: Team, source: string) {
  if (team.stripeCustomerId) {
    return team;
  }

  try {
    const customer = await stripe.customers.create({
      email: team.billingEmail,
      description: `Team ${team.name}`,
      source,
    });

    await team.update({
      stripeCustomerId: customer.id,
    });

    return team;
  } catch (e) {
    console.log(e);
    throw new RouterError("Unable to add card", 400, e.message);
  }
}

export async function doUpdateBillingEmail(team: Team, billingEmail: string) {
  getManager().transaction(async (entityManager) => {
    team.billingEmail = billingEmail;
    await entityManager.save(team);

    await stripe.customers.update(getCustomerId(team), {
      email: billingEmail,
    });
  });
  return team;
}

async function getPlanFromPrice(priceId: string) {
  const price = await stripe.prices.retrieve(priceId, {
    expand: ["product"],
  });

  const product = price.product as Stripe.Product;
  return product.metadata.plan as TeamPlan;
}

/*
 * Subscriptions Methods
 * --------------
 */

export const doCreateSubscription = async (
  team: Team,
  price: string,
  promoCode: string
) => {
  const plan = await getPlanFromPrice(price);
  await team.update({ plan, status: TeamStatus.ENABLED });

  const subscription_options: Stripe.SubscriptionCreateParams = {
    customer: getCustomerId(team),
    items: [{ price }],
  };

  if (promoCode) {
    const promotionCodes = await stripe.promotionCodes.list({
      code: promoCode,
    });

    if (!promotionCodes.data.length) {
      throw RouterError.basic("Invalid promotion code", 409);
    }

    subscription_options.promotion_code = promotionCodes.data[0].id;
  }

  try {
    const subscription = await stripe.subscriptions.create(
      subscription_options
    );
    return subscription;
  } catch (e) {
    console.log(e);
    throw new RouterError("Failed to create new subscription in Stripe!", 400);
  }
};

export const doDeactivateSubscription = async (team: Team) => {
  try {
    const subscriptions = await stripe.subscriptions.list({
      customer: getCustomerId(team),
    });

    if (!subscriptions.data.length) {
      return;
    }

    const subscription = await stripe.subscriptions.update(
      subscriptions.data[0].id,
      {
        cancel_at_period_end: true,
      }
    );
    const {
      cancel_at_period_end,
      // current_period_end,
    } = subscription;
    if (cancel_at_period_end) {
      console.log(cancel_at_period_end);
      await team.update({
        status: TeamStatus.DISABLED,
      });
    }

    return { subscription };
  } catch (error) {
    throw new RouterError(
      error,
      400,
      error.apiResponse?.message ?? error.message
    );
  }
};

export const doGetSubscription = async (team: Team) => {
  try {
    const subscription = await stripe.subscriptions.list({
      customer: getCustomerId(team),
      expand: ["data.plan.product"],
    });
    return subscription.data[0];
  } catch (error) {
    throw new RouterError(error, 400);
  }
};

/*
 * Stripe Payments Methods
 * ----------------------
 */
export const doGetPaymentMethods = async (team: Team) => {
  try {
    const response = await stripe.paymentMethods.list({
      customer: getCustomerId(team),
      type: "card",
    });
    return response.data;
  } catch (e) {
    throw new RouterError(
      "Failed to get the payment methods for customer in Stripe!",
      400,
      "Failed to get payment methods"
    );
  }
};

export const doReplacePaymentMethods = async (team: Team, source: string) => {
  try {
    const customerId = await getOrCreateCustomerId(team);
    return await stripe.customers.update(customerId, {
      source,
    });
  } catch (e) {
    console.log(e);
    throw new RouterError(
      "Failed to update payment method",
      400,
      "Failed to update payment method"
    );
  }
};

/*
 * Stripe Transaction History
 * ----------------------
 */
export const doGetTransactionHistory = async (team: Team) => {
  try {
    const response = await stripe.charges.list({
      customer: getCustomerId(team),
      limit: 10,
    });
    return response.data.map((obj) => ({
      ...obj,
      amount_usd: obj.amount / 100,
    }));
  } catch {
    throw new RouterError(
      "Failed to get the transaction history in Stripe!",
      400
    );
  }
};
