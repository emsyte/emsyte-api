import { Reaction, ReactionStatus } from "../db/entities/Reaction";
import AnalyticsService, { EmsyteReport } from "../lib/analyticsService";
import * as jwt from "jsonwebtoken";

const API_BASE = process.env.API_BASE;
const APP_SECRET = process.env.APP_SECRET || "not_a_secret";

async function getAttendeeReaction(attendeeId: number) {
  const reaction = await Reaction.findOne({ attendee: { id: attendeeId } });
  if (reaction) {
    return reaction;
  }

  const { key } = await AnalyticsService.start(["attention", "emotion"]);

  return await Reaction.create({
    attendee: { id: attendeeId },
    status: ReactionStatus.STAGING,
    report: {},
    observationKey: key,
  }).save();
}

export async function doGetUploadUrl(attendeeId: number, start: number) {
  const reaction = await getAttendeeReaction(attendeeId);
  return await AnalyticsService.getUploadUrl(reaction.observationKey, start);
}

export interface Meta {
  token: string;
}

export async function doCalculateReaction(reaction: Reaction) {
  console.log("starting", reaction);
  const response = await AnalyticsService.finalize(reaction.observationKey, {
    callback: `${API_BASE}/webhooks/emsyte`,
    // callback: "https://h2lf97gi80.execute-api.us-east-1.amazonaws.com/dev",
    meta: {
      token: jwt.sign({ id: reaction.id }, APP_SECRET, {
        expiresIn: "1d",
      }),
    },
  });
  await reaction.update({ status: ReactionStatus.PROCESSING });
  return response;
}

export async function doHandleReactionReport(report: EmsyteReport<Meta>) {
  const { id } = jwt.verify(report.meta.token, APP_SECRET) as any;
  const reaction = await Reaction.findOne(id);
  await reaction.update({
    report: report.report as any,
    status: ReactionStatus.READY,
  });
}
