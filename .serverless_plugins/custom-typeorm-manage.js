"use strict";

const { createConnection } = require("typeorm");
const {
  Builder,
  // fixturesIterator,
  Loader,
  Parser,
  Resolver,
} = require("typeorm-fixtures-cli/dist");
const path = require("path");

const { sum } = require("lodash");

function* fixturesBatchIterator(fixtures, batchSize = 50) {
  const state = {};
  console.log(`total fixtures: ${fixtures.length}`);
  while (true) {
    const results = fixtures
      .filter(
        (l) =>
          sum(l.dependencies.map((d) => (state[d] !== undefined ? 1 : 0))) ===
            l.dependencies.length && !state[l.name]
      )
      .slice(0, batchSize);

    if (results.length) {
      for (const result of results) {
        state[result.name] = true;
      }
      yield results;
    } else {
      return;
    }
  }
}

// Inspired by https://github.com/nathanagez/serverless-typeorm-migrations
class MigrationPlugin {
  connection = null;

  constructor(serverless, options) {
    this.serverless = serverless;

    this.options = options;
    this.log = (message) =>
      this.serverless.cli.log(`Serverless Migrations - ${message}`);

    this.commands = {
      migrate: {
        usage: "",
        lifecycleEvents: ["help"],
        commands: {
          up: {
            usage: "Runs forward migrations",
            lifecycleEvents: ["migrate"],
          },
          down: {
            usage: "Rolls back migration",
            lifecycleEvents: ["rollback"],
          },
        },
      },
      fixtures: {
        usage: "",
        lifecycleEvents: ["help"],
        commands: {
          load: {
            usage: "Load fixture into the database",
            lifecycleEvents: ["load"],
          },
        },
      },
    };

    this.hooks = {
      "migrate:help": this.missingArguments.bind(this),
      "migrate:up:migrate": this.migrate.bind(this),
      "migrate:down:rollback": this.rollback.bind(this),
      "fixtures:help": this.missingArguments.bind(this),
      "fixtures:load:load": this.loadFixtures.bind(this),
    };
  }

  missingArguments() {
    this.log(`Missing arguments run serverless migrate --help`);
  }

  afterDeploy() {
    if (this.options.function) {
      this.log(`Calling migration function: ${this.options.function}`);
      this.serverless.pluginManager.invoke(["invoke"]);
    } else {
      this.log("No migration function defined");
      this.log("Specify a function name using the --function / -f option.");
    }
  }

  async init() {
    if (!this.connection) {
      this.connection = await createConnection(this.getConnectionOptions());
    }
  }

  /**
   * Drops the current database, runs the migrations, and loads the fixtures
   */
  async loadFixtures() {
    await this.init();
    this.log("Dropping data");
    await this.connection.dropDatabase();
    await this.migrate();

    this.log("Loading fixtures");
    await this._loadFixtures();
  }

  async _loadFixtures() {
    const loader = new Loader();
    loader.load(path.resolve(this.getFixturesPath()));

    const resolver = new Resolver();
    const fixtures = resolver.resolve(loader.fixtureConfigs);
    const builder = new Builder(this.connection, new Parser());

    for (const fixtureBatch of fixturesBatchIterator(fixtures)) {
      await this.connection.transaction(async (manager) => {
        await Promise.all(
          fixtureBatch.map(async (fixture) => {
            const entity = await builder.build(fixture);
            const repository = manager.getRepository(entity.constructor.name);

            this.log(`Loading fixture ${fixture.name}`);
            await repository.save(entity);
          })
        );
      });
    }
  }

  async migrate() {
    this.log("Looking for migrations");
    await this.init();

    const migration = new Migration(this.connection);
    const migrations = await migration.runMigration();
    this.logMigrations(migrations);
  }

  logMigrations(migrations) {
    if (typeof migrations !== "undefined" && migrations?.length > 0) {
      migrations.forEach((migration) =>
        this.log(`Migrated: ${migration.name}`)
      );
    } else {
      this.log("No new migrations to run");
    }
  }

  async rollback() {
    this.log("Undoing last migration");
    await this.init();
    const migration = new Migration(this.connection);
    await migration.undoLastMigration();
    this.log("Done.");
  }

  getConnectionOptions() {
    const env = this.serverless.service.provider.environment;
    // if (env.APP_ENV === "production") {
    return {
      type: "aurora-data-api-pg",
      database: env.DB_DATABASE,
      secretArn: env.DB_SECRET,
      resourceArn: env.DB_ARN,
      region: "us-east-1",
      migrations: [this.getMigrationPath()],
      entities: [this.getEntityPath()],
    };
    // }

    return {
      username: env.DB_USER,
      password: env.DB_PASSWORD,
      database: env.DB_DATABASE,
      host: env.DB_HOST,
      port: parseInt(env.DB_PORT),
      type: "postgres",
      migrations: [this.getMigrationPath()],
      entities: [this.getEntityPath()],
    };
  }

  getMigrationPath() {
    const custom = this.serverless.service.custom["custom-migrations"];

    if (!custom.migrationPath) {
      this.log("migrationPath option must be set!");
      process.exit(1);
    }
    return custom.migrationPath;
  }

  getEntityPath() {
    const custom = this.serverless.service.custom["custom-migrations"];

    if (!custom.entityPath) {
      this.log("entityPath option must be set!");
      process.exit(1);
    }
    return custom.entityPath;
  }

  getFixturesPath() {
    const custom = this.serverless.service.custom["custom-migrations"];

    if (!custom.fixturesPath) {
      this.log("fixturesPath option must be set!");
      process.exit(1);
    }
    return custom.fixturesPath;
  }
}

class Migration {
  constructor(connection) {
    this.connection = connection;
  }

  async runMigration() {
    try {
      const result = await this.connection?.runMigrations({
        transaction: "none",
      });

      return result;
    } catch (error) {
      throw error;
    }
  }

  async undoLastMigration() {
    try {
      await this.connection?.undoLastMigration({
        transaction: "none",
      });
    } catch (error) {
      throw error;
    }
  }
}

module.exports = MigrationPlugin;
