import { User } from "./entities/User";
import { Attendee } from "./entities/Attendee";
import { Event, SentEventNotification } from "./entities/Event";
import { Integration } from "./entities/Integration";
import { Notification } from "./entities/Notification";
import { Offer } from "./entities/Offer";
import { Template, TemplateInstance } from "./entities/Template";
import { Recurrence, Webinar } from "./entities/Webinar";
import { ConnectionOptions, Connection, createConnection } from "typeorm";
import { Team, TeamToUser } from "./entities/Team";
import { Reaction } from "./entities/Reaction";
import { PageView } from "./entities/PageView";
import { OfferEvent } from "./entities/OfferEvent";
import { PaidRegistration } from "./entities/PaidRegistration";
import { ChatMessage } from "./entities/ChatMessage";
import { NotificationGateway } from "./entities/NotificationsGateway";
import { ViewerEvent } from "./entities/ViewerEvent";

const ENTITIES = [
  User,
  Team,
  TeamToUser,
  Attendee,
  Reaction,
  PageView,
  OfferEvent,
  Event,
  Integration,
  PaidRegistration,
  Notification,
  SentEventNotification,
  Offer,
  Template,
  TemplateInstance,
  ChatMessage,
  NotificationGateway,
  Webinar,
  Recurrence,
  ViewerEvent,
];

// Can't read ormconfig.json in production, so the options need to be duplicated here

// istanbul ignore next
export function getEnvConnectionOptions(): ConnectionOptions {
  if (process.env.NODE_ENV === "production") {
    return {
      type: "aurora-data-api-pg",
      database: process.env.DB_DATABASE,
      secretArn: process.env.DB_SECRET,
      resourceArn: process.env.DB_ARN,
      region: "us-east-1",
    };
  }

  return {
    username: process.env.DB_USER ?? "emsyte",
    password: process.env.DB_PASSWORD ?? "emsyte",
    database: process.env.DB_DATABASE ?? "justwebinar",
    host: process.env.DB_HOST ?? "127.0.0.1",
    port: parseInt(process.env.DB_PORT) ?? 5432,
    type: "postgres",
  };
}

export const initializeDatabase = async (
  optionOverrides: Partial<ConnectionOptions> = {}
): Promise<Connection> => {
  const connectionOptions = getEnvConnectionOptions();
  const options: any = {
    entities: ENTITIES,

    ...connectionOptions,

    ...optionOverrides,
  };

  const connection = await createConnection(options);

  return connection;
};

export default initializeDatabase;
