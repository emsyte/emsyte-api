const report = require("../data/report.json");

class ReactionProcessor {
  postProcess(_name, object) {
    object.report = report.report;
  }
}

exports.default = ReactionProcessor;
