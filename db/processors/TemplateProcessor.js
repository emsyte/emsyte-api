class TemplateProcessor {
  postProcess(_name, object) {
    if (object.content) {
      const content = require(`../data/${object.content}.json`);
      object.content = JSON.stringify(content);
    } else {
      object.content = "";
    }
  }
}

exports.default = TemplateProcessor;
