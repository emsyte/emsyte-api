class ViewerEventProcessor {
  postProcess(_name, object) {
    if (object.time_offset) {
      object.time_offset = parseInt(object.time_offset);
    }
  }
}

exports.default = ViewerEventProcessor;
