class TemplateProcessor {
  postProcess(_name, object) {
    object.offset_timestamp = parseInt(object.offset_timestamp);
  }
}

exports.default = TemplateProcessor;
