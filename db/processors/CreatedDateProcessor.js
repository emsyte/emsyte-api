class TemplateProcessor {
  postProcess(_name, object) {
    if (object.createdDate) {
      object.createdDate = new Date(object.createdDate);
    }
  }
}

exports.default = TemplateProcessor;
