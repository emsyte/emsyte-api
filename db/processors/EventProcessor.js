class TemplateProcessor {
  postProcess(_name, object) {
    object.start = new Date(object.start);
    object.end = new Date(object.end);
  }
}

exports.default = TemplateProcessor;
