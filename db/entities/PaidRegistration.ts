import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { Attendee } from "./Attendee";
import { CoreEntity } from "./Core";
import { Event } from "./Event";
import { Webinar } from "./Webinar";

@Entity({ name: "paid_registration" })
export class PaidRegistration extends CoreEntity {
  @Column("integer")
  registrationFee: number;

  @ManyToOne(() => Webinar)
  webinar: Webinar;

  @ManyToOne(() => Event)
  event: Event;

  @ManyToOne(() => Attendee)
  @JoinColumn({ name: "attendeeId" })
  attendee: Attendee;

  @Column("int")
  attendeeId: number;
}
