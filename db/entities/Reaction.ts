import { Entity, OneToOne, Column, JoinColumn } from "typeorm";
import { Attendee } from "./Attendee";
import { CoreEntity } from "./Core";

export enum ReactionStatus {
  /** Observation created, collecting data */
  STAGING = "staging",

  /** Has started processing the data */
  PROCESSING = "processing",

  /** Backend has finished processing the data and the report is ready */
  READY = "ready",
}

export const EMOTIONS = [
  "neutral",
  "happiness",
  "surprise",
  "sadness",
  "anger",
] as const;

export type Emotion = typeof EMOTIONS[number];

export interface ReactionReport {
  attention?: number[];
  emotion?: Record<Emotion, number[]>;
}

@Entity({ name: "reaction" })
export class Reaction extends CoreEntity {
  @Column("varchar")
  observationKey: string;

  @Column("enum", { enum: Object.values(ReactionStatus) })
  status: ReactionStatus;

  @Column("jsonb")
  report: ReactionReport;

  @OneToOne(() => Attendee, (attendee) => attendee.reaction)
  @JoinColumn()
  attendee: Attendee;
}
