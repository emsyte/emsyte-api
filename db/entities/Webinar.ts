import { WebinarType, WebinarStatus, Frequency } from "@emsyte/common/webinar";
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  JoinColumn,
  OneToOne,
} from "typeorm";
import { CoreEntity } from "./Core";
import { Event } from "./Event";
import { Notification } from "./Notification";
import { Offer } from "./Offer";
import { Team } from "./Team";
import { WebinarSettings } from "./WebinarSettings";
import { TemplateInstance } from "./Template";
import { NotificationGateway } from "./NotificationsGateway";

import { RRule, Options } from "rrule/dist/es5/rrule-tz";
import { UnreachableCaseError } from "../../utils/errors";
import * as moment from "moment-timezone";

export const WEEKDAY = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

const RRULE_WEEKDAY = [
  RRule.SU,
  RRule.MO,
  RRule.TU,
  RRule.WE,
  RRule.TH,
  RRule.FR,
  RRule.SA,
];

function getFrequency(frequency: Frequency) {
  switch (frequency) {
    case Frequency.WEEKLY:
      return RRule.WEEKLY;
    case Frequency.DAILY:
      return RRule.DAILY;
    default:
      throw new UnreachableCaseError(frequency);
  }
}

function getDay(day: number) {
  return RRULE_WEEKDAY[day];
}

@Entity({ name: "webinar" })
export class Webinar extends CoreEntity {
  @Column("varchar")
  name: string;

  @Column("enum", { enum: Object.values(WebinarType) })
  type: WebinarType;

  @Column("varchar", { default: "" })
  description: string;

  @Column("varchar")
  videoUrl: string;

  @Column("enum", {
    enum: Object.values(WebinarStatus),
    default: WebinarStatus.DRAFT,
  })
  status: WebinarStatus;

  /** Duration of the webinar in seconds */
  @Column("integer")
  duration: number;

  // Is this in days?
  @Column("smallint", { default: 0 })
  replayExpiration: number;

  @Column("varchar", { default: "" })
  thumbnail: string;

  @Column("bool", { default: false })
  isPaid: boolean;

  @Column("integer", { default: 0 })
  registrationFee: number;

  @Column("varchar", { default: "" })
  checkoutLink: string;

  @Column("varchar", { default: "" })
  redirectLink: string;

  /** Webinar settings */
  @Column(() => WebinarSettings)
  settings: WebinarSettings;

  @OneToOne(() => NotificationGateway, (gateway) => gateway.webinar, {
    nullable: true,
    onDelete: "SET NULL",
  })
  @JoinColumn()
  smsGateway: NotificationGateway;

  @OneToOne(() => NotificationGateway, (gateway) => gateway.webinar, {
    nullable: true,
    onDelete: "SET NULL",
  })
  @JoinColumn()
  emailGateway: NotificationGateway;

  /** Offers for the webinar attendees */
  @OneToMany(() => Offer, (offer) => offer.webinar)
  offers: Offer[];

  /** Email/SMS notifications for this webinar */
  @OneToMany(() => Notification, (notification) => notification.webinar)
  notifications: Notification[];

  /** Webinar streaming events */
  @OneToMany(() => Event, (event) => event.webinar)
  events: Event[];

  @Column("int", { nullable: true })
  teamId: number;

  @ManyToOne(() => Team)
  @JoinColumn({ name: "teamId" })
  team: Team;

  @OneToMany(() => TemplateInstance, (template) => template.webinar)
  templates: TemplateInstance[];

  @OneToMany(() => Recurrence, (recurrence) => recurrence.webinar)
  recurrences: Recurrence[];
}

@Entity({ name: "recurrence" })
export class Recurrence extends CoreEntity {
  @Column("enum", { enum: Object.values(Frequency) })
  frequency: Frequency;

  @Column("timestamp")
  start: Date;

  @Column("timestamp")
  end: Date;

  @Column("jsonb", { default: "[]" })
  days: number[];

  @Column("varchar", { default: "America/Chicago" })
  timezone: string;

  @Column("int", { nullable: true })
  webinarId: number;

  @ManyToOne(() => Webinar)
  @JoinColumn({ name: "webinarId" })
  webinar: Webinar;

  @OneToMany(() => Event, (event) => event.recurrence)
  events: Event[];

  getRRule() {
    const frequency = getFrequency(this.frequency);
    const options: Partial<Options> = {
      freq: frequency,
      dtstart: moment(this.start).toDate(),
      until: moment(this.end).tz(this.timezone, true).endOf("day").toDate(),
      tzid: this.timezone,
    };

    if (this.frequency === Frequency.WEEKLY) {
      options.byweekday = this.days.map(getDay);
    }

    return new RRule(options);
  }
}
