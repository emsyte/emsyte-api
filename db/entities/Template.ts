import { TemplateType } from "@emsyte/common/template";
import { Column, Entity, JoinColumn, ManyToOne, Unique } from "typeorm";
import { CoreEntity } from "./Core";
import { Team } from "./Team";
import { Webinar } from "./Webinar";

@Entity({ name: "template" })
export class Template extends CoreEntity {
  @Column("varchar")
  name: string;

  @Column("enum", { enum: Object.values(TemplateType) })
  type: TemplateType;

  @Column("text")
  content: string;

  @Column("varchar", { default: "" })
  thumbnail: string;

  @Column("boolean", { default: false })
  base: boolean;

  @Column("int", { nullable: true })
  teamId: number;

  @ManyToOne(() => Team)
  @JoinColumn({ name: "teamId" })
  team: Team;
}

@Entity({ name: "template_instance" })
@Unique(["webinarId", "type"])
export class TemplateInstance extends CoreEntity {
  @ManyToOne(() => Template)
  @JoinColumn({ name: "parentId" })
  parent: Template;

  @ManyToOne(() => Webinar, (webinar) => webinar.templates)
  @JoinColumn({ name: "webinarId" })
  webinar: Webinar;

  @Column("int", { nullable: true })
  parentId: number;

  @Column("int", { nullable: true })
  webinarId: number;

  @Column("enum", { enum: Object.values(TemplateType) })
  type: TemplateType;

  @Column("text")
  content: string;

  @Column("varchar", { default: "" })
  thumbnail: string;

  @Column("int", { nullable: true })
  teamId: number;

  @ManyToOne(() => Team)
  @JoinColumn({ name: "teamId" })
  team: Team;
}
