import { Column, Entity, ManyToOne } from "typeorm";
import { Attendee } from "./Attendee";
import { CoreEntity } from "./Core";
import { Offer } from "./Offer";

export enum OfferEventType {
  CLICK = "click",
  PURCHASE = "purchase",
}

@Entity({ name: "offer_event" })
export class OfferEvent extends CoreEntity {
  @Column("enum", {
    enum: Object.values(OfferEventType),
    default: OfferEventType.CLICK,
  })
  type: OfferEventType;

  @Column("bigint", { default: 0 })
  price: number;

  @ManyToOne(() => Offer)
  offer: Offer;

  @ManyToOne(() => Attendee)
  attendee: Attendee;
}
