import "reflect-metadata";
import {
  BaseEntity,
  CreateDateColumn,
  DeepPartial,
  EntityTarget,
  getConnection,
  getRepository,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

export abstract class CoreEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  async update(entity: DeepPartial<this>) {
    Object.assign(this, entity, { id: this.id });
    return await this.save();
  }

  static en<T extends BaseEntity, F extends keyof T>(
    this: EntityTarget<T>,
    field: F,
    value: T[F]
  ) {
    const column = getRepository(this).metadata.columns.find(
      (column) => column.propertyName === field
    );
    return {
      [field]: getConnection().driver.preparePersistentValue(value, column),
    } as Record<F, any>;
  }
}
