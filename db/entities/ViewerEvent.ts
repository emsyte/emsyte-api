import { Column, Entity, ManyToOne } from "typeorm";
import { Attendee } from "./Attendee";
import { CoreEntity } from "./Core";

@Entity({ name: "viewer_event" })
export class ViewerEvent extends CoreEntity {
  @ManyToOne(() => Attendee)
  attendee: Attendee;

  @Column("integer")
  time_offset: number;
}
