import {
  Column,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  Unique,
} from "typeorm";
import { CoreEntity } from "./Core";
import { Event } from "./Event";
import { Reaction } from "./Reaction";
import { ViewerEvent } from "./ViewerEvent";

@Entity({ name: "attendee" })
@Unique("unique_registrations", ["email", "event"])
export class Attendee extends CoreEntity {
  @Column("varchar")
  firstName: string;

  @Column("varchar")
  lastName: string;

  @Column("varchar")
  email: string;

  @Column("varchar")
  timezone: string;

  @Column("varchar", { default: "" })
  phone: string;

  @Column("uuid")
  @Generated("uuid")
  key: string;

  @Column("boolean", { default: false })
  active: boolean;

  @Column("boolean", { default: false })
  attended: boolean;

  /** Which event this attendee is registered for */
  @ManyToOne(() => Event, (event) => event.attendees)
  @JoinColumn({ name: "eventId" })
  event: Event;

  @Column("int", { nullable: true })
  eventId: number;

  @OneToOne(() => Reaction, (reaction) => reaction.attendee)
  reaction: Reaction;

  @OneToMany(() => ViewerEvent, (viewerEvent) => viewerEvent.attendee)
  viewer_events: ViewerEvent[];
}
