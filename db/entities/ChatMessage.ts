import { ManyToOne, Column, Entity } from "typeorm";
import { Webinar } from "./Webinar";
import { CoreEntity } from "./Core";
import { WebinarChatLabel, WebinarChatObject } from "@emsyte/common/webinar";

@Entity({ name: "chat_message" })
export class ChatMessage extends CoreEntity implements WebinarChatObject {
  @ManyToOne(() => Webinar, (webinar) => webinar.offers)
  webinar: Webinar;

  @Column("integer")
  offset_timestamp: number;

  @Column("enum", {
    enum: Object.values(WebinarChatLabel),
    default: WebinarChatLabel.ATTENDEE,
  })
  label: WebinarChatLabel;

  @Column("varchar")
  name: string;

  @Column("varchar")
  content: string;
}
