import { Column, Entity, Generated, ManyToOne } from "typeorm";
import { CoreEntity } from "./Core";
import { Template } from "./Template";
import { Webinar } from "./Webinar";

@Entity({ name: "page_view" })
export class PageView extends CoreEntity {
  @Column("uuid")
  @Generated("uuid")
  key: string;

  @ManyToOne(() => Template, { nullable: true })
  template: Template;

  @ManyToOne(() => Webinar)
  webinar: Webinar;
}
