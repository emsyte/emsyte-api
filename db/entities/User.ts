import { Column, Entity, OneToMany } from "typeorm";
import { CoreEntity } from "./Core";
import * as bcrypt from "bcryptjs";
import { TeamToUser } from "./Team";
import { UserStatus } from "@emsyte/common/user";

@Entity({ name: "user" })
export class User extends CoreEntity {
  @Column("varchar")
  firstName: string;

  @Column("varchar")
  lastName: string;

  @Column("varchar", { unique: true })
  email: string;

  @Column("varchar")
  password: string;

  @Column("enum", {
    enum: Object.values(UserStatus),
    default: UserStatus.ACTIVE,
  })
  status: UserStatus;

  @OneToMany((_type) => TeamToUser, (teamToUser) => teamToUser.user)
  teamToUsers: TeamToUser[];

  /**
   * Hash the password and set the hashed password on the entity
   *
   * @param password Raw password
   */
  async setPassword(password: string) {
    this.password = await bcrypt.hash(password, 12);
  }
}
