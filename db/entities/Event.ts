import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  Unique,
} from "typeorm";
import { Attendee } from "./Attendee";
import { CoreEntity } from "./Core";
import { Notification } from "./Notification";
import { Webinar, Recurrence } from "./Webinar";

@Entity({ name: "event" })
@Unique("unique_events", ["start", "webinar"])
export class Event extends CoreEntity {
  @Column("timestamp")
  start: Date;

  @Column("timestamp")
  end: Date;

  @Column("bool", { default: false })
  started: boolean;

  @Column("bool", { default: false })
  canceled: boolean;

  @Column("varchar", { default: "" })
  videoUrl: string;

  @OneToMany(
    () => SentEventNotification,
    (sentEventNotification) => sentEventNotification.event
  )
  sentEventNotifications: SentEventNotification[];

  @Column("int", { nullable: true })
  webinarId: number;

  @ManyToOne(() => Webinar, (webinar) => webinar.events)
  @JoinColumn({ name: "webinarId" })
  webinar: Webinar;

  @Column("int", { nullable: true })
  recurrenceId: number;

  @ManyToOne(() => Recurrence, (recurrence) => recurrence.events, {
    nullable: true,
    onDelete: "SET NULL",
  })
  @JoinColumn({ name: "recurrenceId" })
  recurrence: Recurrence;

  @OneToMany(() => Attendee, (attendee) => attendee.event)
  attendees: Attendee[];
}

@Entity({ name: "sent_event_notification" })
export class SentEventNotification extends CoreEntity {
  @ManyToOne(() => Event, (event) => event.sentEventNotifications)
  event: Event;

  @ManyToOne(() => Notification)
  notification: Notification;
}
