import { ManyToOne, Column, Entity } from "typeorm";
import { Webinar } from "./Webinar";
import { CoreEntity } from "./Core";

@Entity({ name: "offer" })
export class Offer extends CoreEntity {
  @ManyToOne(() => Webinar, (webinar) => webinar.offers)
  webinar: Webinar;

  @Column("varchar")
  name: string;

  /** Start time in seconds since the start of the webinar */
  @Column("integer")
  start: number;

  /** End time in seconds since the start of the webinar */
  @Column("integer")
  end: number;

  /** Price of offer in cents */
  @Column("bigint")
  price: number;

  @Column("varchar")
  buttonText: string;

  @Column("varchar")
  description: string;

  /** URL to product offer checkout page */
  @Column("varchar")
  checkout: string;

  @Column("varchar")
  thumbnail: string;
}
