import {
  NotificationDeliveryMethod,
  NotificationType,
} from "@emsyte/common/notification";
import { Column, ManyToOne, Entity } from "typeorm";
import { CoreEntity } from "./Core";
import { Event } from "./Event";
import { Webinar } from "./Webinar";

@Entity({ name: "notification" })
export class Notification extends CoreEntity {
  @Column("enum", {
    enum: Object.values(NotificationType),
  })
  type: NotificationType;

  @Column("enum", {
    enum: Object.values(NotificationDeliveryMethod),
  })
  deliveryMethod: NotificationDeliveryMethod;

  @Column("varchar", { default: "" })
  subject: string;

  @Column("varchar", { default: "" })
  content: string;

  @ManyToOne(() => Webinar)
  webinar: Webinar;

  // Placeholder for grabbing event + notification pairs
  events: Event[];

  /**
   * When to send the notification.
   *
   * For pre-event notifications it's the number of hours before the event starts.
   * For post-event notifications it's the number of hours after the event ends.
   */
  @Column("integer", { default: 0 })
  hoursOffset: number;
}
