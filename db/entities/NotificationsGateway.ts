import { Column, Entity, ManyToOne, OneToOne } from "typeorm";
import { CoreEntity } from "./Core";
import { Integration } from "./Integration";
import { Webinar } from "./Webinar";

@Entity({ name: "notification_gateway" })
export class NotificationGateway extends CoreEntity {
  @ManyToOne(() => Integration, {
    nullable: false,
    eager: true,
  })
  integration: Integration;

  @Column("jsonb")
  settings: any;

  @OneToOne(() => Webinar)
  webinar: Webinar;
}
