import {
  TeamPlan,
  TeamUserRole,
  TeamUserStatus,
  TeamStatus,
} from "@emsyte/common/team";
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  Index,
  Unique,
  JoinColumn,
} from "typeorm";
import { CoreEntity } from "./Core";
import { User } from "./User";

@Entity({ name: "team" })
export class Team extends CoreEntity {
  @Column("varchar")
  name: string;

  @Column("varchar", { unique: true })
  slug: string;

  @Column("enum", { enum: Object.values(TeamPlan) })
  plan: TeamPlan;

  @Column("enum", { enum: Object.values(TeamStatus) })
  status: TeamStatus;

  // TODO: Do we need to send a verification email?
  @Column("varchar")
  billingEmail: string;

  @Column("varchar", { default: "" })
  stripeCustomerId: string;

  @OneToMany((_type) => TeamToUser, (teamToUser) => teamToUser.team)
  teamToUsers: TeamToUser[];
}

@Entity({ name: "team_to_user" })
@Index(["team"], { where: "role='owner'", unique: true }) // One owner per team
@Unique("one_pair", ["team", "user"])
export class TeamToUser extends CoreEntity {
  @Column("int", { nullable: true })
  teamId: number;

  @ManyToOne((_type) => Team, (team) => team.teamToUsers, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "teamId" })
  team: Team;

  @ManyToOne((_type) => User, (user) => user.teamToUsers, {
    onDelete: "CASCADE",
  })
  user: User;

  @Column("enum", { enum: Object.values(TeamUserRole) })
  role: TeamUserRole;

  @Column("enum", { enum: Object.values(TeamUserStatus) })
  status: TeamUserStatus;
}
