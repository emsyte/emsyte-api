import { WebinarSettingsObject } from "@emsyte/common/webinar";
import { Column } from "typeorm";

export class WebinarSettings implements WebinarSettingsObject {
  @Column("varchar", { default: "" })
  postRegistrationUrl: string;

  @Column("varchar", { default: "" })
  postViewUrl: string;

  @Column("varchar", { default: "" })
  registrationPageScript: string;

  @Column("varchar", { default: "" })
  thankyouPageScript: string;

  @Column("varchar", { default: "" })
  registrationFormScript: string;

  @Column("varchar", { default: "" })
  liveRoomScript: string;

  @Column("varchar", { default: "" })
  replayRoomScript: string;
}
