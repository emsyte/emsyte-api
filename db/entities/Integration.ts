import { IntegrationProvider } from "@emsyte/common/integration";
import { Column, Entity, ManyToOne, JoinColumn, Unique } from "typeorm";
import { CoreEntity } from "./Core";
import { Team } from "./Team";

@Unique(["team", "provider"])
@Entity({ name: "integration" })
export class Integration extends CoreEntity {
  @Column("int", { nullable: true })
  teamId: number;

  @ManyToOne(() => Team)
  @JoinColumn({ name: "teamId" })
  team: Team;

  @Column("enum", { enum: Object.values(IntegrationProvider) })
  provider: IntegrationProvider;

  @Column("jsonb")
  settings: any;
}
