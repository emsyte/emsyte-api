import {MigrationInterface, QueryRunner} from "typeorm";

export class EventVideoUrl1624813629361 implements MigrationInterface {
    name = 'EventVideoUrl1624813629361'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "videoUrl" character varying NOT NULL DEFAULT ''`);
        await queryRunner.query(`COMMENT ON COLUMN "recurrence"."days" IS NULL`);
        await queryRunner.query(`ALTER TABLE "recurrence" ALTER COLUMN "days" SET DEFAULT '{}'::smallint[]`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "recurrence" ALTER COLUMN "days" SET DEFAULT '{}'`);
        await queryRunner.query(`COMMENT ON COLUMN "recurrence"."days" IS NULL`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "videoUrl"`);
    }

}
