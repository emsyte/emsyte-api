import {MigrationInterface, QueryRunner} from "typeorm";

export class WebinarRecurrence1624343921492 implements MigrationInterface {
    name = 'WebinarRecurrence1624343921492'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "recurrence_frequency_enum" AS ENUM('weekly', 'daily')`);
        await queryRunner.query(`CREATE TABLE "recurrence" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "frequency" "recurrence_frequency_enum" NOT NULL, "start" TIMESTAMP NOT NULL, "end" TIMESTAMP NOT NULL, "days" smallint array NOT NULL DEFAULT '{}'::smallint[], "timezone" character varying NOT NULL DEFAULT 'America/Chicago', "webinarId" integer, CONSTRAINT "PK_47b87ac66b63dd3f1cfa1674240" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "webinar" DROP COLUMN "start"`);
        await queryRunner.query(`ALTER TABLE "webinar" DROP COLUMN "end"`);
        await queryRunner.query(`ALTER TABLE "webinar" DROP COLUMN "times"`);
        await queryRunner.query(`ALTER TABLE "webinar" DROP COLUMN "days"`);
        await queryRunner.query(`ALTER TABLE "webinar" DROP COLUMN "timezone"`);
        await queryRunner.query(`ALTER TABLE "event" ADD "recurrenceId" integer`);
        await queryRunner.query(`ALTER TABLE "recurrence" ADD CONSTRAINT "FK_9e3a8c8d972c126ae9814431ad8" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_3cf7e81ec02220091d361ab04e8" FOREIGN KEY ("recurrenceId") REFERENCES "recurrence"("id") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_3cf7e81ec02220091d361ab04e8"`);
        await queryRunner.query(`ALTER TABLE "recurrence" DROP CONSTRAINT "FK_9e3a8c8d972c126ae9814431ad8"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "recurrenceId"`);
        await queryRunner.query(`ALTER TABLE "webinar" ADD "timezone" character varying NOT NULL DEFAULT 'America/Chicago'`);
        await queryRunner.query(`ALTER TABLE "webinar" ADD "days" smallint array NOT NULL DEFAULT '{}'`);
        await queryRunner.query(`ALTER TABLE "webinar" ADD "times" TIME array NOT NULL DEFAULT '{}'`);
        await queryRunner.query(`ALTER TABLE "webinar" ADD "end" date`);
        await queryRunner.query(`ALTER TABLE "webinar" ADD "start" date`);
        await queryRunner.query(`DROP TABLE "recurrence"`);
        await queryRunner.query(`DROP TYPE "recurrence_frequency_enum"`);
    }

}
