import { MigrationInterface, QueryRunner } from "typeorm";

export class Initial1611334932267 implements MigrationInterface {
  name = "Initial1611334932267";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "offer" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "start" integer NOT NULL, "end" integer NOT NULL, "price" bigint NOT NULL, "buttonText" character varying NOT NULL, "description" character varying NOT NULL, "checkout" character varying NOT NULL, "thumbnail" character varying NOT NULL, "webinarId" integer, CONSTRAINT "PK_57c6ae1abe49201919ef68de900" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "user_status_enum" AS ENUM('inactive', 'active')`
    );
    await queryRunner.query(
      `CREATE TABLE "user" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "status" "user_status_enum" NOT NULL DEFAULT 'active', CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "team_plan_enum" AS ENUM('free', 'basic', 'premium')`
    );
    await queryRunner.query(
      `CREATE TYPE "team_status_enum" AS ENUM('enabled', 'disabled')`
    );
    await queryRunner.query(
      `CREATE TABLE "team" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "slug" character varying NOT NULL, "plan" "team_plan_enum" NOT NULL, "status" "team_status_enum" NOT NULL, "billingEmail" character varying NOT NULL, "stripeCustomerId" character varying NOT NULL DEFAULT '', CONSTRAINT "UQ_e459cfa57273996b76d24a0fa68" UNIQUE ("slug"), CONSTRAINT "PK_f57d8293406df4af348402e4b74" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "team_to_user_role_enum" AS ENUM('owner', 'admin', 'member')`
    );
    await queryRunner.query(
      `CREATE TYPE "team_to_user_status_enum" AS ENUM('invited', 'activated')`
    );
    await queryRunner.query(
      `CREATE TABLE "team_to_user" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "teamId" integer, "role" "team_to_user_role_enum" NOT NULL, "status" "team_to_user_status_enum" NOT NULL, "userId" integer, CONSTRAINT "one_pair" UNIQUE ("teamId", "userId"), CONSTRAINT "PK_72dfe91f11fec020a6f330ca972" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_0f253ff3d8ded72cd8441277db" ON "team_to_user" ("teamId") WHERE role='owner'`
    );
    await queryRunner.query(
      `CREATE TYPE "template_type_enum" AS ENUM('thankyou', 'confirmation', 'registration')`
    );
    await queryRunner.query(
      `CREATE TABLE "template" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "type" "template_type_enum" NOT NULL, "content" text NOT NULL, "thumbnail" character varying NOT NULL DEFAULT '', "base" boolean NOT NULL DEFAULT false, "teamId" integer, CONSTRAINT "PK_fbae2ac36bd9b5e1e793b957b7f" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "template_instance_type_enum" AS ENUM('thankyou', 'confirmation', 'registration')`
    );
    await queryRunner.query(
      `CREATE TABLE "template_instance" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "parentId" integer, "webinarId" integer, "type" "template_instance_type_enum" NOT NULL, "content" text NOT NULL, "thumbnail" character varying NOT NULL DEFAULT '', "teamId" integer, CONSTRAINT "UQ_52bbc8c6b11ca4d7764e32a77f5" UNIQUE ("webinarId", "type"), CONSTRAINT "PK_ddceabfa4d2280f0c88e047e729" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "integration_provider_enum" AS ENUM('sendgrid', 'mailchimp', 'twilio', 'active_campaign', 'hub_spot')`
    );
    await queryRunner.query(
      `CREATE TABLE "integration" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "teamId" integer, "provider" "integration_provider_enum" NOT NULL, "settings" jsonb NOT NULL, CONSTRAINT "UQ_715132adcf9a67c433bc6342bfc" UNIQUE ("teamId", "provider"), CONSTRAINT "PK_f348d4694945d9dc4c7049a178a" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "notification_gateway" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "settings" jsonb NOT NULL, "integrationId" integer NOT NULL, CONSTRAINT "PK_78a5b4b33f24b46560181ae2783" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "webinar_type_enum" AS ENUM('live', 'recorded')`
    );
    await queryRunner.query(
      `CREATE TYPE "webinar_status_enum" AS ENUM('draft', 'active')`
    );
    await queryRunner.query(
      `CREATE TABLE "webinar" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "type" "webinar_type_enum" NOT NULL, "description" character varying NOT NULL DEFAULT '', "videoUrl" character varying NOT NULL, "start" date, "end" date, "status" "webinar_status_enum" NOT NULL DEFAULT 'active', "duration" integer NOT NULL, "replayExpiration" smallint NOT NULL, "thumbnail" character varying NOT NULL DEFAULT '', "isPaid" boolean NOT NULL DEFAULT false, "registrationFee" integer NOT NULL DEFAULT '0', "checkoutLink" character varying NOT NULL DEFAULT '', "redirectLink" character varying NOT NULL DEFAULT '', "timezone" character varying NOT NULL DEFAULT 'America/Chicago', "times" TIME array NOT NULL DEFAULT '{}'::time[], "days" smallint array NOT NULL DEFAULT '{}'::smallint[], "teamId" integer, "smsGatewayId" integer, "emailGatewayId" integer, "settingsPostregistrationurl" character varying NOT NULL DEFAULT '', "settingsPostviewurl" character varying NOT NULL DEFAULT '', "settingsRegistrationpagescript" character varying NOT NULL DEFAULT '', "settingsThankyoupagescript" character varying NOT NULL DEFAULT '', "settingsRegistrationformscript" character varying NOT NULL DEFAULT '', "settingsLiveroomscript" character varying NOT NULL DEFAULT '', "settingsReplayroomscript" character varying NOT NULL DEFAULT '', CONSTRAINT "REL_ca187ba4505a14d2c7687ec4dd" UNIQUE ("smsGatewayId"), CONSTRAINT "REL_c4dba78947b12ee71fde79f2bf" UNIQUE ("emailGatewayId"), CONSTRAINT "PK_603a787d99c2e1c2f5f883e2646" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "notification_type_enum" AS ENUM('welcome', 'pre_event', 'post_event', 'last_minute')`
    );
    await queryRunner.query(
      `CREATE TYPE "notification_deliverymethod_enum" AS ENUM('sms', 'email')`
    );
    await queryRunner.query(
      `CREATE TABLE "notification" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "type" "notification_type_enum" NOT NULL, "deliveryMethod" "notification_deliverymethod_enum" NOT NULL, "subject" character varying NOT NULL DEFAULT '', "content" character varying NOT NULL DEFAULT '', "hoursOffset" integer NOT NULL DEFAULT '0', "webinarId" integer, CONSTRAINT "PK_705b6c7cdf9b2c2ff7ac7872cb7" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "event" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "start" TIMESTAMP NOT NULL, "end" TIMESTAMP NOT NULL, "started" boolean NOT NULL DEFAULT false, "canceled" boolean NOT NULL DEFAULT false, "webinarId" integer, CONSTRAINT "unique_events" UNIQUE ("start", "webinarId"), CONSTRAINT "PK_30c2f3bbaf6d34a55f8ae6e4614" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "sent_event_notification" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "eventId" integer, "notificationId" integer, CONSTRAINT "PK_8dc4d7b67deeff2807c66bf17d7" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "reaction_status_enum" AS ENUM('staging', 'processing', 'ready')`
    );
    await queryRunner.query(
      `CREATE TABLE "reaction" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "observationKey" character varying NOT NULL, "status" "reaction_status_enum" NOT NULL, "report" jsonb NOT NULL, "attendeeId" integer, CONSTRAINT "REL_a73b91933c0beeb2e9559edb09" UNIQUE ("attendeeId"), CONSTRAINT "PK_41fbb346da22da4df129f14b11e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "attendee" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "firstName" character varying NOT NULL, "lastName" character varying NOT NULL, "email" character varying NOT NULL, "timezone" character varying NOT NULL, "phone" character varying NOT NULL DEFAULT '', "key" uuid NOT NULL DEFAULT uuid_generate_v4(), "active" boolean NOT NULL DEFAULT false, "eventId" integer, "attended" boolean NOT NULL DEFAULT false, CONSTRAINT "unique_registrations" UNIQUE ("email", "eventId"), CONSTRAINT "PK_070338c19378315cb793abac656" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "chat_message_label_enum" AS ENUM('attendee', 'presenter', 'support_team')`
    );
    await queryRunner.query(
      `CREATE TABLE "chat_message" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "offset_timestamp" integer NOT NULL, "label" "chat_message_label_enum" NOT NULL DEFAULT 'attendee', "name" character varying NOT NULL, "content" character varying NOT NULL, "webinarId" integer, CONSTRAINT "PK_3cc0d85193aade457d3077dd06b" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TYPE "offer_event_type_enum" AS ENUM('click', 'purchase')`
    );
    await queryRunner.query(
      `CREATE TABLE "offer_event" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "type" "offer_event_type_enum" NOT NULL DEFAULT 'click', "price" bigint NOT NULL DEFAULT '0', "offerId" integer, "attendeeId" integer, CONSTRAINT "PK_292fd2efd338dfd5d627a4310e3" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "page_view" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "key" uuid NOT NULL DEFAULT uuid_generate_v4(), "templateId" integer, "webinarId" integer, CONSTRAINT "PK_b01ec2dc6dc377f811d4db5613e" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "paid_registration" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "registrationFee" integer NOT NULL, "attendeeId" integer NOT NULL, "webinarId" integer, "eventId" integer, CONSTRAINT "PK_fd2828457e01298a923c181367a" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "viewer_event" ("id" SERIAL NOT NULL, "createdDate" TIMESTAMP NOT NULL DEFAULT now(), "updatedDate" TIMESTAMP NOT NULL DEFAULT now(), "time_offset" integer NOT NULL, "attendeeId" integer, CONSTRAINT "PK_83b2945fa55a1635ed243a22323" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "offer" ADD CONSTRAINT "FK_c403cea650adc33733b745ef18b" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "team_to_user" ADD CONSTRAINT "FK_006abba7a1e6044a1c6a7b026bb" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "team_to_user" ADD CONSTRAINT "FK_720a370dd5449b8a0b711bc3f68" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "template" ADD CONSTRAINT "FK_5bd2d67708bfb4266fab9774779" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "template_instance" ADD CONSTRAINT "FK_bcc51d8ca3fb718e36bef5e558f" FOREIGN KEY ("parentId") REFERENCES "template"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "template_instance" ADD CONSTRAINT "FK_df251194a13e27d2c8807e8952e" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "template_instance" ADD CONSTRAINT "FK_b49ead5a7554e0e3bea3a7b084a" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "integration" ADD CONSTRAINT "FK_0948b39f1c2d363aad743fcd0f7" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "notification_gateway" ADD CONSTRAINT "FK_ffd19976ed0ff8b468e490484df" FOREIGN KEY ("integrationId") REFERENCES "integration"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "webinar" ADD CONSTRAINT "FK_ca187ba4505a14d2c7687ec4dd5" FOREIGN KEY ("smsGatewayId") REFERENCES "notification_gateway"("id") ON DELETE SET NULL ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "webinar" ADD CONSTRAINT "FK_c4dba78947b12ee71fde79f2bff" FOREIGN KEY ("emailGatewayId") REFERENCES "notification_gateway"("id") ON DELETE SET NULL ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "webinar" ADD CONSTRAINT "FK_12ef079b25b507db4bb95ede97b" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "notification" ADD CONSTRAINT "FK_be478d3cb9cd91d86703680b683" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "event" ADD CONSTRAINT "FK_a11bd6c7ba28c7396bd40c4f7fa" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "sent_event_notification" ADD CONSTRAINT "FK_b74c936d6381386b0dd65ae8632" FOREIGN KEY ("eventId") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "sent_event_notification" ADD CONSTRAINT "FK_e4a72229e72734af982cf46407b" FOREIGN KEY ("notificationId") REFERENCES "notification"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "reaction" ADD CONSTRAINT "FK_a73b91933c0beeb2e9559edb095" FOREIGN KEY ("attendeeId") REFERENCES "attendee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "attendee" ADD CONSTRAINT "FK_7d85e02cada107c99eb697dd1fe" FOREIGN KEY ("eventId") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_message" ADD CONSTRAINT "FK_f07516386039bea9d86a0c03789" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "offer_event" ADD CONSTRAINT "FK_cc769877acda297302650842319" FOREIGN KEY ("offerId") REFERENCES "offer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "offer_event" ADD CONSTRAINT "FK_bf7635aba9e9261ccea654e5908" FOREIGN KEY ("attendeeId") REFERENCES "attendee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "page_view" ADD CONSTRAINT "FK_d0eefc306a9e049f910827fe072" FOREIGN KEY ("templateId") REFERENCES "template"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "page_view" ADD CONSTRAINT "FK_194d04cb4525985883960416f13" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "paid_registration" ADD CONSTRAINT "FK_17d2eed002e821d7694080cd090" FOREIGN KEY ("webinarId") REFERENCES "webinar"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "paid_registration" ADD CONSTRAINT "FK_04173939e50e549e731b6913af6" FOREIGN KEY ("eventId") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "paid_registration" ADD CONSTRAINT "FK_d42b3b0c0b1b144fe73c54bf448" FOREIGN KEY ("attendeeId") REFERENCES "attendee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "viewer_event" ADD CONSTRAINT "FK_20735638767648e1ce50d8b0420" FOREIGN KEY ("attendeeId") REFERENCES "attendee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "viewer_event" DROP CONSTRAINT "FK_20735638767648e1ce50d8b0420"`
    );
    await queryRunner.query(
      `ALTER TABLE "paid_registration" DROP CONSTRAINT "FK_d42b3b0c0b1b144fe73c54bf448"`
    );
    await queryRunner.query(
      `ALTER TABLE "paid_registration" DROP CONSTRAINT "FK_04173939e50e549e731b6913af6"`
    );
    await queryRunner.query(
      `ALTER TABLE "paid_registration" DROP CONSTRAINT "FK_17d2eed002e821d7694080cd090"`
    );
    await queryRunner.query(
      `ALTER TABLE "page_view" DROP CONSTRAINT "FK_194d04cb4525985883960416f13"`
    );
    await queryRunner.query(
      `ALTER TABLE "page_view" DROP CONSTRAINT "FK_d0eefc306a9e049f910827fe072"`
    );
    await queryRunner.query(
      `ALTER TABLE "offer_event" DROP CONSTRAINT "FK_bf7635aba9e9261ccea654e5908"`
    );
    await queryRunner.query(
      `ALTER TABLE "offer_event" DROP CONSTRAINT "FK_cc769877acda297302650842319"`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_message" DROP CONSTRAINT "FK_f07516386039bea9d86a0c03789"`
    );
    await queryRunner.query(
      `ALTER TABLE "attendee" DROP CONSTRAINT "FK_7d85e02cada107c99eb697dd1fe"`
    );
    await queryRunner.query(
      `ALTER TABLE "reaction" DROP CONSTRAINT "FK_a73b91933c0beeb2e9559edb095"`
    );
    await queryRunner.query(
      `ALTER TABLE "sent_event_notification" DROP CONSTRAINT "FK_e4a72229e72734af982cf46407b"`
    );
    await queryRunner.query(
      `ALTER TABLE "sent_event_notification" DROP CONSTRAINT "FK_b74c936d6381386b0dd65ae8632"`
    );
    await queryRunner.query(
      `ALTER TABLE "event" DROP CONSTRAINT "FK_a11bd6c7ba28c7396bd40c4f7fa"`
    );
    await queryRunner.query(
      `ALTER TABLE "notification" DROP CONSTRAINT "FK_be478d3cb9cd91d86703680b683"`
    );
    await queryRunner.query(
      `ALTER TABLE "webinar" DROP CONSTRAINT "FK_12ef079b25b507db4bb95ede97b"`
    );
    await queryRunner.query(
      `ALTER TABLE "webinar" DROP CONSTRAINT "FK_c4dba78947b12ee71fde79f2bff"`
    );
    await queryRunner.query(
      `ALTER TABLE "webinar" DROP CONSTRAINT "FK_ca187ba4505a14d2c7687ec4dd5"`
    );
    await queryRunner.query(
      `ALTER TABLE "notification_gateway" DROP CONSTRAINT "FK_ffd19976ed0ff8b468e490484df"`
    );
    await queryRunner.query(
      `ALTER TABLE "integration" DROP CONSTRAINT "FK_0948b39f1c2d363aad743fcd0f7"`
    );
    await queryRunner.query(
      `ALTER TABLE "template_instance" DROP CONSTRAINT "FK_b49ead5a7554e0e3bea3a7b084a"`
    );
    await queryRunner.query(
      `ALTER TABLE "template_instance" DROP CONSTRAINT "FK_df251194a13e27d2c8807e8952e"`
    );
    await queryRunner.query(
      `ALTER TABLE "template_instance" DROP CONSTRAINT "FK_bcc51d8ca3fb718e36bef5e558f"`
    );
    await queryRunner.query(
      `ALTER TABLE "template" DROP CONSTRAINT "FK_5bd2d67708bfb4266fab9774779"`
    );
    await queryRunner.query(
      `ALTER TABLE "team_to_user" DROP CONSTRAINT "FK_720a370dd5449b8a0b711bc3f68"`
    );
    await queryRunner.query(
      `ALTER TABLE "team_to_user" DROP CONSTRAINT "FK_006abba7a1e6044a1c6a7b026bb"`
    );
    await queryRunner.query(
      `ALTER TABLE "offer" DROP CONSTRAINT "FK_c403cea650adc33733b745ef18b"`
    );
    await queryRunner.query(`DROP TABLE "viewer_event"`);
    await queryRunner.query(`DROP TABLE "paid_registration"`);
    await queryRunner.query(`DROP TABLE "page_view"`);
    await queryRunner.query(`DROP TABLE "offer_event"`);
    await queryRunner.query(`DROP TYPE "offer_event_type_enum"`);
    await queryRunner.query(`DROP TABLE "chat_message"`);
    await queryRunner.query(`DROP TYPE "chat_message_label_enum"`);
    await queryRunner.query(`DROP TABLE "attendee"`);
    await queryRunner.query(`DROP TABLE "reaction"`);
    await queryRunner.query(`DROP TYPE "reaction_status_enum"`);
    await queryRunner.query(`DROP TABLE "sent_event_notification"`);
    await queryRunner.query(`DROP TABLE "event"`);
    await queryRunner.query(`DROP TABLE "notification"`);
    await queryRunner.query(`DROP TYPE "notification_deliverymethod_enum"`);
    await queryRunner.query(`DROP TYPE "notification_type_enum"`);
    await queryRunner.query(`DROP TABLE "webinar"`);
    await queryRunner.query(`DROP TYPE "webinar_status_enum"`);
    await queryRunner.query(`DROP TYPE "webinar_type_enum"`);
    await queryRunner.query(`DROP TABLE "notification_gateway"`);
    await queryRunner.query(`DROP TABLE "integration"`);
    await queryRunner.query(`DROP TYPE "integration_provider_enum"`);
    await queryRunner.query(`DROP TABLE "template_instance"`);
    await queryRunner.query(`DROP TYPE "template_instance_type_enum"`);
    await queryRunner.query(`DROP TABLE "template"`);
    await queryRunner.query(`DROP TYPE "template_type_enum"`);
    await queryRunner.query(`DROP INDEX "IDX_0f253ff3d8ded72cd8441277db"`);
    await queryRunner.query(`DROP TABLE "team_to_user"`);
    await queryRunner.query(`DROP TYPE "team_to_user_status_enum"`);
    await queryRunner.query(`DROP TYPE "team_to_user_role_enum"`);
    await queryRunner.query(`DROP TABLE "team"`);
    await queryRunner.query(`DROP TYPE "team_status_enum"`);
    await queryRunner.query(`DROP TYPE "team_plan_enum"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TYPE "user_status_enum"`);
    await queryRunner.query(`DROP TABLE "offer"`);
  }
}
