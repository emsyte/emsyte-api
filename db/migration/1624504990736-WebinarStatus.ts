import {MigrationInterface, QueryRunner} from "typeorm";

export class WebinarStatus1624504990736 implements MigrationInterface {
    name = 'WebinarStatus1624504990736'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."webinar_status_enum" RENAME TO "webinar_status_enum_old"`);
        await queryRunner.query(`CREATE TYPE "webinar_status_enum" AS ENUM('draft', 'active', 'archive')`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" TYPE "webinar_status_enum" USING "status"::"text"::"webinar_status_enum"`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" SET DEFAULT 'draft'`);
        await queryRunner.query(`DROP TYPE "webinar_status_enum_old"`);
        await queryRunner.query(`COMMENT ON COLUMN "webinar"."status" IS NULL`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" SET DEFAULT 'draft'`);
        await queryRunner.query(`COMMENT ON COLUMN "recurrence"."days" IS NULL`);
        await queryRunner.query(`ALTER TABLE "recurrence" ALTER COLUMN "days" SET DEFAULT '{}'::smallint[]`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "recurrence" ALTER COLUMN "days" SET DEFAULT '{}'`);
        await queryRunner.query(`COMMENT ON COLUMN "recurrence"."days" IS NULL`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" SET DEFAULT 'active'`);
        await queryRunner.query(`COMMENT ON COLUMN "webinar"."status" IS NULL`);
        await queryRunner.query(`CREATE TYPE "webinar_status_enum_old" AS ENUM('draft', 'active')`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" TYPE "webinar_status_enum_old" USING "status"::"text"::"webinar_status_enum_old"`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "status" SET DEFAULT 'draft'`);
        await queryRunner.query(`DROP TYPE "webinar_status_enum"`);
        await queryRunner.query(`ALTER TYPE "webinar_status_enum_old" RENAME TO  "webinar_status_enum"`);
    }

}
