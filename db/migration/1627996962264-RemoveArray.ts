import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveArray1627996962264 implements MigrationInterface {
    name = 'RemoveArray1627996962264'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "recurrence" DROP COLUMN "days"`);
        await queryRunner.query(`ALTER TABLE "recurrence" ADD "days" jsonb NOT NULL DEFAULT '[]'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "recurrence" DROP COLUMN "days"`);
        await queryRunner.query(`ALTER TABLE "recurrence" ADD "days" smallint array NOT NULL DEFAULT '{}'`);
    }

}
