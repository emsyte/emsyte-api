import {MigrationInterface, QueryRunner} from "typeorm";

export class ConstantContact1627849891029 implements MigrationInterface {
    name = 'ConstantContact1627849891029'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "integration" DROP CONSTRAINT "UQ_715132adcf9a67c433bc6342bfc"`);
        await queryRunner.query(`ALTER TYPE "integration_provider_enum" RENAME TO "integration_provider_enum_old"`);
        await queryRunner.query(`CREATE TYPE "integration_provider_enum" AS ENUM('sendgrid', 'mailchimp', 'twilio', 'active_campaign', 'hub_spot', 'constant_contact')`);
        await queryRunner.query(`ALTER TABLE "integration" ALTER COLUMN "provider" TYPE "integration_provider_enum" USING "provider"::"text"::"integration_provider_enum"`);
        await queryRunner.query(`DROP TYPE "integration_provider_enum_old"`);
        await queryRunner.query(`ALTER TABLE "integration" ADD CONSTRAINT "UQ_715132adcf9a67c433bc6342bfc" UNIQUE ("teamId", "provider")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "integration" DROP CONSTRAINT "UQ_715132adcf9a67c433bc6342bfc"`);
        await queryRunner.query(`CREATE TYPE "integration_provider_enum_old" AS ENUM('sendgrid', 'mailchimp', 'twilio', 'active_campaign', 'hub_spot')`);
        await queryRunner.query(`ALTER TABLE "integration" ALTER COLUMN "provider" TYPE "integration_provider_enum_old" USING "provider"::"text"::"integration_provider_enum_old"`);
        await queryRunner.query(`DROP TYPE "integration_provider_enum"`);
        await queryRunner.query(`ALTER TYPE "integration_provider_enum_old" RENAME TO "integration_provider_enum"`);
        await queryRunner.query(`ALTER TABLE "integration" ADD CONSTRAINT "UQ_715132adcf9a67c433bc6342bfc" UNIQUE ("teamId", "provider")`);
    }

}
