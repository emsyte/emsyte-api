import {MigrationInterface, QueryRunner} from "typeorm";

export class ReplayExpirationDefault1624819841953 implements MigrationInterface {
    name = 'ReplayExpirationDefault1624819841953'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "webinar"."replayExpiration" IS NULL`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "replayExpiration" SET DEFAULT '0'`);
        await queryRunner.query(`COMMENT ON COLUMN "recurrence"."days" IS NULL`);
        await queryRunner.query(`ALTER TABLE "recurrence" ALTER COLUMN "days" SET DEFAULT '{}'::smallint[]`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "recurrence" ALTER COLUMN "days" SET DEFAULT '{}'`);
        await queryRunner.query(`COMMENT ON COLUMN "recurrence"."days" IS NULL`);
        await queryRunner.query(`ALTER TABLE "webinar" ALTER COLUMN "replayExpiration" DROP DEFAULT`);
        await queryRunner.query(`COMMENT ON COLUMN "webinar"."replayExpiration" IS NULL`);
    }

}
