import { Recurrence } from "../entities/Webinar";
import { Frequency } from "@emsyte/common/webinar";

describe("RRule", () => {
  test("Generates all events for daily recurrence", () => {
    const r = Recurrence.create({
      frequency: Frequency.DAILY,
      start: "2021-06-16 10:30",
      end: "2021-06-20",
      timezone: "America/New_York",
    });

    const days = r
      .getRRule()
      .all()
      .map((d) => d.toISOString());

    expect(days).toEqual([
      "2021-06-16T14:30:00.000Z",
      "2021-06-17T14:30:00.000Z",
      "2021-06-18T14:30:00.000Z",
      "2021-06-19T14:30:00.000Z",
      "2021-06-20T14:30:00.000Z",
    ]);
  });

  test("Generates all events for weekly recurrence", () => {
    const r = Recurrence.create({
      frequency: Frequency.WEEKLY,
      start: "2021-06-16 8:30",
      end: "2021-06-30",
      timezone: "America/New_York",
      days: [3],
    });

    const days = r
      .getRRule()
      .all()
      .map((d) => d.toISOString());

    expect(days).toEqual([
      "2021-06-16T12:30:00.000Z",
      "2021-06-23T12:30:00.000Z",
      "2021-06-30T12:30:00.000Z",
    ]);
  });
});
