/// <reference types="handlebars" />

declare module "*.handlebars" {
  const template: Handlebars.TemplateDelegate;

  namespace template {}

  export = template;
}
